package cn.topcode.unicorn.wxsdk;

import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by Unicorn on 2017/8/15.
 */
public abstract class WXMessageListener<T extends Message> {

    @SuppressWarnings("unchecked")
    public WXMessageListener() {
        Class<T> entityClass = null;
        Type t = getClass().getGenericSuperclass();
        if(t instanceof ParameterizedType){
            Type[] p = ((ParameterizedType)t).getActualTypeArguments();
            entityClass = (Class<T>)p[0];
        }
        WXContext.registListener(entityClass, this);
    }

    public abstract void onReceive(T message);
}
