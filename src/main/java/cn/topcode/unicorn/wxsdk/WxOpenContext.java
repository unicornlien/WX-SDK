package cn.topcode.unicorn.wxsdk;

import cn.topcode.unicorn.wxsdk.openapi.mobile.WxLoginInvoker;

/**
 * Created by Unicorn on 2017/12/13.
 */
public class WxOpenContext {

    private static WxLoginInvoker wxLoginInvoker = null;

    public static WxLoginInvoker getWxLoginInvoker() {
        if(wxLoginInvoker == null) {
            synchronized (WxLoginInvoker.class) {
                if(wxLoginInvoker == null) {
                    wxLoginInvoker = new WxLoginInvoker();
                }
            }
        }
        return wxLoginInvoker;
    }
}
