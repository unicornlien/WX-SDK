package cn.topcode.unicorn.wxsdk.shop.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class GetSubCatalogResult extends Result {

    @JSONField(name = "cate_list")
    private List<Catalog> cateList;
}
