package cn.topcode.unicorn.wxsdk.shop.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class SkuList {

    /**
     * sku信息
     */
    @JSONField(name = "sku_id")
    private String skuId;

    /**
     * sku微信价
     */
    private Integer price;

    /**
     * sku图标
     */
    @JSONField(name = "icon_url")
    private String iconUrl;

    /**
     * 商家商品编码
     */
    @JSONField(name = "product_code")
    private String productCode;

    /**
     * sku原价
     */
    @JSONField(name = "ori_price")
    private Integer oriPrice;

    /**
     * sku库存
     */
    private Integer quantity;

}
