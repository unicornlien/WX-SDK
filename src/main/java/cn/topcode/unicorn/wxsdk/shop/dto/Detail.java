package cn.topcode.unicorn.wxsdk.shop.dto;

import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class Detail {

    /**
     * 文字描述
     */
    private String text;

    /**
     * 图片
     */
    private String img;
}
