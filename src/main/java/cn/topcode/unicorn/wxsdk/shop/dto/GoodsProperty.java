package cn.topcode.unicorn.wxsdk.shop.dto;

import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class GoodsProperty {

    /**
     * 属性id
     */
    private String id;

    /**
     * 属性值id
     */
    private String vid;
}
