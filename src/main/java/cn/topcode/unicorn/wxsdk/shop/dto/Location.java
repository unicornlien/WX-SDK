package cn.topcode.unicorn.wxsdk.shop.dto;

import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class Location {

    /**
     * 国家
     */
    private String country;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 地址
     */
    private String address;
}
