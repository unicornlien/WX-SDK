package cn.topcode.unicorn.wxsdk.shop;

import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.shop.dto.*;

/**
 * 微信小店接口
 */
public interface ShopInvoker {

    /*商品管理接口*/

    /**
     * 创建商品
     * @param createGoods
     * @return
     */
    CreateGoodsResult createGoods(String mpId, CreateGoods createGoods);

    CreateGoodsResult createGoods(CreateGoods createGoods);

    /**
     * 删除商品
     * @param productId
     * @return
     */
    Result deleteGoods(String mpId, String productId);

    Result deleteGoods(String productId);

    /**
     * 修改商品
     * @param updateGoods
     * @return
     */
    Result updateGoods(String mpId, UpdateGoods updateGoods);

    Result updateGoods(UpdateGoods updateGoods);

    /**
     * 查询商品信息
     * @param productId
     * @return
     */
    QueryGoodsResult queryGoods(String mpId, String productId);

    QueryGoodsResult queryGoods(String productId);

    /**
     * 根据状态查询所有商品
     * @param status
     * @return
     */
    QueryGoodsByStatusResult queryGoodsByStatus(String mpId, int status);

    QueryGoodsByStatusResult queryGoodsByStatus(int status);

    /**
     * 商品上架
     * @param productId
     * @return
     */
    Result putOnGoods(String mpId, String productId);

    Result putOnGoods(String productId);

    /**
     * 商品下架
     * @param productId
     * @return
     */
    Result putOffGoods(String mpId, String productId);

    Result putOffGoods(String productId);

    /**
     * 获取指定分类的所有子分类
     * @param cateId
     * @return
     */
    GetSubCatalogResult getSubCatalog(String mpId, String cateId);

    GetSubCatalogResult getSubCatalog(String cateId);

    void getSubCatalogSku(String mpId, String cateId);

    void getSubCatalogSku(String cateId);

    void getCatalogProperty(String mpId);

    void getCatalogProperty();

    /*库存管理接口*/

    /**
     * 添加库存
     * @param productId
     * @param skuInfo
     * @param quantity
     * @return
     */
    Result addStock(String mpId, String productId, String skuInfo, int quantity);

    Result addStock(String productId, String skuInfo, int quantity);

    /**
     * 减少库存
     * @param productId
     * @param skuInfo
     * @param quantity
     * @return
     */
    Result subStock(String mpId, String productId, String skuInfo, int quantity);

    Result subStock(String productId, String skuInfo, int quantity);

    /*邮费模板管理接口*/


    void addPostageTemplate(String mpId);

    void addPostageTemplate();

    void deletePostageTemplate(String mpId);

    void deletePostageTemplate();

    void updatePostageTemplate(String mpId);

    void updatePostageTemplate();

    void getPostageTemplateById(String mpId);

    void getPostageTemplateById();

    void getAllPostageTemplate(String mpId);

    void getAllPostageTemplate();

    /*分组管理接口*/

    void addGroup(String mpId);

    void addGroup();

    void deleteGroup(String mpId);

    void deleteGroup();

    void updateGroupProperty(String mpId);

    void updateGroupProperty();

    void updateGroupGoods(String mpId);

    void updateGroupGoods();

    void getAllGroup(String mpId);

    void getAllGroup();

    void getGroupById(String mpId);

    void getGroupById();

    /*货架管理接口*/

    void addShelves(String mpId);

    void addShelves();

    void deleteShelves(String mpId);

    void deleteShelves();

    void updateShelves(String mpId);

    void updateShelves();

    void getAllShelves(String mpId);

    void getAllShelves();

    void getShelvesById(String mpId);

    void getShelvesById();

    void setShelvesPage(String mpId);

    void setShelvesPage();

    /*订单管理接口*/

    void orderPayNotify(String mpId);

    void orderPayNotify();

    void getOrderById(String mpId);

    void getOrderById();

    void getOrderByStatus(String mpId);

    void getOrderByStatus();

    void setOrderSendGoodsInfo(String mpId);

    void setOrderSendGoodsInfo();

    void closeOrder(String mpId);

    void closeOrder();

    /*功能接口*/

    void uploadImage(String mpId);

    void uploadImage();
}
