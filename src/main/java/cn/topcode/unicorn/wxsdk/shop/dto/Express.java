package cn.topcode.unicorn.wxsdk.shop.dto;

import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class Express {

    /**
     * 快递ID
     */
    private Integer id;

    /**
     * 运费（单位：分）
     */
    private Integer price;
}
