package cn.topcode.unicorn.wxsdk.shop.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class CreateGoods {

    /**
     * 基本属性
     */
    @JSONField(name = "product_base")
    private ProductBase productBase;

    /**
     * sku信息列表
     */
    @JSONField(name = "sku_list")
    private List<SkuList> skuList;

    /**
     * 商品其他属性
     */
    private Attrext attrext;

    /**
     * 运费信息
     */
    @JSONField(name = "delivery_info")
    private DeliveryInfo deliveryInfo;
}
