package cn.topcode.unicorn.wxsdk.shop.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class QueryGoodsResult extends Result {

    @JSONField(name = "product_info")
    private UpdateGoods productInfo;
}
