package cn.topcode.unicorn.wxsdk.shop.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import java.util.List;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class ProductBase {

    /**
     * 商品名称，必填
     */
    private String name;

    /**
     * 商品分类ID
     */
    @JSONField(name = "category_id")
    private List<String> categoryId;

    /**
     * 商品主图，必填，推荐640x600
     */
    @JSONField(name = "main_img")
    private String mainImg;

    /**
     * 商品图片列表，必填，推荐640x600
     */
    private List<String> img;

    /**
     * 商品详情列表，必填
     */
    private List<Detail> detail;

    /**
     * 商品属性列表
     */
    private List<GoodsProperty> property;

    /**
     * 商品sku定义
     */
    @JSONField(name = "sku_info")
    private List<SkuInfo> skuInfo;

    /**
     * 用户商品限购数量
     */
    @JSONField(name = "buy_limit")
    private Integer buyLimit;
}
