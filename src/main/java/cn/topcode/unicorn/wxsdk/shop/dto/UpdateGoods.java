package cn.topcode.unicorn.wxsdk.shop.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class UpdateGoods extends CreateGoods {

    /**
     * 商品ID
     */
    @JSONField(name = "product_id")
    private String productId;
}
