package cn.topcode.unicorn.wxsdk.shop.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class DeliveryInfo {

    /**
     * 运费类型
     */
    @JSONField(name = "delivery_type")
    private Integer deliveryType;

    /**
     * 邮费模板ID
     */
    @JSONField(name = "template_id")
    private Integer templateId;

    private List<Express> express;
}
