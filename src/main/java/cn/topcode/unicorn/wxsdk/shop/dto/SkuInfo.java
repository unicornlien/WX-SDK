package cn.topcode.unicorn.wxsdk.shop.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class SkuInfo {

    /**
     * sku属性
     */
    private String id;

    /**
     * sku值
     */
    private List<String> vid;
}
