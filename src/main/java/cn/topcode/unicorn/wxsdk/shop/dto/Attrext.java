package cn.topcode.unicorn.wxsdk.shop.dto;

import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
public class Attrext {

    /**
     * 商品所在地地址
     */
    private Location location;

    /**
     * 是否包邮，包邮这delivery_info可省略
     */
    private Integer isPostFree;

    /**
     * 是否提供发票
     */
    private Integer isHasReceipt;

    /**
     * 是否保修
     */
    private Integer isUnderGuaranty;

    /**
     * 是否支持退换货
     */
    private Integer isSupportReplace;
}
