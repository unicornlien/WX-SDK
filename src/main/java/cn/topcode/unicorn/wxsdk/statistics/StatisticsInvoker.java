package cn.topcode.unicorn.wxsdk.statistics;

public interface StatisticsInvoker {

    /**
     * 获取用户增减数据
     * @author Leo Lien
     * 2016年12月5日 下午10:17:08 创建
     */
    void getUserSummary(String mpId);

    void getUserSummary();
    
    /**
     * 获取累计用户数据
     * @author Leo Lien
     * 2016年12月5日 下午10:17:16 创建
     */
    void getUserCumulate(String mpId);

    void getUserCumulate();
    
    /**
     * 获取图文群发每日数据
     * @author Leo Lien
     * 2016年12月5日 下午10:18:10 创建
     */
    void getArticleSummary(String mpId);

    void getArticleSummary();
    
    /**
     * 获取图文群发总数据
     * @author Leo Lien
     * 2016年12月5日 下午10:18:26 创建
     */
    void getArticleTotal(String mpId);

    void getArticleTotal();
    
    /**
     * 获取图文统计数据
     * @author Leo Lien
     * 2016年12月5日 下午10:18:53 创建
     */
    void getUsreRead(String mpId);

    void getUsreRead();
    
    /**
     * 获取图文统计分时数据
     * @author Leo Lien
     * 2016年12月5日 下午10:19:08 创建
     */
    void getUserReadHour(String mpId);

    void getUserReadHour();
    
    /**
     * 获取图文分享转发数据
     * @author Leo Lien
     * 2016年12月5日 下午10:19:23 创建
     */
    void getUserShare(String mpId);

    void getUserShare();
    
    /**
     * 获取图文分享转发分时数据
     * @author Leo Lien
     * 2016年12月5日 下午10:19:37 创建
     */
    void getUserShareHour(String mpId);

    void getUserShareHour();
    
    /**
     * 获取消息发送概况数据
     * @author Leo Lien
     * 2016年12月5日 下午10:21:54 创建
     */
    void getUpstreamMsg(String mpId);

    void getUpstreamMsg();
    
    /**
     * 获取消息分送分时数据
     * @author Leo Lien
     * 2016年12月5日 下午10:22:00 创建
     */
    void getUpstreamMsgHour(String mpId);

    void getUpstreamMsgHour();
    
    /**
     * 获取消息发送周数据
     * @author Leo Lien
     * 2016年12月5日 下午10:22:05 创建
     */
    void getUpstreamMsgWeek(String mpId);

    void getUpstreamMsgWeek();
    
    /**
     * 获取消息发送月数据
     * @author Leo Lien
     * 2016年12月5日 下午10:22:11 创建
     */
    void getUpstreamMsgMonth(String mpId);

    void getUpstreamMsgMonth();
    
    /**
     * 获取消息发送分布数据
     * @author Leo Lien
     * 2016年12月5日 下午10:22:16 创建
     */
    void getUpstreamMsgDist(String mpId);

    void getUpstreamMsgDist();
    
    /**
     * 获取消息发送分布周数据
     * @author Leo Lien
     * 2016年12月5日 下午10:22:23 创建
     */
    void getUpstreamMsgDistWeek(String mpId);

    void getUpstreamMsgDistWeek();
    
    /**
     * 获取消息发送分布月数据
     * @author Leo Lien
     * 2016年12月5日 下午10:22:29 创建
     */
    void getUpstreamMsgDistMonth(String mpId);

    void getUpstreamMsgDistMonth();
    
    /**
     * 获取接口分析数据
     * @author Leo Lien
     * 2016年12月5日 下午10:22:56 创建
     */
    void getInterfaceSummary(String mpId);

    void getInterfaceSummary();
    
    /**
     * 获取接口分析分时数据
     * @author Leo Lien
     * 2016年12月5日 下午10:23:01 创建
     */
    void getInterfaceSummaryHour(String mpId);

    void getInterfaceSummaryHour();

}
