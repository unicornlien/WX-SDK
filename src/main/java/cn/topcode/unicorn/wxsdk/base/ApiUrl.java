package cn.topcode.unicorn.wxsdk.base;

/**
 * 微信api url
 * @author Unicorn Lien
 * 2016年12月4日 上午1:13:08 创建
 */
public class ApiUrl {
    
    /*请求Token*/
    public static final String REQUEST_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

    /*请求JS Ticket*/
    public static final String REQUEST_JS_TICKET = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=jsapi";
    
    /*获取微信服务器IP列表*/
    public static final String GET_WECHAT_SERVER_IP = "https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=%s";

    /*网页授权*/
    public static class WebAuth {

        public static final String OAUTH2_SNSAPI_BASE  = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base&state=%s#wechat_redirect";

        public static final String OAUTH2_SNSAPI_USERINFO = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_userinfo&state=%s#wechat_redirect";

        /*请求网页授权token*/
        public static final String GET_WEB_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";

        /*网页授权请求用户信息*/
        public static final String GET_USER_INFO = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";

        private WebAuth() {}
    }

    public static class Statistics {
        
        /*获取用户增减数据*/
        public static final String GET_USER_SUMMARY = "https://api.weixin.qq.com/datacube/getusersummary?access_token=%s";
        /*获取累计用户数据*/
        public static final String GET_USER_CUMULATE = "https://api.weixin.qq.com/datacube/getusercumulate?access_token=%s";
        
        /* 获取图文群发每日数据 */
        public static final String GET_ARTICLE_SUMMARY = "https://api.weixin.qq.com/datacube/getarticlesummary?access_token=%s";
        /* 获取图文群发总数据 */
        public static final String GET_ARTICLE_TOTAL = "https://api.weixin.qq.com/datacube/getarticletotal?access_token=%s";
        /* 获取图文统计数据 */
        public static final String GET_USER_READ = "https://api.weixin.qq.com/datacube/getuserread?access_token=%s";
        /* 获取图文统计分时数据 */
        public static final String GET_USER_READ_HOUR = "https://api.weixin.qq.com/datacube/getuserreadhour?access_token=%s";
        /* 获取图文分享转发数据 */
        public static final String GET_USER_SHARE = "https://api.weixin.qq.com/datacube/getusershare?access_token=%s";
        /* 获取图文分享转发分时数据 */
        public static final String GET_USER_SHARE_HOUR = "https://api.weixin.qq.com/datacube/getusersharehour?access_token=%s";
        
        /* 获取消息发送概况数据 */
        public static final String GET_UPSTREAM_MSG = "https://api.weixin.qq.com/datacube/getupstreammsg?access_token=%s";
        /* 获取消息分送分时数据 */
        public static final String GET_UPSTREAM_MSG_HOUR = "https://api.weixin.qq.com/datacube/getupstreammsghour?access_token=%s";
        /* 获取消息发送周数据 */
        public static final String GET_UPSTREAM_MSG_WEEK = "https://api.weixin.qq.com/datacube/getupstreammsgweek?access_token=%s";
        /* 获取消息发送月数据 */
        public static final String GET_UPSTREAM_MSG_MONTH = "https://api.weixin.qq.com/datacube/getupstreammsgmonth?access_token=%s";
        /* 获取消息发送分布数据 */
        public static final String GET_UPSTREAM_MSG_DIST = "https://api.weixin.qq.com/datacube/getupstreammsgdist?access_token=%s";
        /* 获取消息发送分布周数据 */
        public static final String GET_UPSTREAM_MSG_DIST_WEEK = "https://api.weixin.qq.com/datacube/getupstreammsgdistweek?access_token=%s";
        /* 获取消息发送分布月数据 */
        public static final String GET_UPSTREAM_MSG_DIST_MONTH = "https://api.weixin.qq.com/datacube/getupstreammsgdistmonth?access_token=%s";
        
        /* 获取接口分析数据 */
        public static final String GET_INTERFACE_SUMMARY = "https://api.weixin.qq.com/datacube/getinterfacesummary?access_token=%s";
        /* 获取接口分析分时数据 */
        public static final String GET_INTERFACE_SUMMARY_HOUR = "https://api.weixin.qq.com/datacube/getinterfacesummaryhour?access_token=%s";
        
        private Statistics() {}
    }

    /*账号管理*/
    public static class Account {
        
        /*创建二维码ticket*/
        public static final String CREATE_TICKET = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s";
        
        /*通过ticket换取二维码*/
        public static final String SHOW_QRCODE = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=%s";
        
        /*长链接转短链接*/
        public static final String SHORT_URL = "https://api.weixin.qq.com/cgi-bin/shorturl?access_token=%s";
        
        private Account() {}
    }

    /*菜单接口*/
    public static class Menu {
        
        /*创建菜单*/
        public static final String CREATE_MENU = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=%s";
        /*查询菜单*/
        public static final String QUERY_MENU = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=%s";
        /*删除菜单*/
        public static final String DELETE_MENU = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=%s";
        /*创建个性化菜单*/
        public static final String CREATE_PERSONAL_MENU = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=%s";
        /*删除个性化菜单*/
        public static final String DELETE_PERSONAL_MENU = "https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token=%s";
        /*测试个性化菜单*/
        public static final String TEST_PERSONAL_MENU = "https://api.weixin.qq.com/cgi-bin/menu/trymatch?access_token=%s";
        /*获取自定义菜单配置*/
        public static final String QUERY_PERSONAL_MENU = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=%s";
        
        private Menu() {}
    }
    
    public static class User {
        
        /*创建用户标签*/
        public static final String CRREATE_USER_TAG = "https://api.weixin.qq.com/cgi-bin/tags/create?access_token=%s";
        /*获取公众号已创建的标签*/
        public static final String GET_TAG_LIST = "https://api.weixin.qq.com/cgi-bin/tags/get?access_token=%s";
        /*编辑标签*/
        public static final String EDIT_TAG = "https://api.weixin.qq.com/cgi-bin/tags/update?access_token=%s";
        /*删除标签*/
        public static final String DELETE_TAG = "https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=%s";
        /*获取标签下粉丝列表*/
        public static final String GET_TAG_USER_LIST = "https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=%s";
        /*批量为用户打标签*/
        public static final String BATCH_TAG = "https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=%s";
        /*批量为用户取消标签*/
        public static final String BATCH_CANCEL_TAG = "https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=%s";
        /*获取用户身上的标签列表*/
        public static final String GET_USER_TAG = "https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token=%s";
        
        
        /*获取用户列表*/
        public static final String GET_USER_LIST = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s&next_openid=%s";
        
        /*设置用户备注名称*/
        public static final String SET_REMARK = "https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=%s";
        
        /*获取用户基本信息*/
        public static final String GET_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=%s";
        
        /*获取黑名单列表*/
        public static final String GET_BLACK_LIST = "https://api.weixin.qq.com/cgi-bin/tags/members/getblacklist?access_token=%s";
        
        /*拉黑用户*/
        public static final String MARK_BLACK = "https://api.weixin.qq.com/cgi-bin/tags/members/batchblacklist?access_token=%s";
        
        /*取消拉黑*/
        public static final String MARK_WHITE = "https://api.weixin.qq.com/cgi-bin/tags/members/batchunblacklist?access_token=%s";
        
        private User() {}
    }
    
    public static class Material {
        
        /*新增临时素材*/
        public static final String CREATE_TEMP_MATERIAL = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s";
        /*获取临时素材*/
        public static final String GET_TEMP_MATERIAL = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s";
        /*新增永久图文素材*/
        public static final String CREATE_FOREVER_ARTICLE_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=%s";
        /*上传永久图文素材的图片*/
        public static final String UPLOAD_ARTICLE_IMG = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=%s";
        /*新增永久素材*/
        public static final String CREATE_FOREVER_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=%s&type=%s";
        /*获取永久素材*/
        public static final String GET_FOREVER_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=%s";
        /*删除永久素材*/
        public static final String DELETE_FOREVER_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=%s";
        /*修改永久图文素材*/
        public static final String UPDATE_FOREVER_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/update_news?access_token=%s";
        /*获取素材总数*/
        public static final String GET_MATERIAL_COUNT = "https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=%s";
        /*获取素材列表*/
        public static final String GET_MATERIAL_LIST = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=%s";
        
        private Material() {}
    }
    
    public static class Message {
        
        /*添加客服账号*/
        public static final String ADD_KF_ACCOUNT = "https://api.weixin.qq.com/customservice/kfaccount/add?access_token=%s";
        /*修改客户账号*/
        public static final String MODIFY_KF_ACCOUNT = "https://api.weixin.qq.com/customservice/kfaccount/update?access_token=%s";
        /*删除客服账号*/
        public static final String DELETE_KF_ACCOUNT = "https://api.weixin.qq.com/customservice/kfaccount/del?access_token=%s";
        /*设置客服账号头像*/
        public static final String SET_KF_ACCOUNT_HEAD_IMG = "http://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=%s&kf_account=%s";
        /*获取所有客服账号*/
        public static final String GET_ALL_KF_ACCOUNT = "https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=%s";
        /*发送客服消息*/
        public static final String SEND_KF_MESSAGE = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=%s";
        
        /*上传图文消息内的图片获取URL*/
        public static final String UPLOAD_IMG_MESSAGE_IMG = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=%s";
        /*上传图文消息素材*/
        public static final String UPLOAD_IMG_MESSAGE_MATERIAL = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=%s";
        /*根据标签进行群发*/
        public static final String MASS_TAG = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=%s";
        /*根据OpenID列表群发*/
        public static final String MASS_OPENID_LIST = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=%s";
        /*删除群发*/
        public static final String DELETE_MASS = "https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=%s";
        /*预览接口*/
        public static final String PREVIEW_IMAGE_MESSAGE = "https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=%s";
        /*查询群发消息发送状态*/
        public static final String QUERY_MASS_STATUS = "https://api.weixin.qq.com/cgi-bin/message/mass/get?access_token=%s";

        /*设置所属行业*/
        public static final String SET_INDUSTRY = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=%s";
        
        /*获取所属行业*/
        public static final String GET_INDUSTRY = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=%s";
        
        /*获取模板ID*/
        public static final String GET_TEMPLATE_ID = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=%s";
        
        /*获取模板列表*/
        public static final String GET_TEMPALTE_LIST = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=%s";
        
        /*删除模板*/
        public static final String DELETE_TEMPLATE = "https://api.weixin.qq.com/cgi-bin/template/del_private_template?access_token=%s";
        
        /*模板消息发送地址*/
        public static final String SEND_TEMPLATE_MESSAGE_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%s";
        
        /*获取自动回复规则*/
        public static final String GET_AUTO_REPLY_RULE = "https://api.weixin.qq.com/cgi-bin/get_current_autoreply_info?access_token=%s";
        
        private Message() {}
    }

    /*卡券*/
    public static class Coupons {

        /*上传卡券Logo*/
        public static final String UPLOAD_COUPONS_IMG = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=%s";

        /*创建卡券*/
        public static final String CREATE_COUPONS = "https://api.weixin.qq.com/card/create?access_token=%s";

        /*设置买单接口*/
        public static final String PAY_CELL = "https://api.weixin.qq.com/card/paycell/set?access_token=%s";

        /*设置自助核销接口*/
        public static final String SELF_CONSUME_CELL = "https://api.weixin.qq.com/card/selfconsumecell/set?access_token=%s";

        /*创建二维码*/
        public static final String CREATE_QRCODE = "https://api.weixin.qq.com/card/qrcode/create?access_token=%s";

        /*创建货架*/
        public static final String CREATE_SHIFT = "https://api.weixin.qq.com/card/landingpage/create?access_token=%s";

        /*导入自定义code*/
        public static final String DEPOSIT_CODE = "http://api.weixin.qq.com/card/code/deposit?access_token=%s";

        /*查询导入code数目*/
        public static final String GET_DEPOSIT_COUNT = "http://api.weixin.qq.com/card/code/getdepositcount?access_token=%s";

        /*核查code接口*/
        public static final String CHECK_CODE = "http://api.weixin.qq.com/card/code/checkcode?access_token=%s";

        /*图文消息群发卡券*/
        public static final String MP_NEWS = "https://api.weixin.qq.com/card/mpnews/gethtml?access_token=%s";

        /*设置测试白名单*/
        public static final String SET_TEST_WHITE_LIST = "https://api.weixin.qq.com/card/testwhitelist/set?access_token=%s";

        /*查询code*/
        public static final String CODE_GET = "https://api.weixin.qq.com/card/code/get?access_token=%s";

        /*核销code*/
        public static final String CONSUME_CODE = "https://api.weixin.qq.com/card/code/consume?access_token=%s";

        /*code解码*/
        public static final String DECRYPT_CODE = "https://api.weixin.qq.com/card/code/decrypt?access_token=%s";

        /*获取用户已领取卡券接口*/
        public static final String GET_CARD_LIST = "https://api.weixin.qq.com/card/user/getcardlist?access_token=%s";

        /*卡券详情*/
        public static final String CARD_DETAIL = "https://api.weixin.qq.com/card/get?access_token=%s";

        /*批量查询卡券列表*/
        public static final String BATCH_GET = "https://api.weixin.qq.com/card/batchget?access_token=%s";

        /*更改卡券信息接口*/
        public static final String UPDATE_COUPONS = "https://api.weixin.qq.com/card/update?access_token=%s";

        /*更改库存接口*/
        public static final String MODIFY_STOCK = "https://api.weixin.qq.com/card/modifystock?access_token=%s";

        /*更改CODE接口*/
        public static final String UPDATE_CODE = "https://api.weixin.qq.com/card/code/update?access_token=%s";

        /*删除卡券接口*/
        public static final String DELETE_COUPONS = "https://api.weixin.qq.com/card/delete?access_token=%s";

        /*设置卡券失效接口*/
        public static final String UNAVAILABLE_COUPONS = "https://api.weixin.qq.com/card/code/unavailable?access_token=%s";

        /*拉取卡券概况数据接口*/
        public static final String GET_CARD_BIZ_UIN_INFO = "https://api.weixin.qq.com/datacube/getcardbizuininfo?access_token=%s";

        /*获取免费券数据接口*/
        public static final String GET_FREE_CARD_INFO = "https://api.weixin.qq.com/datacube/getcardcardinfo?access_token=%s";

        /*拉取会员卡概况数据接口*/
        public static final String GET_MEMBER_CARD_INFO = "https://api.weixin.qq.com/datacube/getcardmembercardinfo?access_token=%s";

        /*拉取单张会员卡数据接口*/
        public static final String GET_MEMBER_CARD_DETAIL = "https://api.weixin.qq.com/datacube/getcardmembercarddetail?access_token=%s";

    }

    public static class CustomerService {

        /*获取客服账号列表*/
        public static final String GET_ACCOUNT_LIST = "https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=%s";

        /*获取所有客服状态*/
        public static final String GET_ACCOUNT_STATUS = "https://api.weixin.qq.com/cgi-bin/customservice/getonlinekflist?access_token=%s";

        /*添加客服账号*/
        public static final String ADD_ACCOUNT = "https://api.weixin.qq.com/customservice/kfaccount/add?access_token=%s";

        /*邀请绑定客服账号*/
        public static final String INVITE_WORKER = "https://api.weixin.qq.com/customservice/kfaccount/inviteworker?access_token=%s";

        /*设置客服信息*/
        public static final String SET_ACCOUNT_INFO = "https://api.weixin.qq.com/customservice/kfaccount/update?access_token=%s";

        /*上传客服头像*/
        public static final String UPLOAD_ACCOUNT_HEAD_IMG = "https://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=%s&kf_account=%s";

        /*删除客服账号*/
        public static final String DELETE_ACCOUNT = "https://api.weixin.qq.com/customservice/kfaccount/del?access_token=%s&kf_account=%s";

        private CustomerService() {}
    }

    private ApiUrl() {}
}
