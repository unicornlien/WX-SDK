package cn.topcode.unicorn.wxsdk.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 微信公共响应结果
 * @author Unicorn Lien
 * 2017年3月28日 下午10:52:46 创建
 */
@Data
public class Result implements Serializable {

    /*错误码*/
    private Integer errcode = 0;
    
    /*错误消息*/
    private String errmsg = "请求成功";

}
