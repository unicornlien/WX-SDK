package cn.topcode.unicorn.wxsdk.base;

/**
 * 全局响应码
 * @author Unicorn Lien
 * 2016年12月4日 上午12:11:50 创建
 */
public class WXResponseCode {

    //     系统繁忙，此时请开发者稍候再试
    public static final int CODE_M_1_SYSTEM_BUSY = -1;                                                               
    
    //  请求成功
    public static final int CODE_0_SUCCESS = 0;                                                                        
    
    public static final int CODE_40001_APP_SECRET_ERR = 40001;                                                     //   获取access_token时AppSecret错误，或者access_token无效。请开发者认真比对AppSecret的正确性，或查看是否正在为恰当的公众号调用接口
    
    public static final int CODE_40002_ILLEGAL_TOKEN_TYPE = 40002;                                             //       不合法的凭证类型
    
    public static final int CODE_40003_ILLEGAL_OPEN_ID = 40003;                                                   //       不合法的OpenID，请开发者确认OpenID（该用户）是否已关注公众号，或是否是其他公众号的OpenID
    
    public static final int CODE_40004_ILLEGAL_MEDIA_FILE_TYPE = 40004;                                     //       不合法的媒体文件类型
    
    public static final int CODE_40005_ILLEGAL_FILE_TYPE = 40005;                                                  //   不合法的文件类型
    
    public static final int CODE_40006_ILLEGAL_FILE_SIZE = 40006;                                                   //   不合法的文件大小
    
    public static final int CODE_40007_ILLEGAL_MEDIA_FILE_ID = 40007;                                          //   不合法的媒体文件id
    
    public static final int CODE_40008_ILLEGAL_MESSAGE_TYPE = 40008;                                           //   不合法的消息类型
    
    public static final int CODE_40009_ILLEGAL_IMAGE_SIZE = 40009;                                                 //     不合法的图片文件大小
    
    public static final int CODE_40010_ILLEGAL_AUDIO_SIZE = 40010;                                                 //   不合法的语音文件大小
    
    public static final int CODE_40011_ILLEGAL_VIDEO_SIZE = 40011;                                                 //   不合法的视频文件大小
    
    public static final int CODE_40012_ILLEGAL_THUMBNAIL_SIZE = 40012;                                         //   不合法的缩略图文件大小
    
    public static final int CODE_40013_ILLEGAL_APPID = 40013;                                                          //   不合法的AppID，请开发者检查AppID的正确性，避免异常字符，注意大小写
    
    public static final int CODE_40014_ILLEGAL_ACCESS_TOKEN = 40014;                                            //   不合法的access_token，请开发者认真比对access_token的有效性（如是否过期），或查看是否正在为恰当的公众号调用接口
    
    public static final int CODE_40015_ILLEGAL_MENU_TYPE = 40015;                                                  //   不合法的菜单类型
    
    public static final int CODE_40016_ILLEGAL_BUTTON_QUANTITY = 40016;                                     //   不合法的按钮个数
    
    public static final int CODE_40017_ILLEGAL_BUTTON_QUANTITY_2 = 40017;                                  //   不合法的按钮个数
    
    public static final int CODE_40018_ILLEGAL_BUTTON_NAME_LENGTH = 40018;                             //   不合法的按钮名字长度
    
    public static final int CODE_40019_ILLEGAL_BUTTON_KEY_LENGTH = 40019;                                  //   不合法的按钮KEY长度
    
    public static final int CODE_40020_ILLEGAL_BUTTON_URL_LENGTH = 40020;                                  //   不合法的按钮URL长度
    
    public static final int CODE_40021_ILLEGAL_MENU_VERSION = 40021;                                           //   不合法的菜单版本号
    
    public static final int CODE_40022_ILLEGAL_SUB_MENU_LEVEL = 40022;                                          //     不合法的子菜单级数
    
    public static final int CODE_40023_ILLEGAL_SUB_MENU_QUANTITY = 40023;                                  //        不合法的子菜单按钮个数
    
    public static final int CODE_40024_ILLEGAL_SUB_MENU_BUTTON_TYPE = 40024;                               //        不合法的子菜单按钮类型
    
    public static final int CODE_40025_ILLEGAL_SUB_MENU_BUTTON_NAME_LENGTH = 40025;                 //        不合法的子菜单按钮名字长度
    
    public static final int CODE_40026_ILLEGAL_SUB_MENU_BUTTON_KEY_LENGTH = 40026;                     //        不合法的子菜单按钮KEY长度
    
    public static final int CODE_40027_ILLEGAL_SUB_MENU_BUTTON_URL_LENGTH = 40027;                     //        不合法的子菜单按钮URL长度
    
    public static final int CODE_40028_ILLEGAL_CUSTOM_MENU_USER = 40028;                                       //        不合法的自定义菜单使用用户
    
    public static final int CODE_40029_ILLEGAL_OAUTH_CODE = 40029;                                                     //        不合法的oauth_code
    
    public static final int CODE_40030_ILLEGAL_REFRESH_TOKEN = 40030;                                              //        不合法的refresh_token
    
    public static final int CODE_40031_ILLEGAL_OPEN_ID_LIST = 40031;                                                   //        不合法的openid列表
    
    public static final int CODE_40032_ILLEGAL_OPEN_ID_LIST_LENGTH = 40032;                                    //        不合法的openid列表长度
    
    public static final int CODE_40033_ILLEGAL_REQUEST_CHAR = 40033;                                               //        不合法的请求字符，不能包含\ u xxxx格式的字符
    
    public static final int CODE_40035_ILLEGAL_ARGUMENT = 40035;                                                    //        不合法的参数
    
    public static final int CODE_40038_ILLEGAL_REQUEST_FORMAT = 40038;                                             //        不合法的请求格式
    
    public static final int CODE_40039_ILLEGAL_URL_LENGTH = 40039;                                                      //        不合法的URL长度
    
    public static final int CODE_40050_ILLEGAL_GROUP_ID = 40050;                                                           //        不合法的分组id
    
    public static final int CODE_40051_ILLEGAL_GROUP_NAME = 40051;                                                     //        分组名字不合法
    
    public static final int CODE_40117_ILLEGAL_GROUP_NAME_2 = 40117;                                               //        分组名字不合法
    
    public static final int CODE_40118_ILLEGAL_MEDIA_ID_LENGTH = 40118;                                        //        media_id大小不合法
    
    public static final int CODE_40119_BUTTON_TYPE_ERROR = 40119;                                                      //        button类型错误
    
    public static final int CODE_40120_BUTTON_TYPE_ERROR_2 = 40120 ;                                               //       button类型错误
    
    public static final int CODE_40121_ILLEGAL_MEDIA_ID = 40121;                                                           //        不合法的media_id类型
    public static final int CODE_40132_ILLEGAL_WECHAT = 40132;                                                             //        微信号不合法
    
    public static final int CODE_40137_IMAGE_FORMAT_NOT_SUPPORT = 40137;                                       //        不支持的图片格式
    
    public static final int CODE_41001_LACK_ACCESS_TOKEN = 41001;                                                      //        缺少access_token参数
    
    public static final int CODE_41002_LACK_APPID = 41002;                                                             //        缺少appid参数
    
    public static final int CODE_41003_LACK_REFRESH_TOKEN = 41003;                                                 //        缺少refresh_token参数
    
    public static final int CODE_41004_LACK_SECRET = 41004;                                                     //        缺少secret参数
    
    
    public static final int CODE_41005_LACK_MEDIA_FILE_DATA = 41005;                                                       //        缺少多媒体文件数据
    public static final int CODE_41006_LACK_MEDIA_ID = 41006;                                                                       //        缺少media_id参数
    public static final int CODE_41007_LACK_SUB_MENU_DATA = 41007;                                                                 //        缺少子菜单数据
    
    public static final int CODE_41008_LACK_OAUTH_CODE = 41008;                                                                //        缺少oauth code
    
    public static final int CODE_41009_LACK_OPENID = 41009;                                                                                 //        缺少openid
    
    public static final int CODE_42001_ACCESS_TOKEN_OVER_TIME = 42001;                                          //        access_token超时，请检查access_token的有效期，请参考基础支持-获取access_token中，对access_token的详细机制说明
    
    public static final int CODE_42002_REFRESH_TOKEN_OVER_TIME = 42002 ;                                                   //       refresh_token超时
    
    public static final int CODE_42003_OAUTH_CODE_OVER_TIME = 42003 ;                                                  //       oauth_code超时
    
    public static final int CODE_42007_ACCESSTOKEN_AND_REFRESHTOKEN_INVALID = 42007 ;                                       //       用户修改微信密码，accesstoken和refreshtoken失效，需要重新授权
    
    
    public static final int CODE_43001_REQUIRED_GET = 43001 ;                                                                           //       需要GET请求
    public static final int CODE_43002_REQUIRED_POST = 43002 ;                                                                   //       需要POST请求
    
    public static final int CODE_43003_REQUIRED_HTTPS = 43003 ;                                                                 //       需要HTTPS请求
    
    public static final int CODE_43004_REQUIRED_RECEIVER_ATTENTION = 43004 ;    //       需要接收者关注
    
    public static final int CODE_43005_REQUIRED_FRIENDLY = 43005 ;    //       需要好友关系
    
    public static final int CODE_44001_MEDIA_FILE_IS_NULL = 44001 ;    //       多媒体文件为空
    
    public static final int CODE_44002_POST_DATA_IS_NULL = 44002 ;    //       POST的数据包为空
    
    public static final int CODE_44003_IMAGE_TEXT_IS_NULL = 44003 ;    //       图文消息内容为空
    
    public static final int CODE_44004_TEXT_IS_NULL = 44004 ;    //       文本消息内容为空
    
    public static final int CODE_45001_MEDIA_FILE_SIZE_EXCEED_LIMIT = 45001 ;    //       多媒体文件大小超过限制
    
    public static final int CODE_45002_MESSAGE_LENGTH_EXCEED_LIMIT = 45002;    //        消息内容超过限制
    
    public static final int CODE_45003_TITLE_LENGTH_EXCEED_LIMIT = 45003;    //        标题字段超过限制
    
    public static final int CODE_45004_DESCRIPTION_LENGTH_EXCEED_LIMIT = 45004;    //        描述字段超过限制
    
    public static final int CODE_45005_LINK_LENGTH_EXCEED_LIMIT = 45005;    //        链接字段超过限制
    
    public static final int CODE_45006_IMAGE_LINK_LENGTH_EXCEED_LIMIT = 45006;    //        图片链接字段超过限制
    
    
    public static final int CODE_45007_AUDIO_LENGTH_EXCEED_LIMIT = 45007;    //        语音播放时间超过限制
    public static final int CODE_45008_IMAGE_TEXT_LENGTH_EXCEED_LIMIT = 45008;    //        图文消息超过限制
    
    public static final int CODE_45009_INTERFACE_CALL_EXCEED_LIMIT = 45009;    //        接口调用超过限制
    
    public static final int CODE_45010_MENU_QUANTITY_EXCEED_LIMIT = 45010;    //        创建菜单个数超过限制
    
    public static final int CODE_45015_REPLY_TIME_EXCEED_LIMIT = 45015;    //        回复时间超过限制
    
    public static final int CODE_45016_SYSTEM_GROUP_BAN_MODIFY = 45016;    //        系统分组，不允许修改
    
    public static final int CODE_45017_GROUP_NAME_LENGTH_TO_LONG = 45017;    //        分组名字过长
    
    
    public static final int CODE_45018_GROUP_QUANTITY_EXCEED_LIMIT = 45018;    //        分组数量超过上限
    public static final int CODE_45047_CUSTOMER_SERVICE_INTERFACE_CALL_EXCEED_LIMIT = 45047;    //        客服接口下行条数超过上限
    
    public static final int CODE_46001_NO_EXIST_MEDIA_DATA = 46001;    //        不存在媒体数据
    
    public static final int CODE_46002_NO_EXIST_MENU_VERSION = 46002;    //        不存在的菜单版本
    
    public static final int CODE_46003_NO_EXIST_MENU_DATA = 46003 ;    //       不存在的菜单数据
    
    public static final int CODE_46004_NO_EXIST_USER = 46004;    //        不存在的用户
    
    public static final int CODE_47001_PARSE_XML_JSON_ERR = 47001;    //        解析JSON/XML内容错误
    
    public static final int CODE_48001_API_UNAUTHORIZED = 48001;    //        api功能未授权，请确认公众号已获得该接口，可以在公众平台官网-开发者中心页中查看接口权限
    
    
    public static final int CODE_48004_API_BAN = 48004 ;    //       api接口被封禁，请登录mp.weixin.qq.com查看详情
    public static final int CODE_50001_USER_API_UNAUTHORIZED = 50001;    //        用户未授权该api
    
    public static final int CODE_50002_USER_LIMIT = 50002;    //        用户受限，可能是违规后接口被封禁
    
    public static final int CODE_61451_INVALID_PARAMETER = 61451;    //        参数错误(invalid parameter)
    
    public static final int CODE_61452_INVALID_KF_ACCOUNT = 61452;    //        无效客服账号(invalid kf_account)
    
    public static final int CODE_61453_KF_ACCOUNT_EXSITED = 61453;    //        客服帐号已存在(kf_account exsited)
    
    public static final int CODE_61454_INVALID_KF_ACOUNT_LENGTH = 61454;    //        客服帐号名长度超过限制(仅允许10个英文字符，不包括@及@后的公众号的微信号)(invalid kf_acount length)
    
    public static final int CODE_61455_ILLEGAL_CHARACTER_IN_KF_ACCOUNT = 61455;    //        客服帐号名包含非法字符(仅允许英文+数字)(illegal character in kf_account)
    
    public static final int CODE_61456_KF_ACCOUNT_COUNT_EXCEEDED_10 = 61456;    //        客服帐号个数超过限制(10个客服账号)(kf_account count exceeded)
    
    public static final int CODE_61457_INVALID_HEAD_FILE_TYPE = 61457;    //        无效头像文件类型(invalid file type)
    
    public static final int CODE_61450_SYSTEM_ERROR = 61450;    //        系统错误(system error)
    
    public static final int CODE_61500_DATE_FORMAT_ERROR = 61500;    //        日期格式错误
    
    public static final int CODE_65301_MENU_ID_CUSTOM_NO_EXIST = 65301;    //        不存在此menuid对应的个性化菜单
    
    public static final int CODE_65302_NO_ACCORDINGLY_USER = 65302;    //        没有相应的用户
    
    public static final int CODE_65303_NO_DEFAULT_MENU = 65303;    //        没有默认菜单，不能创建个性化菜单
    
    public static final int CODE_65304_MATCH_RULE_EMPTY = 65304;    //        MatchRule信息为空
    
    public static final int CODE_65305_CUSTOM_MENU_QUANTITY_LIMIT = 65305;    //        个性化菜单数量受限
    
    public static final int CODE_65306_ACCOUNT_CUSTOM_MENU_NOT_SUPPORT = 65306;    //        不支持个性化菜单的帐号
    
    public static final int CODE_65307_CUSTOM_MENU_INFO_EMPTY = 65307;    //        个性化菜单信息为空
    
    public static final int CODE_65308_NO_RESPONSE_TYPE_BUTTON = 65308;    //        包含没有响应类型的button
    
    public static final int CODE_65309_CUSTOM_MENU_IS_CLOSED = 65309;    //        个性化菜单开关处于关闭状态
    
    public static final int CODE_65310_COUNTRY_INFO_IS_EMPTY = 65310;    //        填写了省份或城市信息，国家信息不能为空
    
    public static final int CODE_65311_PROVINCE_INFO_IS_EMPTY = 65311;    //        填写了城市信息，省份信息不能为空
    
    public static final int CODE_65312_ILLEGAL_COUNTRY_INFO = 65312;    //        不合法的国家信息
    
    public static final int CODE_65313_ILLEGAL_PROVINCE_INFO = 65313;    //        不合法的省份信息
    
    public static final int CODE_65314_ILLEGAL_CITY_INFO = 65314;    //        不合法的城市信息
    
    public static final int CODE_65316_FORWARD_URL_OVER_3 = 65316;    //        该公众号的菜单设置了过多的域名外跳（最多跳转到3个域名的链接）
    
    public static final int CODE_65317_ILLEGAL_URL = 65317;    //        不合法的URL

    public static final int CODE_65400_ILLEGAL_KF_ACCOUNT_API = 65400;             //	API不可用，即没有开通/升级到新版客服功能

    public static final int CODE_65401_ILLEGAL_KF_ACCOUNT = 65401;	            //  无效客服帐号

    public static final int CODE_65403_ILLEGAL_KF_ACCOUNT_NICKNAME = 65403;	            //  客服昵称不合法

    public static final int CODE_65404_ILLEGAL_KF_ACCOUNT_ACC = 65404;	            //  客服帐号不合法

    public static final int CODE_65405 = 65405;            //  帐号数目已达到上限，不能继续添加

    public static final int CODE_65406_KF_ACCOUNT_EXIST = 65406;	            //  已经存在的客服帐号

    public static final int CODE_65407 = 65407;              //  邀请对象已经是该公众号客服

    public static final int CODE_65408 = 65408;              //  本公众号已经有一个邀请给该微信

    public static final int CODE_65409 = 65409;             //  无效的微信号

    public static final int CODE_65410 = 65410;             //  邀请对象绑定公众号客服数达到上限（目前每个微信号可以绑定5个公众号客服帐号）

    public static final int CODE_65411 = 65411;             //  该帐号已经有一个等待确认的邀请，不能重复邀请

    public static final int CODE_65412 = 65412;             //  该帐号已经绑定微信号，不能进行邀请

    public static final int CODE_40005 = 40005;             //  不支持的媒体类型

    public static final int CODE_40009 = 40009;             //  媒体文件长度不合法

    
    public static final int CODE_9001001_ILLEGAL_POST_ARGUMENT = 9001001;    //      POST数据参数不合法
    
    public static final int CODE_9001002_REMOTE_SERVICE_INVALID = 9001002;    //      远端服务不可用
    
    public static final int CODE_9001003_ILLEGAL_TICKET = 9001003;    //      Ticket不合法
    
    public static final int CODE_9001004_GET_SHAKE_AROUND_USER_INFO_FAILED = 9001004;    //      获取摇周边用户信息失败
    
    public static final int CODE_9001005_GET_BUSINESS_INFO_FAILED = 9001005;    //      获取商户信息失败
    
    public static final int CODE_9001006_GET_OPEN_ID_FAILED = 9001006;    //      获取OpenID失败
    
    public static final int CODE_9001007_LACK_UPLOAD_FILE = 9001007;    //      上传文件缺失
    
    public static final int CODE_9001008_ILLEGAL_UPLOAD_FILE_TYPE = 9001008 ;    //     上传素材的文件类型不合法
    
    public static final int CODE_9001009_ILLEGAL_UPLOAD_FILE_SIZE = 9001009 ;    //     上传素材的文件尺寸不合法
    
    public static final int CODE_9001010_UPLOAD_FAILED = 9001010 ;    //     上传失败
    
    public static final int CODE_9001020_ILLEGAL_ACCOUNT = 9001020 ;    //     帐号不合法
    
    public static final int CODE_9001021_BAN_ADD_DEVICE = 9001021 ;    //     已有设备激活率低于50%，不能新增设备
    
    public static final int CODE_9001022_DEVICE_APPLY_MUST_OVER_0 = 9001022 ;    //     设备申请数不合法，必须为大于0的数字
    
    public static final int CODE_9001023_DEVICE_ID_APPLY_EXIST = 9001023 ;    //     已存在审核中的设备ID申请
    
    public static final int CODE_9001024_QUERY_DEVICE_ID_OVER_50_ONCE = 9001024 ;    //     一次查询设备ID数量不能超过50
    
    public static final int CODE_9001025_ILLEGAL_DEVICE_ID = 9001025 ;    //     设备ID不合法
    
    public static final int CODE_9001026_ILLEGAL_PAGE_ID = 9001026 ;    //     页面ID不合法
    
    public static final int CODE_9001027_ILLEGAL_PAGE_ARGUMENT = 9001027 ;    //     页面参数不合法
    
    public static final int CODE_9001028_DELETE_PAGE_ID_OVER_10_ONCE = 9001028;    //      一次删除页面ID数量不能超过10
    
    public static final int CODE_9001029_PAGE_APPLIED_IN_DEVICE  = 9001029;    //      页面已应用在设备中，请先解除应用关系再删除
    
    public static final int CODE_9001030_QUERY_PAGE_ID_OVER_50_ONCE = 9001030;    //      一次查询页面ID数量不能超过50
    
    public static final int CODE_9001031_ILLEGAL_TIME_RANGE = 9001031;    //      时间区间不合法
    
    public static final int CODE_9001032_DEVICE_PAGE_BIND_ARGUMENT_ERR = 9001032;    //      保存设备与页面的绑定关系参数错误
    
    public static final int CODE_9001033_ILLEGAL_STORE_ID = 9001033;    //      门店ID不合法
    
    public static final int CODE_9001034_DEVICE_REMARK_TO_LONG = 9001034 ;    //     设备备注信息过长
    
    public static final int CODE_9001035_ILLEGAL_APPLY_ARGUMENT = 9001035;                               //      设备申请参数不合法
    
    public static final int CODE_9001036_ILLEGAL_BEGIN = 9001036;                                                   //      查询起始值begin不合法

    private WXResponseCode() {}
}