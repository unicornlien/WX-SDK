package cn.topcode.unicorn.wxsdk.base;

import lombok.Data;

/**
 * 公众号信息
 * Created by Unicorn on 2018/3/20.
 */
@Data
public class WechatMpInfo {

    /**
     * 公众号名称
     */
    private String name;

    /**
     * 公众号AppId
     */
    private String appId;

    /**
     * 公众号Secret
     */
    private String secret;

    /**
     * 公众号ID
     */
    private String mpId;

    /**
     * 公众号地址
     */
    private String mpUrl;

    /**
     * 公众号Token
     */
    private String token;

    /**
     * 公众号Token过期秒数
     */
    private Integer tokenExpires;

    /**
     * 最后获取公众号Token时间
     */
    private Long lastGetTokenTimemillis;

    /**
     * 公众号网页JsTicket
     */
    private String jsTicket;

    /**
     * 公众号网页JsTicket过期秒数
     */
    private Integer jsTicketExpires;

    /**
     * 最后获取公众号网页JsTicket时间
     */
    private Long lastGetJsTicketTimemillis;

    /**
     * 开放平台AppId
     */
    private String openAppId;

    /**
     * 开放平台Secret
     */
    private String openSecret;
}
