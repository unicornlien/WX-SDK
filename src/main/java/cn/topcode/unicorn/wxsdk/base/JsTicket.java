package cn.topcode.unicorn.wxsdk.base;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * JsTicket
 * @author Unicorn Lien
 * 2017年3月28日 下午10:52:06 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class JsTicket extends Result {

    private String ticket;
    
    @JSONField(name="expires_in")
    private int expiresIn;

}
