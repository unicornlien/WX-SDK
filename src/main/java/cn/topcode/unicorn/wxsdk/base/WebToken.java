package cn.topcode.unicorn.wxsdk.base;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @author Unicorn
 * @date 2017/4/5
 */
@Data
public class WebToken extends Result {

    @JSONField(name="access_token")
    private String accessToken;

    @JSONField(name="expires_in")
    private Integer expiresIn;

    @JSONField(name="refresh_token")
    private String refreshToken;

    @JSONField(name="openid")
    private String openId;

    @JSONField(name="scope")
    private String scope;

    @JSONField(name="unionid")
    private String unionId;
}
