package cn.topcode.unicorn.wxsdk.base;

import lombok.Data;

/**
 * JS-SDK配置
 * @author Unicorn Lien
 * 2017年3月28日 下午10:48:53 创建
 */
@Data
public class JSSdkConfig {

    private String appId;
    
    /*生成签名时间*/
    private String timestamp;
    
    /*随机字符串*/
    private String nonceStr;
    
    /*签名*/
    private String signature;

}
