package cn.topcode.unicorn.wxsdk.base;

import lombok.Data;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * AccessToken
 * @author Unicorn Lien
 * 2017年3月28日 下午10:53:25 创建
 */
@Data
public class Token {

    @JSONField(name="access_token")
    private String accessToken;
    
    @JSONField(name="expires_in")
    private int expiresIn;

}
