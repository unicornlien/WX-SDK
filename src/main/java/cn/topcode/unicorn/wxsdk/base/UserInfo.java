package cn.topcode.unicorn.wxsdk.base;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import java.util.List;

/**
 * @author Unicorn
 * @date 2017/4/5
 */
@Data
public class UserInfo {

    @JSONField(name="openid")
    private String openId;

    private String nickname;

    private Integer sex;

    private String province;

    private String city;

    private String country;

    @JSONField(name="headimgurl")
    private String headImgUrl;

    private List<String> privilege;

    @JSONField(name="unionid")
    private String unionId;
}
