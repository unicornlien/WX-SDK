package cn.topcode.unicorn.wxsdk.user;

import java.util.List;
import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.user.dto.CreateUserTagResult;
import cn.topcode.unicorn.wxsdk.user.dto.GetBlackListResult;
import cn.topcode.unicorn.wxsdk.user.dto.GetTagListResult;
import cn.topcode.unicorn.wxsdk.user.dto.GetTagUserListResult;
import cn.topcode.unicorn.wxsdk.user.dto.GetUserInfoResult;
import cn.topcode.unicorn.wxsdk.user.dto.GetUserListResult;
import cn.topcode.unicorn.wxsdk.user.dto.GetUserTagResult;

/**
 * 用户管理接口
 * 包括用户标签管理、用户基本信息管理和黑名单管理
 * @author Unicorn Lien
 * 2017年3月28日 下午9:33:50 创建
 */
public interface UserInvoker {
   
    /**
     * 创建标签
     * @author Unicorn Lien
     * 2017年3月28日 下午9:33:08 创建
     * @param mpId      公众号ID
     * @param tagName   标签名
     * @return  请求创建标签结果
     */
    CreateUserTagResult createUserTag(String mpId, String tagName);

    CreateUserTagResult createUserTag(String tagName);
    
    /**
     * 获取公众号已创建的标签
     * @author Unicorn Lien
     * 2017年3月28日 下午9:34:38 创建
     * @return  请求结果
     */
    GetTagListResult getTagList(String mpId);

    GetTagListResult getTagList();
    
    /**
     * 编辑标签
     * @author Unicorn Lien
     * 2017年3月28日 下午9:35:48 创建
     * @param tagId 标签ID
     * @param tagName   标签名称
     * @return  请求结果
     */
    Result editTag(String mpId, Long tagId, String tagName);

    Result editTag(Long tagId, String tagName);
    
    /**
     * 删除标签
     * @author Unicorn Lien
     * 2017年3月28日 下午9:36:13 创建
     * @param tagId 标签ID
     * @return  请求结果
     */
    Result deleteTag(String mpId, Long tagId);

    Result deleteTag(Long tagId);
    
    /**
     * 获取标签下粉丝列表
     * @author Unicorn Lien
     * 2017年3月28日 下午9:36:48 创建
     * @param tagId 标签ID
     * @param nextOpenId    第一个拉取的OPENID，不填默认从头开始拉取
     * @return  请求结果
     */
    GetTagUserListResult getTagUserList(String mpId, Long tagId, String nextOpenId);

    GetTagUserListResult getTagUserList(Long tagId, String nextOpenId);
    
    /**
     * 批量为用户打标签
     * @author Unicorn Lien
     * 2017年3月28日 下午9:38:14 创建
     * @param tagId 标签ID
     * @param openIds   用户openId列表
     * @return  请求结果
     */
    Result batchTag(String mpId, Long tagId, List<String> openIds);

    Result batchTag(Long tagId, List<String> openIds);
    
    /**
     * 批量为用户取消标签
     * @author Unicorn Lien
     * 2017年3月28日 下午9:38:56 创建
     * @param tagId 标签ID
     * @param openIds   用户openId列表
     * @return   请求结果
     */
    Result batchCancelTag(String mpId, Long tagId, List<String> openIds);

    Result batchCancelTag(Long tagId, List<String> openIds);

    /**
     * 批量打标签，没有50个openid的限制
     * @param tagId
     * @param openIds
     * @return
     */
    String batchTagUnlimit(String mpId, Long tagId, List<String> openIds);

    String batchTagUnlimit(Long tagId, List<String> openIds);

    /**
     * 批量取消标签，没有50个openid的限制
     * @param tagId
     * @param openIds
     * @return
     */
    String batchCancelTagUnlimit(String mpId, Long tagId, List<String> openIds);

    String batchCancelTagUnlimit(Long tagId, List<String> openIds);

    /**
     * 获取用户身上的标签列表
     * @author Unicorn Lien
     * 2017年3月28日 下午9:39:30 创建
     * @param openId    用户openId
     * @return  请求结果
     */
    GetUserTagResult getUserTag(String mpId, String openId);

    GetUserTagResult getUserTag(String openId);
    
    /**
     * 获取用户列表
     * @author Unicorn Lien
     * 2017年3月28日 下午9:40:05 创建
     * @param nextOpenId 第一个拉取的OPENID，不填默认从头开始拉取
     * @return  请求结果
     */
    GetUserListResult getUserList(String mpId, String nextOpenId);

    GetUserListResult getUserList(String nextOpenId);
    
    /**
     * 设置用户备注名
     * @author Unicorn Lien
     * 2017年3月28日 下午9:32:02 创建
     * @param openId    用户openId
     * @param remark    用户备注名
     * @return  请求结果
     */
    Result setRemark(String mpId, String openId, String remark);

    Result setRemark(String openId, String remark);
    
    /**
     * 获取用户基本信息
     * @author Unicorn Lien
     * 2017年3月28日 下午9:41:52 创建
     * @param openId    用户openId
     * @param language 国家地区语言
     * @return  请求结果
     */
    GetUserInfoResult getUserInfo(String mpId, String openId, String language);

    GetUserInfoResult getUserInfo(String openId, String language);
    
    /**
     * 获取公众号黑名单列表
     * @author Unicorn Lien
     * 2017年3月28日 下午9:44:02 创建
     * @param beginOpenId 第一个openId，当 begin_openid 为空时，默认从开头拉取
     * @return  请求结果
     */
    GetBlackListResult getBlackList(String mpId, String beginOpenId);

    GetBlackListResult getBlackList(String beginOpenId);
    
    /**
     * 拉黑用户
     * @author Unicorn Lien
     * 2017年3月28日 下午9:45:28 创建
     * @param openIds   用户openId列表
     * @return  请求结果
     */
    Result markBlack(String mpId, List<String> openIds);

    Result markBlack(List<String> openIds);
    
    /**
     * 取消拉黑
     * @author Unicorn Lien
     * 2017年3月28日 下午9:46:04 创建
     * @param openIds   用户openId列表
     * @return  请求结果
     */
    Result markWhite(String mpId, List<String> openIds);

    Result markWhite(List<String> openIds);
}
