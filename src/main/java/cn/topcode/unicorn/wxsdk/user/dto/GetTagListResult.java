package cn.topcode.unicorn.wxsdk.user.dto;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetTagListResult extends Result {

    private List<Tag> tags;
    
}
