package cn.topcode.unicorn.wxsdk.user.dto;

import java.util.List;

import lombok.Data;

@Data
public class TagUserList {

    private List<String> openid;

}
