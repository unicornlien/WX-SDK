package cn.topcode.unicorn.wxsdk.user.dto;

import lombok.Data;
import com.alibaba.fastjson.annotation.JSONField;

@Data
public class ReqGetTagUserList {

    private Long tagid;
    
    @JSONField(name="next_openid")
    private String nextOpenid;

}
