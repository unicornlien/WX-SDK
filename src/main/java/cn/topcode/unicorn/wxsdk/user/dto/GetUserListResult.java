package cn.topcode.unicorn.wxsdk.user.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetUserListResult extends Result {

    private int total;
    
    private int count;
    
    private OpenIdList data;
    
    @JSONField(name="next_openid")
    private String nextOpenid;

}
