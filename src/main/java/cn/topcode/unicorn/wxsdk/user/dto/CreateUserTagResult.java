package cn.topcode.unicorn.wxsdk.user.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

@Data
@EqualsAndHashCode(callSuper=false)
public class CreateUserTagResult extends Result {

    private Tag tag;

}
