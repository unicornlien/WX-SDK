package cn.topcode.unicorn.wxsdk.user.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetUserInfoResult extends Result {

    private int subscribe;
    
    private String openId;
    
    private String nickname;
    
    private int sex;
    
    private String language;
    
    private String city;
    
    private String province;
    
    private String country;
    
    private String headimgurl;
    
    @JSONField(name="subscribe_time")
    private long subscribeTime;
    
    private String unionid;
    
    private String remark;
    
    private int groupid;

}
