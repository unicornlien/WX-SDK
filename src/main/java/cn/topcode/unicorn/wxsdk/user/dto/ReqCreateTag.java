package cn.topcode.unicorn.wxsdk.user.dto;

import lombok.Data;

@Data
public class ReqCreateTag {

    private Tag tag;

}
