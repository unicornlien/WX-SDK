package cn.topcode.unicorn.wxsdk.user.dto;

import java.util.List;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class ReqBatchTag {

    private Long tagid;
    
    @JSONField(name="openid_list")
    private List<String> openidList;

}
