package cn.topcode.unicorn.wxsdk.user.dto;

import lombok.Data;

@Data
public class Tag {
    
    private Long id;

    private String name;
    
    private Integer count;

}
