package cn.topcode.unicorn.wxsdk.user.dto;

import java.util.List;
import lombok.Data;

@Data
public class BlackList {

    private List<String> data;

}
