package cn.topcode.unicorn.wxsdk.user.dto;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetUserTagResult extends Result {

    @JSONField(name="tagid_list")
    private List<Long> tagidList;

}
