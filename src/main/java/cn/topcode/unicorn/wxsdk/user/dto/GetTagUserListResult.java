package cn.topcode.unicorn.wxsdk.user.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetTagUserListResult extends Result {

    private Integer count;
    
    private TagUserList data;
    
    @JSONField(name="next_openid")
    private String nextOpenid;
    
}
