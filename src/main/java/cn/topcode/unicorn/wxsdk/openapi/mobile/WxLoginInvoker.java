package cn.topcode.unicorn.wxsdk.openapi.mobile;

import cn.topcode.unicorn.utils.http.HttpUtil;
import cn.topcode.unicorn.wxsdk.openapi.mobile.pojo.LoginResult;

/**
 * Created by Unicorn on 2017/12/13.
 */
public class WxLoginInvoker {

    public static final String URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";

    public LoginResult getAccessToken(String appId, String secret, String code) {
        String url = String.format(URL, appId, secret, code);
        return HttpUtil.getJson(url, LoginResult.class);
    }
}
