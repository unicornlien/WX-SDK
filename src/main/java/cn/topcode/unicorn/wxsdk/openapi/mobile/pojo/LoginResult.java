package cn.topcode.unicorn.wxsdk.openapi.mobile.pojo;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Unicorn on 2017/12/13.
 */
@Data
public class LoginResult extends Result {

    @JSONField(name="access_token")
    private String accessToken;

    @JSONField(name="expires_in")
    private Integer expiresIn;

    @JSONField(name="refresh_token")
    private String refreshToken;

    @JSONField(name="openid")
    private String openId;

    @JSONField(name="scope")
    private String scope;

    @JSONField(name="unionid")
    private String unionId;
}
