package cn.topcode.unicorn.wxsdk.openapi.mobile.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Unicorn on 2017/12/13.
 */
@Data
public class LoginInfo implements Serializable {

    /**
     * 用于换取access_token的code
     */
    private String code;

    /**
     * 请求唯一性标识
     */
    private String state;

    /**
     * 微信客户端当前语言
     */
    private String lang;

    /**
     * 微信用户当前国家信息
     */
    private String country;
}
