package cn.topcode.unicorn.wxsdk.message.template;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetTemplateListResult extends Result {

    @JSONField(name="template_list")
    private List<Template> templateList;

}
