package cn.topcode.unicorn.wxsdk.message.receive.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 点击菜单拉取消息时的事件推送
 * @author Leo Lien
 * 2016年12月4日 上午12:50:55 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ClickMessageMenuEvent extends Event {

    /*事件KEY值，与自定义菜单接口中KEY值对应*/
    private String eventKey;

}
