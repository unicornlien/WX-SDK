package cn.topcode.unicorn.wxsdk.message.receive.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 地理位置消息
 * @author Leo Lien
 * 2016年12月4日 下午3:09:55 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LocationMessage extends GenericMessage {

    /*地理位置维度*/
    @JSONField(name="location_X")
    private double locationX;
    
    /*地理位置经度*/
    @JSONField(name="location_Y")
    private double locationY;
    
    /*地图缩放大小*/
    private int scale;
    
    /*地理位置信息*/
    private String label;
    
}
