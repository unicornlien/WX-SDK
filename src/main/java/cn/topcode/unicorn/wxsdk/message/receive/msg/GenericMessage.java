package cn.topcode.unicorn.wxsdk.message.receive.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public abstract class GenericMessage extends Message {
    
    /*消息id，64位整型*/
    private Long msgId;
    
}
