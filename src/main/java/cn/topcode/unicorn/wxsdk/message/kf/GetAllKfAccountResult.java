package cn.topcode.unicorn.wxsdk.message.kf;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetAllKfAccountResult extends Result {

    @JSONField(name="kf_list")
    private List<KfAccount> kfList;

}
