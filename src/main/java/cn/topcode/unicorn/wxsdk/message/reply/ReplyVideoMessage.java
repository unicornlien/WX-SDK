package cn.topcode.unicorn.wxsdk.message.reply;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@Data
@EqualsAndHashCode(callSuper=false)
@XStreamAlias("xml")
public class ReplyVideoMessage extends ReplyMessage {
    
    @XStreamAlias("MediaId")
    private String mediaId;
    
    @XStreamAlias("Title")
    private String title;
    
    @XStreamAlias("Description")
    private String description;
    
    public ReplyVideoMessage(Message src) {
        super(src);
        this.setMsgType(MSG_TYPE_VIDEO);
    }

}
