package cn.topcode.unicorn.wxsdk.message.receive.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 上报地理位置事件
 * @author Leo Lien
 * 2016年12月4日 上午12:51:12 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LocationEvent extends Event {

    /*地理位置纬度*/
    private double latitude;
    
    /*地理位置经度*/
    private double longitude;
    
    /*地理位置精度*/
    private double precision;

}
