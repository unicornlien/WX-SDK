package cn.topcode.unicorn.wxsdk.message.template;

import java.util.List;

import lombok.Data;

@Data
public class UploadNewsMaterial {
    
    private List<UploadArticle> articles;

}
