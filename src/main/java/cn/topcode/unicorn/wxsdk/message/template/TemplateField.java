package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateField implements Serializable {

    private String value;
    
    private String color;

    public TemplateField(String value, String color) {
        super();
        this.value = value;
        this.color = color;
    }
    
}
