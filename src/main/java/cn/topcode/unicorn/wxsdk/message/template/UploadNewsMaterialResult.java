package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class UploadNewsMaterialResult extends Result {

    private String type;
    
    @JSONField(name="media_id")
    private String mediaId;
    
    @JSONField(name="created_at")
    private long createdAt;

}
