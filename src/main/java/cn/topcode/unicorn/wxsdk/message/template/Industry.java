package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class Industry {

    @JSONField(name="first_class")
    private String firstClass;
    
    @JSONField(name="second_class")
    private String secondClass;

}
