package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class SetIndustry {

    @JSONField(name="industry_id1")
    private String industryId1;
    
    @JSONField(name="industry_id2")
    private String industryId2;

}
