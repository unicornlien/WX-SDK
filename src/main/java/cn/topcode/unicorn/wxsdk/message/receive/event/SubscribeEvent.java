package cn.topcode.unicorn.wxsdk.message.receive.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 订阅/取消订阅事件
 * @author Leo Lien
 * 2016年12月4日 上午12:52:03 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class SubscribeEvent extends Event {

    /*事件KEY值，qrscene_为前缀，后面为二维码的参数值*/
    private String eventKey;
    
    /*二维码的ticket，可用来换取二维码图片*/
    private String ticket;

}
