package cn.topcode.unicorn.wxsdk.message.template;

public class IndustryCode {
    /*IT科技    互联网/电子商务    1*/
    public static final int IT_INTERNET = 1;           
    
    /*IT科技    IT软件与服务 2*/
    public static final int IT_SOFTWARE = 2;  
    
    /*IT科技    IT硬件与设备 3*/
    public static final int IT_HARDSOFT = 3;      
    
    /*IT科技    电子技术    4*/
    public static final int IT_ELECTRON = 4; 
    
    /*IT科技    通信与运营商  5*/
    public static final int IT_COMM = 5;
    
    /*IT科技    网络游戏    6*/
    public static final int IT_GAME = 6;
    
    /*金融业 银行  7*/
    public static final int FINANCE_BANK = 7;     
    
    /*金融业 基金|理财|信托    8*/
    public static final int FINANCE_FOUNDATION = 8; 
    
    /*金融业 保险  9*/
    public static final int FINANCE_INSURANCE = 9;     
    
    /*餐饮  餐饮  10*/
    public static final int CATERING_TRADE = 10;
    
    /*酒店旅游    酒店  11*/
    public static final int HOTEL = 11;                                    
    
    /*  酒店旅游    旅游  12*/
    public static final int TRIP = 12;
    
    /*运输与仓储   快递  13*/
    public static final int DELIVERY = 13;
    
    /*运输与仓储   物流  14*/
    public static final int LOGISTICAL = 14;
    
    /*运输与仓储   仓储  15*/
    public static final int STORAGE = 15;
    
    /*教育  培训  16*/
    public static final int TRAIN = 16;
    
    /*教育  院校  17*/
    public static final int SCHOOL = 17;
    
    /*政府与公共事业 学术科研    18*/
    public static final int SCIENTIFIC_RESEARCH = 18;
    
    /*政府与公共事业 交警  19*/
    public static final int TRAFFIC_POLICE = 19;
    
    /*政府与公共事业 博物馆 20*/
    public static final int MUSEUM = 20;
    
    /*政府与公共事业 公共事业|非盈利机构  21*/
    public static final int COMMONWEAL = 21;
    
    /*医药护理    医药医疗    22*/
    public static final int MEDICAL_TREATMENT = 22;
    
    /*医药护理    护理美容    23*/
    public static final int COSMETOLOGY = 23;
    
    /*医药护理    保健与卫生   24*/
    public static final int HEALTH_CARE = 24;
    
    /*交通工具    汽车相关    25*/
    public static final int CAR = 25;
    
    /*交通工具    摩托车相关   26*/
    public static final int MOTORCYCLE = 26;
    
    /*交通工具    火车相关    27*/
    public static final int TRAFFIC_TRAIN = 27;
    
    /*交通工具    飞机相关    28*/
    public static final int AIRPLANE = 28;
    
    /*房地产 建筑  29*/
    public static final int BUILDING = 29;
    
    /*房地产 物业  30*/
    public static final int PROPERTY = 30;
    
    /*消费品 消费品 31*/
    public static final int CONSUMABLE = 31;
    
    /*商业服务    法律  32*/
    public static final int LAW = 32;
    
    /*商业服务    会展  33*/
    public static final int EXHIBITION = 33;
    
    /*商业服务    中介服务    34*/
    public static final int AGENCY = 34;
    
    /*商业服务    认证  35*/
    public static final int AUTHENTICATION = 35;
    
    /*商业服务    审计  36*/
    public static final int AUDIT = 36;
    
    /*文体娱乐    传媒  37*/
    public static final int MEDIA = 37;
    
    /*文体娱乐    体育  38*/
    public static final int SPORT = 38;
    
    /*文体娱乐    娱乐休闲    39*/
    public static final int PLEASURE = 39;
    
    /*印刷  印刷  40*/
    public static final int PRINT = 40;
    
    /*其它  其它  41*/
    public static final int OTHER = 41;
    
    private IndustryCode() {}
}
