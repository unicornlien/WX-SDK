package cn.topcode.unicorn.wxsdk.message.reply;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@Data
@EqualsAndHashCode(callSuper=false)
@XStreamAlias("xml")
public class ReplyMusicMessage extends ReplyMessage {

    @XStreamAlias("Title")
    private String title;
    
    @XStreamAlias("Description")
    private String description;
    
    @XStreamAlias("MusicURL")
    private String musicUrl;
    
    @XStreamAlias("HQMusicURL")
    private String hqMusicUrl;
    
    @XStreamAlias("ThumbMediaId")
    private String thumbMediaId;

    public ReplyMusicMessage(Message src) {
        super(src);
        this.setMsgType(MSG_TYPE_MUSIC);
    }

}
