package cn.topcode.unicorn.wxsdk.message.receive.event;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;

@Data
@EqualsAndHashCode(callSuper=false)
public class Event extends Message {
    
    /*订阅事件*/
    public static final String EVENT_SUBSCRIBE = "subscribe";
    /*取消订阅事件*/
    public static final String EVENT_UNSUBSCRIBE = "unsubscribe";
    /*扫描带参数二维码事件*/
    public static final String EVENT_SCAN = "SCAN";
    /*上报地理位置事件*/
    public static final String EVENT_LOCATION = "LOCATION";
    /*点击菜单拉取消息事件*/
    public static final String EVENT_CLICK = "CLICK";
    /*点击菜单跳转链接事件*/
    public static final String EVENT_VIEW = "VIEW";
    
    /*扫码推事件的事件推送*/
    public static final String EVENT_SCANCODE_PUSH = "scancode_push";
    
    /*扫码推事件且弹出“消息接收中”提示框的事件推送*/
    public static final String SCANCODE_WAITMSG = "scancode_waitmsg";
    
    /*弹出系统拍照发图的事件推送*/
    public static final String PIC_SYSPHOTO = "pic_sysphoto";
    
    /*弹出拍照或者相册发图的事件推送*/
    public static final String PIC_PHOTO_OR_ALBUM = "pic_photo_or_album";
    
    /*弹出微信相册发图器的事件推送*/
    public static final String PIC_WEIXIN = "pic_weixin";
    
    /*弹出地理位置选择器的事件推送*/
    public static final String LOCATION_SELECT = "location_select";
    
    /*资质认证成功*/
    public static final String EVENT_QUALIFICATION_VERIFY_SUCCESS = "qualification_verify_success";
    /*资质认证失败*/
    public static final String EVENT_QUALIFICATION_VERIFY_FAIL = "qualification_verify_fail";
    /*名称认证成功*/
    public static final String EVENT_NAMING_VERIFY_SUCCESS = "naming_verify_success";
    /*名称认证失败*/
    public static final String EVENT_NAMING_VERIFY_FAIL = "naming_verify_fail";
    /*年审通知*/
    public static final String EVENT_ANNUAL_RENEW = "annual_renew";
    /*认证失效通知*/
    public static final String EVENT_VERIFY_EXPIRED = "verify_expired";
    
    /*事件*/
    private String event;

}
