package cn.topcode.unicorn.wxsdk.message.reply;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@Data
@EqualsAndHashCode(callSuper=false)
@XStreamAlias("xml")
public class ReplyImageMessage extends ReplyMessage {

    @XStreamAlias("MediaId")
    private String mediaId;

    public ReplyImageMessage(Message src) {
        super(src);
        this.setMsgType(MSG_TYPE_IMAGE);
    }

}
