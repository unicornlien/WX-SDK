package cn.topcode.unicorn.wxsdk.message.kf;

import lombok.Data;
import cn.topcode.unicorn.wxsdk.customerservice.message.KfWxCardMessage;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class ReqSendKfMessage {

    @JSONField(name="touser")
    private String toUser;
    
    @JSONField(name="msgtype")
    private String msgType;
    
    private KfTextMessage text;
    
    @JSONField(name="customservice")
    private CustomService customService;
    
    private KfWxCardMessage wxcard;
    
    private KfImageOrVoiceMessage mpnews;
    
    private KfNewsMessage news;
    
    private KfMusicMessage music;
    
    private KfVideoMessage video;
    
    private KfImageOrVoiceMessage voice;
    
    private KfImageOrVoiceMessage image;
   
    
}
