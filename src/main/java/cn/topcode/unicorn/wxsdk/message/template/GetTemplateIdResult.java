package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetTemplateIdResult extends Result {

    @JSONField(name="template_id")
    private String templateId;

}
