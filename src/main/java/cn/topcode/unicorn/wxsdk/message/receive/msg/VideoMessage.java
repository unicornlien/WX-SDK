package cn.topcode.unicorn.wxsdk.message.receive.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 视频消息
 * @author Leo Lien
 * 2016年12月4日 下午3:07:55 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class VideoMessage extends GenericMessage {

    /*视频消息媒体id，可以调用多媒体文件下载接口拉取数据。*/
    private String mediaId;
    
    /*视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。*/
    private String thumbMediaId;

}
