package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class SendTemplateMessage {

    private String touser;
    
    @JSONField(name="template_id")
    private String templateId;
    
    private String url;
    
    private Object data;

}
