package cn.topcode.unicorn.wxsdk.message.reply;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.customerservice.message.Article;
import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@Data
@EqualsAndHashCode(callSuper=false)
@XStreamAlias("xml")
public class ReplyNewsMessage extends ReplyMessage {

    
    @XStreamAlias("ArticleCount")
    private int articleCount;
    
    @XStreamAlias("Articles")
    private List<Article> articles;
    
    public ReplyNewsMessage(Message src) {
        super(src);
        this.setMsgType(MSG_TYPE_NEWS);
    }

}
