package cn.topcode.unicorn.wxsdk.message.receive.msg;

import java.util.Date;

import lombok.Data;

@Data
public class Message {
    
    /*文本消息*/
    public static final String MSG_TYPE_TEXT = "text";

    /*图片消息*/
    public static final String MSG_TYPE_IMAGE = "image";

    /*语音消息*/
    public static final String MSG_TYPE_VOICE = "voice";

    /*视频消息*/
    public static final String MSG_TYPE_VIDEO = "video";

    /*小视频消息*/
    public static final String MSG_TYPE_SHORT_VIDEO = "shortvideo";

    /*地理位置消息*/
    public static final String MSG_TYPE_LOCATION = "location";

    /*链接消息*/
    public static final String MSG_TYPE_LINK = "link";

    /*图文消息*/
    public static final String MSG_TYPE_NEWS = "news";
    
    /*事件*/
    public static final String MSG_TYPE_EVENT = "event";

    /*开发者微信号*/
    private String toUserName;
    
    /*发送方帐号（一个OpenID）*/
    private String fromUserName;
    
    /*消息创建时间 （整型）*/
    private Date createTime;
    
    /*消息类型*/
    private String msgType;
    
}
