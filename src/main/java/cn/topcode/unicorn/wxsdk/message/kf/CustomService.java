package cn.topcode.unicorn.wxsdk.message.kf;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class CustomService {

    @JSONField(name="kf_account")
    private String kfAccount;

}
