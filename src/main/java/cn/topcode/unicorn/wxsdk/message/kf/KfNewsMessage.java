package cn.topcode.unicorn.wxsdk.message.kf;

import java.util.List;

import lombok.Data;
import cn.topcode.unicorn.wxsdk.customerservice.message.Article;

@Data
public class KfNewsMessage {

    private List<Article> articles;

}
