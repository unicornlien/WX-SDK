package cn.topcode.unicorn.wxsdk.message.kf;

import lombok.Data;

@Data
public class KfTextMessage {

    private String content;

}
