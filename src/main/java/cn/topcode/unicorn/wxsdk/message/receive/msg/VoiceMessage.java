package cn.topcode.unicorn.wxsdk.message.receive.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 语音消息
 * @author Leo Lien
 * 2016年12月4日 下午3:07:17 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class VoiceMessage extends GenericMessage {

    /*语音消息媒体id，可以调用多媒体文件下载接口拉取数据。*/
    private String mediaId;
    
    /*语音格式，如amr，speex等*/
    private String format;
    
    /*语音识别结果*/
    @JSONField(name="Recognition")
    private String recognition;
    
}
