package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

@Data
@EqualsAndHashCode(callSuper=false)
public class SendTemplateMessageResult extends Result {

    private Long msgid;

}
