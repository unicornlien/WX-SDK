package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class UploadArticle {

    @JSONField(name="thumb_media_id")
    private String thumbMediaId;
    
    private String author;
    
    private String title;
    
    @JSONField(name="content_source_url")
    private String contentSourceUrl;
    
    private String content;
    
    private String digest;
    
    @JSONField(name="show_cover_pic")
    private int showCoverPic;

}
