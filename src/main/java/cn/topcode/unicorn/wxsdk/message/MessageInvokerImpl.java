package cn.topcode.unicorn.wxsdk.message;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import cn.topcode.unicorn.utils.http.HttpUtil;
import cn.topcode.unicorn.wxsdk.WXContext;
import cn.topcode.unicorn.wxsdk.base.ApiUrl;
import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.message.kf.GetAllKfAccountResult;
import cn.topcode.unicorn.wxsdk.message.kf.ReqCreateKfAccount;
import cn.topcode.unicorn.wxsdk.message.kf.ReqSendKfMessage;
import cn.topcode.unicorn.wxsdk.message.template.GetIndustryResult;
import cn.topcode.unicorn.wxsdk.message.template.GetTemplateIdResult;
import cn.topcode.unicorn.wxsdk.message.template.GetTemplateListResult;
import cn.topcode.unicorn.wxsdk.message.template.SendTemplateMessage;
import cn.topcode.unicorn.wxsdk.message.template.SendTemplateMessageResult;
import cn.topcode.unicorn.wxsdk.message.template.SetIndustry;
import cn.topcode.unicorn.wxsdk.message.template.UploadNewsMaterial;
import cn.topcode.unicorn.wxsdk.message.template.UploadNewsMaterialResult;

public class MessageInvokerImpl implements MessageInvoker {

    @Override
    public Result setIndustry(String mpId, String industry1, String industry2) {
        SetIndustry setIndustry = new SetIndustry();
        setIndustry.setIndustryId1(industry1);
        setIndustry.setIndustryId2(industry2);
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.SET_INDUSTRY, token);
        return HttpUtil.post(setIndustry, Result.class, url);
    }

    @Override
    public Result setIndustry(String industry1, String industry2) {
        return setIndustry(WXContext.getDefaultMpId(), industry1, industry2);
    }

    @Override
    public GetIndustryResult getIndustryResult(String mpId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.GET_INDUSTRY, token);
        return HttpUtil.getJson(url, GetIndustryResult.class);
    }

    @Override
    public GetIndustryResult getIndustryResult() {
        return getIndustryResult(WXContext.getDefaultMpId());
    }

    @Override
    public GetTemplateIdResult getTemplateID(String mpId, String templateIdShort) {
        Map<String,String> map = new HashMap<>();
        map.put("template_id_short", templateIdShort);
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.DELETE_TEMPLATE, token);
        return HttpUtil.post(map, GetTemplateIdResult.class, url);
    }

    @Override
    public GetTemplateIdResult getTemplateID(String templateIdShort) {
        return getTemplateID(WXContext.getDefaultMpId(), templateIdShort);
    }

    @Override
    public GetTemplateListResult getTemplateListResult(String mpId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.GET_TEMPALTE_LIST, token);
        return HttpUtil.getJson(url, GetTemplateListResult.class);
    }

    @Override
    public GetTemplateListResult getTemplateListResult() {
        return getTemplateListResult(WXContext.getDefaultMpId());
    }

    @Override
    public Result deleteTemplate(String mpId, String templateId) {
        Map<String,String> map = new HashMap<>();
        map.put("template_id", templateId);
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.DELETE_TEMPLATE, token);
        return HttpUtil.post(map, Result.class, url);
    }

    @Override
    public Result deleteTemplate(String templateId) {
        return deleteTemplate(WXContext.getDefaultMpId(), templateId);
    }

    @Override
    public SendTemplateMessageResult sendTemplateMessage(String mpId, String openId, String templateId,
            String url, Object data) {
        SendTemplateMessage message = new SendTemplateMessage();
        message.setTouser(openId);
        message.setTemplateId(templateId);
        message.setUrl(url);
        message.setData(data);
        String token = WXContext.getToken(mpId);
        String requestUrl = String.format(ApiUrl.Message.SEND_TEMPLATE_MESSAGE_URL, token);
        return HttpUtil.post(message, SendTemplateMessageResult.class, requestUrl);
    }

    @Override
    public SendTemplateMessageResult sendTemplateMessage(String openId, String templateId, String url, Object data) {
        return sendTemplateMessage(WXContext.getDefaultMpId(), openId, templateId, url, data);
    }

    @Override
    public Result createKfAccount(String mpId, String account, String nickname, String password) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.ADD_KF_ACCOUNT, token);
        ReqCreateKfAccount req = new ReqCreateKfAccount();
        req.setKfAccount(account);
        req.setNickname(nickname);
        req.setPassword(password);
        return HttpUtil.post(req, Result.class, url);
    }

    @Override
    public Result createKfAccount(String account, String nickname, String password) {
        return createKfAccount(WXContext.getDefaultMpId(), account, nickname, password);
    }

    @Override
    public Result modifyKfAccount(String mpId, String account, String nickname, String password) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.MODIFY_KF_ACCOUNT, token);
        ReqCreateKfAccount req = new ReqCreateKfAccount();
        req.setKfAccount(account);
        req.setNickname(nickname);
        req.setPassword(password);
        return HttpUtil.post(req, Result.class, url);
    }

    @Override
    public Result modifyKfAccount(String account, String nickname, String password) {
        return modifyKfAccount(WXContext.getDefaultMpId(), account, nickname, password);
    }

    @Override
    public Result deleteKfAccount(String mpId, String account, String nickname, String password) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.DELETE_KF_ACCOUNT, token);
        ReqCreateKfAccount req = new ReqCreateKfAccount();
        req.setKfAccount(account);
        req.setNickname(nickname);
        req.setPassword(password);
        return HttpUtil.post(req, Result.class, url);
    }

    @Override
    public Result deleteKfAccount(String account, String nickname, String password) {
        return deleteKfAccount(WXContext.getDefaultMpId(), account, nickname, password);
    }

    @Override
    public Result setKfAccountHeadImg(String mpId, String account, File headImg) {

        return null;
    }

    @Override
    public Result setKfAccountHeadImg(String account, File headImg) {
        return setKfAccountHeadImg(WXContext.getDefaultMpId(), account, headImg);
    }

    @Override
    public GetAllKfAccountResult getAllKfAccount(String mpId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.GET_ALL_KF_ACCOUNT, token);
        return HttpUtil.getJson(url, GetAllKfAccountResult.class);
    }

    @Override
    public GetAllKfAccountResult getAllKfAccount() {
        return getAllKfAccount(WXContext.getDefaultMpId());
    }

    @Override
    public Result sendKfMessage(String mpId, ReqSendKfMessage req) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Message.SEND_KF_MESSAGE, token);
        return HttpUtil.post(req, Result.class, url);
    }

    @Override
    public Result sendKfMessage(ReqSendKfMessage req) {
        return sendKfMessage(WXContext.getDefaultMpId(), req);
    }

    @Override
    public String uploadImgMessageImg(String mpId, File media) {
        return null;
    }

    @Override
    public String uploadImgMessageImg(File media) {
        return uploadImgMessageImg(WXContext.getDefaultMpId(), media);
    }

    @Override
    public UploadNewsMaterialResult uploadImgMessageMaterial(String mpId, UploadNewsMaterial req) {

        return null;
    }

    @Override
    public UploadNewsMaterialResult uploadImgMessageMaterial(UploadNewsMaterial req) {
        return uploadImgMessageMaterial(WXContext.getDefaultMpId(), req);
    }

    @Override
    public void massByTag(String mpId, MassByTag massTag) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void massByTag(MassByTag massTag) {
        massByTag(WXContext.getDefaultMpId(), massTag);
    }

    @Override
    public void massOpenIdList(String mpId) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void massOpenIdList() {
        massOpenIdList(WXContext.getDefaultMpId());
    }

    @Override
    public Result deleteMass(String mpId, String msgId) {
        return null;
    }

    @Override
    public Result deleteMass(String msgId) {
        return deleteMass(WXContext.getDefaultMpId(), msgId);
    }

    @Override
    public void previewImageMessage(String mpId) {
        
    }

    @Override
    public void previewImageMessage() {
        previewImageMessage(WXContext.getDefaultMpId());
    }

    @Override
    public String queryMassStatus(String mpId, String msgId) {
        return null;
    }

    @Override
    public String queryMassStatus(String msgId) {
        return queryMassStatus(WXContext.getDefaultMpId(), msgId);
    }

    @Override
    public void getAutoReplyRule(String mpId) {
        
        
    }

    @Override
    public void getAutoReplyRule() {
        getAutoReplyRule(WXContext.getDefaultMpId());
    }

}
