package cn.topcode.unicorn.wxsdk.message.receive.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 点击菜单跳转链接时的事件推送
 * @author Leo Lien
 * 2016年12月4日 上午12:50:26 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ScanCodeWaitMsgEvent extends Event {
    
    public static final String SCAN_TYPE_QRCODE = "qrcode";

    /*事件KEY值，设置的跳转URL*/
    private String eventKey;
    
    /*扫描类似，一般是qrcode*/
    private String scanType;   
    
    /*扫描结果，即二维码对应的字符串信息*/
    private String scanResult;

}
