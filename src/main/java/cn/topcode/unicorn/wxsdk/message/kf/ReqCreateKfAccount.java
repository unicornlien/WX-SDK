package cn.topcode.unicorn.wxsdk.message.kf;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class ReqCreateKfAccount {

    @JSONField(name="kf_account")
    private String kfAccount;
    
    private String nickname;
    
    private String password;

}
