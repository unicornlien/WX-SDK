package cn.topcode.unicorn.wxsdk.message.reply;

import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;

public class ReplySuccessMessage extends ReplyMessage {

    private static final String MSG_TYPE_SUCCESS = null;

    public ReplySuccessMessage(Message src) {
        super(src);
        this.setMsgType(MSG_TYPE_SUCCESS);
    }

}
