package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetIndustryResult extends Result {

    @JSONField(name="primary_industry")
    private Industry primaryIndustry;
    
    @JSONField(name="secondary_industry")
    private Industry secondaryIndustry;

}
