package cn.topcode.unicorn.wxsdk.message.kf;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class KfMusicMessage {

    private String title;
    
    private String description;
    
    @JSONField(name="musicurl")
    private String musicUrl;
    
    @JSONField(name="hqmusicurl")
    private String hqMusicUrl;
    
    @JSONField(name="thumb_media_id")
    private String thumbMediaId;

}
