package cn.topcode.unicorn.wxsdk.message.reply;

import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 转发到客服系统
 * @author Unicorn
 */
@Data
@EqualsAndHashCode(callSuper=false)
@XStreamAlias("xml")
public class ReplyTransferKfMessage extends ReplyMessage {

    private static final String MSG_TYPE_TRANSFER_CUSTOMER_SERVICE = "transfer_customer_service";

    @XStreamAlias("TransInfo")
    private TransInfo transInfo;

    public ReplyTransferKfMessage(Message src) {
        super(src);
        this.setMsgType(MSG_TYPE_TRANSFER_CUSTOMER_SERVICE);
    }

    public void setKfAccount(String kfAccount) {
        transInfo = new TransInfo();
        transInfo.setKfAccount(kfAccount);
    }

}
