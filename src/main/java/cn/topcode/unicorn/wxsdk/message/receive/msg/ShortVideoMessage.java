package cn.topcode.unicorn.wxsdk.message.receive.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 小视频消息
 * @author Leo Lien
 * 2016年12月4日 下午3:08:29 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ShortVideoMessage extends GenericMessage {

    /*视频消息媒体id，可以调用多媒体文件下载接口拉取数据。*/
    private String mediaId;
    
    /*视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。*/
    private String thumbMediaId;

}
