package cn.topcode.unicorn.wxsdk.message.receive.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 点击菜单跳转链接时的事件推送
 * @author Leo Lien
 * 2016年12月4日 上午12:50:26 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ClickLinkMenuEvent extends Event {

    /*事件KEY值，设置的跳转URL*/
    private String eventKey;

}
