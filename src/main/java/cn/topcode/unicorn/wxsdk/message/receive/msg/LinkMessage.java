package cn.topcode.unicorn.wxsdk.message.receive.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 链接消息
 * @author Leo Lien
 * 2016年12月4日 下午3:10:06 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LinkMessage extends GenericMessage {

    /*消息标题*/
    private String title;
    
    /*消息描述*/
    private String description;
    
    /*消息链接*/
    private String url;

}
