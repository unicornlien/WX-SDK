package cn.topcode.unicorn.wxsdk.message.kf;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class KfImageOrVoiceMessage {

    @JSONField(name="media_id")
    private String mediaId;
    
}
