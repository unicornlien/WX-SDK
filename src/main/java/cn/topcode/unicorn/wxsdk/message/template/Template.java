package cn.topcode.unicorn.wxsdk.message.template;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class Template {

    @JSONField(name="template_id")
    private String templateId;
    
    private String title;
    
    @JSONField(name="primary_industry")
    private String primaryIndustry;
    
    @JSONField(name="deputy_industry")
    private String deputyIndustry;
    
    private String content;
    
    private String example;

}
