package cn.topcode.unicorn.wxsdk.message.kf;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class KfAccount {

    @JSONField(name="kf_account")
    private String kfAccount;
    
    @JSONField(name="kf_nick")
    private String kfNick;
    
    @JSONField(name="kf_id")
    private String kfId;
    
    @JSONField(name="kf_headimgurl")
    private String kfHeadimgurl;

}
