package cn.topcode.unicorn.wxsdk.message.receive.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 文本消息
 * @author Leo Lien
 * 2016年12月4日 下午3:06:11 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class TextMessage extends GenericMessage {
    
    /*文本消息内容*/
    private String content;

}
