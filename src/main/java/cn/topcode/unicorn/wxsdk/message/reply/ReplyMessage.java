package cn.topcode.unicorn.wxsdk.message.reply;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@Data
@EqualsAndHashCode(callSuper=false)
@XStreamAlias("xml")
public abstract class ReplyMessage {
    
    /*回复文本消息*/
    public static final String MSG_TYPE_TEXT = "text";
    /*回复图片消息*/
    public static final String MSG_TYPE_IMAGE = "image";
    /*回复语音消息*/
    public static final String MSG_TYPE_VOICE = "voice";
    /*回复视频消息*/
    public static final String MSG_TYPE_VIDEO = "video";
    /*回复音乐消息*/
    public static final String MSG_TYPE_MUSIC = "music";
    /*回复图文消息*/
    public static final String MSG_TYPE_NEWS = "news";

    /*开发者微信号*/
    @XStreamAlias("ToUserName")
    private String toUserName;
    
    /*发送方帐号（一个OpenID）*/
    @XStreamAlias("FromUserName")
    private String fromUserName;
    
    /*消息创建时间 （整型）*/
    @XStreamAlias("CreateTime")
    private long createTime;
    
    /*消息类型*/
    @XStreamAlias("MsgType")
    private String msgType;
    
    public ReplyMessage(Message src) {
        this.toUserName = src.getFromUserName();
        this.fromUserName = src.getToUserName();
        this.createTime = System.currentTimeMillis();
    }

}
