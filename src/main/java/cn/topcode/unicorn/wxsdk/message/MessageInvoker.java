package cn.topcode.unicorn.wxsdk.message;

import java.io.File;

import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.message.kf.GetAllKfAccountResult;
import cn.topcode.unicorn.wxsdk.message.kf.ReqSendKfMessage;
import cn.topcode.unicorn.wxsdk.message.template.GetIndustryResult;
import cn.topcode.unicorn.wxsdk.message.template.GetTemplateIdResult;
import cn.topcode.unicorn.wxsdk.message.template.GetTemplateListResult;
import cn.topcode.unicorn.wxsdk.message.template.SendTemplateMessageResult;
import cn.topcode.unicorn.wxsdk.message.template.UploadNewsMaterial;
import cn.topcode.unicorn.wxsdk.message.template.UploadNewsMaterialResult;


/**
 * 消息请求接口
 * 包括客服账号管理、客户消息请求、模板消息请求、自动回复管理
 * @author Leo Lien
 * 2016年12月4日 上午9:58:09 创建
 */
public interface MessageInvoker {

    /**
     * 创建客服账号
     * @param mpId  公众号ID
     * @param account   客服账号
     * @param nickname  昵称
     * @param password  密码
     * @return
     */
    Result createKfAccount(String mpId, String account, String nickname, String password);

    /**
     * 创建客服账号
     * @param account   客服账号
     * @param nickname  昵称
     * @param password  密码
     * @return
     */
    Result createKfAccount(String account, String nickname, String password);

    /**
     * 修改客服账号
     * @param mpId  公众号ID
     * @param account   客服账号
     * @param nickname  昵称
     * @param password  密码
     * @return
     */
    Result modifyKfAccount(String mpId, String account, String nickname, String password);

    /**
     * 修改客服账号
     * @param account   客服账号
     * @param nickname  昵称
     * @param password  密码
     * @return
     */
    Result modifyKfAccount(String account, String nickname, String password);
    
    Result deleteKfAccount(String mpId, String account, String nickname, String password);

    Result deleteKfAccount(String account, String nickname, String password);
    
    Result setKfAccountHeadImg(String mpId, String account, File headImg);

    Result setKfAccountHeadImg(String account, File headImg);
    
    GetAllKfAccountResult getAllKfAccount(String mpId);

    GetAllKfAccountResult getAllKfAccount();
    
    Result sendKfMessage(String mpId, ReqSendKfMessage req);

    Result sendKfMessage(ReqSendKfMessage req);
    
    String uploadImgMessageImg(String mpId, File media);

    String uploadImgMessageImg(File media);
    
    UploadNewsMaterialResult uploadImgMessageMaterial(String mpId, UploadNewsMaterial req);

    UploadNewsMaterialResult uploadImgMessageMaterial(UploadNewsMaterial req);
    
    void massByTag(String mpId, MassByTag massTag);

    void massByTag(MassByTag massTag);
    
    void massOpenIdList(String mpId);

    void massOpenIdList();
    
    Result deleteMass(String mpId, String msgId);

    Result deleteMass(String msgId);
    
    void previewImageMessage(String mpId);

    void previewImageMessage();
    
    String queryMassStatus(String mpId, String msgId);

    String queryMassStatus(String msgId);
     
     /**
      * 设置所属行业
      * @author Leo Lien
      * 2017年3月24日 上午10:32:11 创建
      * @param industry1    主营行业
      * @param industry2    副营行业
      */
     Result setIndustry(String mpId, String industry1, String industry2);

    Result setIndustry(String industry1, String industry2);
     
     GetIndustryResult getIndustryResult(String mpId);

    GetIndustryResult getIndustryResult();
     
     GetTemplateIdResult getTemplateID(String mpId, String templateIdShort);

    GetTemplateIdResult getTemplateID(String templateIdShort);
     
     GetTemplateListResult getTemplateListResult(String mpId);

    GetTemplateListResult getTemplateListResult();
     
     Result deleteTemplate(String mpId, String templateId);

    Result deleteTemplate(String templateId);

     /**
      * 发送模板消息
      * @author Leo Lien
      * 2017年3月24日 上午10:26:39 创建
      * @param openId   接收者openId
      * @param templateId   模板ID
      * @param url  模板跳转URL（非必填）
      * @param data 模板数据
     * @return 
      */
     SendTemplateMessageResult sendTemplateMessage(String mpId, String openId, String templateId, String url, Object data);

    SendTemplateMessageResult sendTemplateMessage(String openId, String templateId, String url, Object data);
     
     void getAutoReplyRule(String mpId);

    void getAutoReplyRule();
}
