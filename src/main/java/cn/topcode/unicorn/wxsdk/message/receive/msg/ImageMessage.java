package cn.topcode.unicorn.wxsdk.message.receive.msg;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 图片消息
 * @author Leo Lien
 * 2016年12月4日 下午3:06:36 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ImageMessage extends GenericMessage {

    /*图片链接*/
    private String picUrl;
    
    /*图片消息媒体id，可以调用多媒体文件下载接口拉取数据。*/
    private String mediaId;

}
