package cn.topcode.unicorn.wxsdk.message.receive.event;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 扫描带参数二维码事件
 * @author Leo Lien
 * 2016年12月4日 上午12:51:49 创建
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ScanQrcodeEvent extends Event {

    /*事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id*/
    private String eventKey;
    
    /*二维码的ticket，可用来换取二维码图片*/
    private String ticket;

}
