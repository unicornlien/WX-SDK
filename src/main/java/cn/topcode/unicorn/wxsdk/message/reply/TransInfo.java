package cn.topcode.unicorn.wxsdk.message.reply;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/3.
 */
@Data
public class TransInfo {

    @XStreamAlias("KfAccount")
    private String kfAccount;
}
