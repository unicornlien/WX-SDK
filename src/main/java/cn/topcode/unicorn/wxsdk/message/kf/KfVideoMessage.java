package cn.topcode.unicorn.wxsdk.message.kf;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class KfVideoMessage {

    @JSONField(name="media_id")
    private String mediaId;
    
    @JSONField(name="thumb_media_id")
    private String thumbMediaId;
    
    private String title;
    
    private String description;

}
