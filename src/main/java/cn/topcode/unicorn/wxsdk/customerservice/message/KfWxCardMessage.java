package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class KfWxCardMessage {

    @JSONField(name="card_id")
    private String cardId;

}
