package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class Music {

    private String title;
    
    private String description;
    
    private String musicurl;
    
    private String hqmusicurl;
    
    @JSONField(name="thumb_media_id")
    private String thumbMediaId;

}
