package cn.topcode.unicorn.wxsdk.customerservice.account;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2017/8/17.
 */
@Data
public class InviteWorker {

    @JSONField(name="kf_account")
    private String kfAccount;

    @JSONField(name="invite_wx")
    private String inviteWx;
}
