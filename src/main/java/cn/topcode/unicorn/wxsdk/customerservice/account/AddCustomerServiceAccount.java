package cn.topcode.unicorn.wxsdk.customerservice.account;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AddCustomerServiceAccount {

    @JSONField(name="kf_account")
    private String kfAccount;

    private String nickname;

}
