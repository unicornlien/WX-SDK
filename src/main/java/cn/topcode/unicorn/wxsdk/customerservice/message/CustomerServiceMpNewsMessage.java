package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public abstract class CustomerServiceMpNewsMessage extends AbstractCustomerServiceMessage {

    private MpNews mpnews;

}
