package cn.topcode.unicorn.wxsdk.customerservice;

import cn.topcode.unicorn.utils.http.HttpUtil;
import cn.topcode.unicorn.wxsdk.WXContext;
import cn.topcode.unicorn.wxsdk.base.ApiUrl;
import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.customerservice.account.*;

/**
 * Created by Unicorn on 2017/8/17.
 */
public class CustomerServiceInvokerImpl implements CustomerServiceInvoker {

    @Override
    public Result addCustomerServiceAccount(String mpId, AddCustomerServiceAccount account) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.CustomerService.ADD_ACCOUNT, token);
        return HttpUtil.post(account, Result.class, url);
    }

    @Override
    public Result addCustomerServiceAccount(AddCustomerServiceAccount account) {
        return addCustomerServiceAccount(WXContext.getDefaultMpId(), account);
    }

    @Override
    public Result modifyCustomerServiceAccount(String mpId, ModifyCustomerServiceAccount account) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.CustomerService.SET_ACCOUNT_INFO, token);
        return HttpUtil.post(account, Result.class, url);
    }

    @Override
    public Result modifyCustomerServiceAccount(ModifyCustomerServiceAccount account) {
        return modifyCustomerServiceAccount(WXContext.getDefaultMpId(), account);
    }

    @Override
    public Result deleteCustomerServiceAccount(String mpId, String account) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.CustomerService.DELETE_ACCOUNT, token, account);
        return HttpUtil.getJson(url, Result.class);
    }

    @Override
    public Result deleteCustomerServiceAccount(String account) {
        return deleteCustomerServiceAccount(WXContext.getDefaultMpId(), account);
    }

    @Override
    public Result inviteWorker(String mpId, InviteWorker inviteWorker) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.CustomerService.INVITE_WORKER, token);
        return HttpUtil.post(inviteWorker, Result.class, url);
    }

    @Override
    public Result inviteWorker(InviteWorker inviteWorker) {
        return inviteWorker(WXContext.getDefaultMpId(), inviteWorker);
    }

    @Override
    public void uploadHeadImg(String mpId) {

    }

    @Override
    public void uploadHeadImg() {
        uploadHeadImg(WXContext.getDefaultMpId());
    }

    @Override
    public CustomerServiceAccountList getAllCustomerServiceAccount(String mpId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.CustomerService.GET_ACCOUNT_LIST, token);
        return HttpUtil.getJson(url, CustomerServiceAccountList.class);
    }

    @Override
    public CustomerServiceAccountList getAllCustomerServiceAccount() {
        return getAllCustomerServiceAccount(WXContext.getDefaultMpId());
    }

    @Override
    public KfOnlineList getAllCustomerServiceAccountStatus(String mpId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.CustomerService.GET_ACCOUNT_STATUS, token);
        return HttpUtil.getJson(url, KfOnlineList.class);
    }

    @Override
    public KfOnlineList getAllCustomerServiceAccountStatus() {
        return getAllCustomerServiceAccountStatus(WXContext.getDefaultMpId());
    }
}
