package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public abstract class CustomerServiceTextMessage extends AbstractCustomerServiceMessage {

    private Text text;

}
