package cn.topcode.unicorn.wxsdk.customerservice.account;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2017/8/17.
 */
@Data
public class CustomerServiceAccountStatus {

    @JSONField(name = "kf_account")
    private String kfAccount;

    private String status;

    @JSONField(name = "kf_id")
    private String kfId;

    @JSONField(name = "accepted_case")
    private int acceptedCase;
}
