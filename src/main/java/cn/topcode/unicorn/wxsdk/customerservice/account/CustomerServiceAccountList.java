package cn.topcode.unicorn.wxsdk.customerservice.account;

import java.util.List;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class CustomerServiceAccountList {
    
    @JSONField(name="kf_list")
    private List<CustomerServiceAccount> kfList;

}
