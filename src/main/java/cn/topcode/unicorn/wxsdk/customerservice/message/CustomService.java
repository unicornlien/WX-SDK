package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class CustomService {

    @JSONField(name="kf_account")
    private String kfAccount;

}
