package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class Voice {

    @JSONField(name="media_id")
    private String mediaId;

}
