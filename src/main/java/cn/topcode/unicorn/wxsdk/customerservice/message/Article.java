package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@Data
@XStreamAlias("item")
public class Article {

    @XStreamAlias("Title")
    private String title;
    
    @XStreamAlias("Description")
    private String description;
    
    @XStreamAlias("Url")
    private String url;
    
    @XStreamAlias("PicUrl")
    private String picurl;

}
