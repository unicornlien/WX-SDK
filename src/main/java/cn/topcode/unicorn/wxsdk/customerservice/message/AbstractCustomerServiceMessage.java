package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;

@Data
public abstract class AbstractCustomerServiceMessage {

    private String touser;
    
    private String msgtype;
    
    private CustomService customservice;

}
