package cn.topcode.unicorn.wxsdk.customerservice.message;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class News {

    private List<Article> articles;

}
