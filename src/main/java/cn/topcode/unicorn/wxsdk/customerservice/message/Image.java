package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class Image {

    @JSONField(name="media_id")
    private String mediaId;

}
