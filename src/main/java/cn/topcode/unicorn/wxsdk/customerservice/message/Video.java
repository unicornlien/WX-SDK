package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class Video {

    @JSONField(name="media_id")
    private String mediaId;
    
    @JSONField(name="thumb_media_id")
    private String thumbMediaId;
    
    private String title;
    
    private String description;

}
