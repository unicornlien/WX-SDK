package cn.topcode.unicorn.wxsdk.customerservice.account;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2017/8/17.
 */
@Data
public class KfOnlineList {

    @JSONField(name = "kf_online_list")
    private List<CustomerServiceAccountStatus> kfOnlineList;
}
