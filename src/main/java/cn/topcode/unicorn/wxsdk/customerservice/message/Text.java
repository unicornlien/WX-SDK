package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;

@Data
public class Text {

    private String content;

}
