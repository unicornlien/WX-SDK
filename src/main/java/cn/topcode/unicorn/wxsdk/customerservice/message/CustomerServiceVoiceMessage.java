package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public abstract class CustomerServiceVoiceMessage extends AbstractCustomerServiceMessage {

    private Voice voice;

}
