package cn.topcode.unicorn.wxsdk.customerservice;

import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.customerservice.account.*;

public interface CustomerServiceInvoker {

    /**
     * 添加客服账号
     * @author Leo Lien
     * 2016年12月5日 下午9:45:07 创建
     * @param account
     */
    Result addCustomerServiceAccount(String mpId, AddCustomerServiceAccount account);

    Result addCustomerServiceAccount(AddCustomerServiceAccount account);
    
    /**
     * 修改客服账号
     * @author Leo Lien
     * 2016年12月5日 下午9:45:15 创建
     * @param account
     */
    Result modifyCustomerServiceAccount(String mpId, ModifyCustomerServiceAccount account);

    Result modifyCustomerServiceAccount(ModifyCustomerServiceAccount account);
    
    /**
     * 删除客服账号
     * @author Leo Lien
     * 2016年12月5日 下午9:45:20 创建
     * @param account
     */
    Result deleteCustomerServiceAccount(String mpId, String account);

    Result deleteCustomerServiceAccount(String account);

    /**
     * 邀请绑定客服账号
     * @return
     */
    Result inviteWorker(String mpId, InviteWorker inviteWorker);

    Result inviteWorker(InviteWorker inviteWorker);
    
    /**
     * 上传头像
     * @author Leo Lien
     * 2016年12月5日 下午9:45:22 创建
     */
    void uploadHeadImg(String mpId);

    void uploadHeadImg();
    
    /**
     * 获取所有客服账号
     * @author Leo Lien
     * 2016年12月5日 下午9:45:25 创建
     * @return
     */
    CustomerServiceAccountList getAllCustomerServiceAccount(String mpId);

    CustomerServiceAccountList getAllCustomerServiceAccount();

    KfOnlineList getAllCustomerServiceAccountStatus(String mpId);

    KfOnlineList getAllCustomerServiceAccountStatus();
    
}
