package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class WxCard {

    @JSONField(name="card_id")
    private String cardId;
    
    @JSONField(name="card_ext")
    private String cardExt;

}
