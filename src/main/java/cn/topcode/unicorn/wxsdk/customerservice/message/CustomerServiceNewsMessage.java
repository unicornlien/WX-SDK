package cn.topcode.unicorn.wxsdk.customerservice.message;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public abstract class CustomerServiceNewsMessage extends AbstractCustomerServiceMessage {

    private News news;

}
