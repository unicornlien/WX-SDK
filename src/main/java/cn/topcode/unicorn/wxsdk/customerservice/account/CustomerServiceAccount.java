package cn.topcode.unicorn.wxsdk.customerservice.account;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class CustomerServiceAccount {

    public static final String INVITE_STATUS_WAITING = "waiting";

    public static final String INVITE_STATUS_REJECTED = "rejected";

    public static final String INVITE_STATUS_EXPIRED = "expired";

    @JSONField(name="kf_account")
    private String kfAccount;
    
    @JSONField(name="kf_nick")
    private String kfNick;
    
    @JSONField(name="kf_id")
    private String kfId;
    
    @JSONField(name="kf_headimgurl")
    private String kfHeadimgurl;

    @JSONField(name="kf_wx")
    private String kfWx;

    @JSONField(name="inviteWx")
    private String inviteWx;

    @JSONField(name="invite_expire_time")
    private Long inviteExpireTime;

    @JSONField(name="invite_status")
    private String inviteStatus;

}
