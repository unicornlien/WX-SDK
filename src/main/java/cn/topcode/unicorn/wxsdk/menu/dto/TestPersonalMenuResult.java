package cn.topcode.unicorn.wxsdk.menu.dto;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

@Data
@EqualsAndHashCode(callSuper=false)
public class TestPersonalMenuResult extends Result {

    private List<Button> button;

}
