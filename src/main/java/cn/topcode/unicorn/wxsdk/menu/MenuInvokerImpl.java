package cn.topcode.unicorn.wxsdk.menu;

import java.util.HashMap;
import java.util.Map;

import cn.topcode.unicorn.utils.http.HttpUtil;
import cn.topcode.unicorn.wxsdk.WXContext;
import cn.topcode.unicorn.wxsdk.base.ApiUrl;
import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.menu.dto.CreateMenu;
import cn.topcode.unicorn.wxsdk.menu.dto.CreatePersonalMenu;
import cn.topcode.unicorn.wxsdk.menu.dto.CreatePersonalMenuResult;
import cn.topcode.unicorn.wxsdk.menu.dto.QueryMenuResult;
import cn.topcode.unicorn.wxsdk.menu.dto.TestPersonalMenuResult;

public class MenuInvokerImpl implements MenuInvoker {
    
    @Override
    public Result createMenu(String mpId, CreateMenu menu) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Menu.CREATE_MENU, token);
        return HttpUtil.post(menu, Result.class, url);
    }

    @Override
    public Result createMenu(CreateMenu menu) {
        return createMenu(WXContext.getDefaultMpId(), menu);
    }

    @Override
    public QueryMenuResult queryMenuResult(String mpId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Menu.QUERY_MENU, token);
        return HttpUtil.getJson(url, QueryMenuResult.class);
    }

    @Override
    public QueryMenuResult queryMenuResult() {
        return queryMenuResult(WXContext.getDefaultMpId());
    }

    @Override
    public Result deleteMenu(String mpId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Menu.DELETE_MENU, token);
        return HttpUtil.getJson(url, Result.class);
    }

    @Override
    public Result deleteMenu() {
        return deleteMenu(WXContext.getDefaultMpId());
    }

    @Override
    public CreatePersonalMenuResult createPersonalMenu(String mpId, CreatePersonalMenu menu) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Menu.CREATE_PERSONAL_MENU, token);
        return HttpUtil.post(menu, CreatePersonalMenuResult.class, url);
    }

    @Override
    public CreatePersonalMenuResult createPersonalMenu(CreatePersonalMenu menu) {
        return createPersonalMenu(WXContext.getDefaultMpId(), menu);
    }

    @Override
    public Result deletePersonalMenu(String mpId, String menuId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Menu.DELETE_PERSONAL_MENU, token);
        Map<String,Object> map = new HashMap<>();
        map.put("menuid", menuId);
        return HttpUtil.post(map, Result.class, url);
    }

    @Override
    public Result deletePersonalMenu(String menuid) {
        return deletePersonalMenu(WXContext.getDefaultMpId(), menuid);
    }

    @Override
    public TestPersonalMenuResult testPersonalMenuResult(String mpId, String userId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Menu.TEST_PERSONAL_MENU, token);
        Map<String,Object> map = new HashMap<>();
        map.put("user_id", userId);
        return HttpUtil.post(map, TestPersonalMenuResult.class, url);
    }

    @Override
    public TestPersonalMenuResult testPersonalMenuResult(String userId) {
        return testPersonalMenuResult(WXContext.getDefaultMpId(), userId);
    }

}
