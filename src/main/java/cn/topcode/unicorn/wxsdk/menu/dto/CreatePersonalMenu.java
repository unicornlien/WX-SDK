package cn.topcode.unicorn.wxsdk.menu.dto;

import java.util.List;

import lombok.Data;

@Data
public class CreatePersonalMenu {

    private List<Button> button;
    
    private MatchRule matchrule;

}
