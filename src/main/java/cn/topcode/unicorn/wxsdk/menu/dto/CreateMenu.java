package cn.topcode.unicorn.wxsdk.menu.dto;

import java.util.List;

import lombok.Data;

@Data
public class CreateMenu {

    private List<Button> button;

}
