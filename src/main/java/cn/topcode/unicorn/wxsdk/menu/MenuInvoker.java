package cn.topcode.unicorn.wxsdk.menu;

import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.menu.dto.CreateMenu;
import cn.topcode.unicorn.wxsdk.menu.dto.CreatePersonalMenu;
import cn.topcode.unicorn.wxsdk.menu.dto.CreatePersonalMenuResult;
import cn.topcode.unicorn.wxsdk.menu.dto.QueryMenuResult;
import cn.topcode.unicorn.wxsdk.menu.dto.TestPersonalMenuResult;

/**
 * 菜单管理接口
 * 包括普通菜单和个性菜单的管理
 * @author Unicorn Lien
 * 2016年12月4日 上午9:56:25 创建
 */
public interface MenuInvoker {
    
    /**
     * 请求创建菜单
     * @author Unicorn Lien
     * 2016年12月4日 上午9:52:47 创建
     * @param menu  创建菜单数据
     * @return  请求结果
     */
    Result createMenu(String mpId, CreateMenu menu);

    Result createMenu(CreateMenu menu);
    
    /**
     * 请求查询菜单
     * @author Unicorn Lien
     * 2016年12月4日 上午9:53:59 创建
     * @return  请求结果
     */
    QueryMenuResult queryMenuResult(String mpId);

    QueryMenuResult queryMenuResult();
    
    /**
     * 删除菜单
     * @author Unicorn Lien
     * 2016年12月4日 上午9:54:35 创建
     * @return  请求结果
     */
    Result deleteMenu(String mpId);

    Result deleteMenu();
    
    /**
     * 创建个性菜单
     * @author Unicorn Lien 
     * 2017年3月28日 下午10:00:59 创建
     * @param menu  创建个性菜单数据
     * @return  请求结果
     */
    CreatePersonalMenuResult createPersonalMenu(String mpId, CreatePersonalMenu menu);

    CreatePersonalMenuResult createPersonalMenu(CreatePersonalMenu menu);
    
    /**
     * 删除个性菜单
     * @author Unicorn Lien
     * 2017年3月28日 下午10:01:26 创建
     * @param menuid    菜单ID
     * @return  请求结果
     */
    Result deletePersonalMenu(String mpId, String menuid);

    Result deletePersonalMenu(String menuid);
    
    /**
     * 测试个性菜单
     * @author Unicorn Lien
     * 2017年3月28日 下午10:01:52 创建
     * @param userId    用户Id
     * @return  测试结果
     */
    TestPersonalMenuResult testPersonalMenuResult(String mpId, String userId);

    TestPersonalMenuResult testPersonalMenuResult(String userId);
}
