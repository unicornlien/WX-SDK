package cn.topcode.unicorn.wxsdk.menu.dto;

import java.util.List;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class Button {
    
    public static final String TYPE_CLICK = "click";
    public static final String TYPE_VIEW = "view";
    public static final String TYPE_SCANCODE_PUSH = "scancode_push";
    public static final String TYPE_SCANCODE_WAITMSG = "scancode_waitmsg";
    public static final String TYPE_PIC_SYSPHOTO = "pic_sysphoto";
    public static final String TYPE_PIC_PHOTO_OR_ALBUM = "pic_photo_or_album";
    public static final String TYPE_PIC_WEIXIN = "pic_weixin";
    public static final String TYPE_LOCATION_SELECT = "location_select";
    public static final String TYPE_MEDIA_ID = "media_id";
    public static final String TYPE_VIEW_LIMITED = "view_limited";

    private String type;
    
    private String name;
    
    private String key;
    
    @JSONField(name="sub_button")
    private List<Button> subButton;
    
    private String url;
    
    @JSONField(name="media_id")
    private String mediaId;
    
}
