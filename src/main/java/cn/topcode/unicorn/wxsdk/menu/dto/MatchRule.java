package cn.topcode.unicorn.wxsdk.menu.dto;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class MatchRule {

    @JSONField(name="tag_id")
    private Long tagId;
    
    private String sex;
    
    private String country;
    
    private String province;
    
    private String city;
    
    @JSONField(name="client_platform_type")
    private String clientPlatformType;
    
    private String language;

}
