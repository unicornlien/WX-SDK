package cn.topcode.unicorn.wxsdk.menu.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

@Data
@EqualsAndHashCode(callSuper=false)
public class CreatePersonalMenuResult extends Result {

    private String menuid;

}
