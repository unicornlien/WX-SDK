package cn.topcode.unicorn.wxsdk.wxpay.dto.refund;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 申请退款
 */
@XStreamAlias("xml")
public class Refund {

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 子商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 子商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 微信订单号
     */
    @XStreamAlias("transaction_id")
    private String transactionId;

    /**
     * 商户订单号
     */
    @XStreamAlias("out_trade_no")
    private String outTradeNo;

    /**
     * 商户退款单号
     */
    @XStreamAlias("out_refund_no")
    private String outRefundNo;

    /**
     * 订单金额
     */
    @XStreamAlias("total_fee")
    private String totalFee;

    /**
     * 申请退款金额
     */
    @XStreamAlias("refund_fee")
    private String refundFee;

    /**
     * 货币种类
     */
    @XStreamAlias("refund_fee_type")
    private String refundFeeType;

    /**
     * 退款原因
     */
    @XStreamAlias("refund_desc")
    private String refundDesc;

    /**
     * 退款资金来源
     */
    @XStreamAlias("refund_account")
    private String refundAccount;
}
