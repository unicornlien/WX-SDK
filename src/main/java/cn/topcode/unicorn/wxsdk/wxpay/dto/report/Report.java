package cn.topcode.unicorn.wxsdk.wxpay.dto.report;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 交易保障
 */
@XStreamAlias("xml")
public class Report {

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 子商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 子商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 设备号
     */
    @XStreamAlias("device_info")
    private String deviceInfo;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 接口URL
     */
    @XStreamAlias("interface_url")
    private String interfaceUrl;

    /**
     * 接口耗时
     */
    @XStreamAlias("execute_time")
    private String executeTime;

    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;

    /**
     * 业务结果
     */
    @XStreamAlias("result_code")
    private String resultCode;

    /**
     * 错误代码
     */
    @XStreamAlias("err_code")
    private String errCode;

    /**
     * 错误代码描述
     */
    @XStreamAlias("err_code_des")
    private String errCodeDes;

    /**
     * 商户订单号
     */
    @XStreamAlias("out_trade_no")
    private String outTradeNo;

    /**
     * 访问接口IP
     */
    @XStreamAlias("user_ip")
    private String userIp;

    /**
     * 商户上报时间
     */
    @XStreamAlias("time")
    private String time;
}
