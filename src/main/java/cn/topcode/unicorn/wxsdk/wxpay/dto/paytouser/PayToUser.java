package cn.topcode.unicorn.wxsdk.wxpay.dto.paytouser;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
@XStreamAlias("xml")
public class PayToUser {

    /**
     * 商户账号appid
     */
    @XStreamAlias("mch_appid")
    private String mchAppId;

    /**
     * 商户号 长度32
     */
    @XStreamAlias("mchid")
    private String mchId;

    /**
     * 设备号 长度32
     */
    @XStreamAlias("device_info")
    private String deviceInfo;

    /**
     * 随机字符串 长度32
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名 长度32
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 商户订单号
     */
    @XStreamAlias("partner_trade_no")
    private String partnerTradeNo;

    /**
     * 用户openid
     */
    @XStreamAlias("openid")
    private String openId;

    /**
     * 校验用户姓名选项
     */
    @XStreamAlias("check_name")
    private String checkName;

    /**
     * 收款用户姓名
     */
    @XStreamAlias("re_user_name")
    private String reUserName;

    /**
     * 金额 10099
     */
    @XStreamAlias("amount")
    private int amount;

    /**
     * 企业付款描述信息
     */
    @XStreamAlias("desc")
    private String desc;

    /**
     * 调用接口的机器ip地址
     */
    @XStreamAlias("spbill_create_ip")
    private String spbillCreateIp;
}
