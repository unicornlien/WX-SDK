package cn.topcode.unicorn.wxsdk.wxpay.dto.unifiedorder;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2017/12/31.
 */
@Data
public class PrepayInfo {

    private String appId;

    private String nonceStr;

    private String prepayId;

    private String signType;

    private String paySign;

    private String timeStamp;

    @JSONField(name = "package")
    private String pkg;

    public void prepayId(String prepayId) {
        this.pkg = "prepay_id=" + prepayId;
    }
}
