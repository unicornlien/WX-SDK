package cn.topcode.unicorn.wxsdk.wxpay.dto.orderquery;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 订单查询结果
 */
@Data
@XStreamAlias("xml")
public class OrderQueryResult {

    /**
     * 	此交易订单号不存在	查询系统中不存在此交易订单号	该API只能查提交支付交易返回成功的订单，请商户检查需要查询的订单号是否正确
     */
    public static final String CODE_ORDERNOTEXIST = "ORDERNOTEXIST";

    /**
     * 	系统错误	后台系统返回错误	系统异常，请再调用发起查询
     */
    public static final String CODE_SYSTEMERROR = "SYSTEMERROR";

    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 业务结果
     */
    @XStreamAlias("result_code")
    private String resultCode;

    /**
     * 错误代码
     */
    @XStreamAlias("err_code")
    private String errCode;

    /**
     * 错误代码描述
     */
    @XStreamAlias("err_code_des")
    private String errCodeDes;

    /**
     * 设备号
     */
    @XStreamAlias("device_info")
    private String deviceInfo;

    /**
     * 用户标识
     */
    @XStreamAlias("openid")
    private String openId;

    /**
     * 是否关注公众账号
     */
    @XStreamAlias("is_subscribe")
    private String isSubscribe;

    /**
     * 交易类型
     */
    @XStreamAlias("trade_type")
    private String tradeType;

    /**
     * 交易状态
     */
    @XStreamAlias("trade_state")
    private String tradeState;

    /**
     * 付款银行
     */
    @XStreamAlias("bank_type")
    private String bankType;

    /**
     * 标价金额
     */
    @XStreamAlias("total_fee")
    private Integer totalFee;

    /**
     * 应结订单金额
     */
    @XStreamAlias("settlement_total_fee")
    private String settlementTotalFee;

    /**
     * 标价币种
     */
    @XStreamAlias("fee_type")
    private String feeType;

    /**
     * 现金支付金额
     */
    @XStreamAlias("cash_fee")
    private String cashFee;

    /**
     * 现金支付货币类型
     */
    @XStreamAlias("cash_fee_type")
    private String cashFeeType;

    /**
     * 代金券金额
     */
    @XStreamAlias("coupon_fee")
    private String couponFee;

    /**
     * 代金券使用数量
     */
    @XStreamAlias("coupon_count")
    private String couponCount;

    /**
     * 代金券ID
     */
    @XStreamAlias("coupon_id_$n")
    private String couponId$n;

    /**
     * 代金券类型
     */
    @XStreamAlias("coupon_type_$n")
    private String couponType$n;

    /**
     * 单个代金券金额
     */
    @XStreamAlias("coupon_fee_$n")
    private String couponFee$n;

    /**
     * 微信支付订单号
     */
    @XStreamAlias("transaction_id")
    private String transactionId;

    /**
     * 商户订单号
     */
    @XStreamAlias("out_trade_no")
    private String outTradeNo;

    /**
     * 商家数据包
     */
    @XStreamAlias("attach")
    private String attach;

    /**
     * 支付完成时间
     */
    @XStreamAlias("time_end")
    private String timeEnd;

    /**
     * 交易状态描述
     */
    @XStreamAlias("trade_state_desc")
    private String tradeStateDesc;

}
