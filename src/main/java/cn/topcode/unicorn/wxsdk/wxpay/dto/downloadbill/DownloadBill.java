package cn.topcode.unicorn.wxsdk.wxpay.dto.downloadbill;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 下载对账单
 */
@XStreamAlias("xml")
public class DownloadBill {

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 子商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 子商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 对账单日期
     */
    @XStreamAlias("bill_date")
    private String billDate;

    /**
     * 账单类型
     */
    @XStreamAlias("bill_type")
    private String billType;

    /**
     * 压缩账单
     */
    @XStreamAlias("tar_type")
    private String tarType;
}
