package cn.topcode.unicorn.wxsdk.wxpay.dto.refundquery;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 查询退款结果
 */
@XStreamAlias("xml")
public class RefundQueryResult {

    /**
     * 	接口返回错误	系统超时	请尝试再次掉调用API。
     */
    public static final String CODE_SYSTEMERROR = "SYSTEMERROR";


    /**
     * 	退款订单查询失败	订单号错误或订单状态不正确	请检查订单号是否有误以及订单状态是否正确，如：未支付、已支付未退款
     */
    public static final String CODE_REFUNDNOTEXIST = "REFUNDNOTEXIST";

    /**
     * 	无效transaction_id	请求参数未按指引进行填写	请求参数错误，检查原交易号是否存在或发起支付交易接口返回失败
     */
    public static final String CODE_INVALID_TRANSACTIONID = "INVALID_TRANSACTIONID";

    /**
     * 	参数错误	请求参数未按指引进行填写	请求参数错误，请检查参数再调用退款申请
     */
    public static final String CODE_PARAM_ERROR = "PARAM_ERROR";

    /**
     * 	APPID不存在	参数中缺少APPID	请检查APPID是否正确
     */
    public static final String CODE_APPID_NOT_EXIST = "APPID_NOT_EXIST";

    /**
     * 	MCHID不存在	参数中缺少MCHID	请检查MCHID是否正确
     */
    public static final String CODE_MCHID_NOT_EXIST = "MCHID_NOT_EXIST";

    /**
     * 	请使用post方法	未使用post传递参数 	请检查请求参数是否通过post方法提交
     */
    public static final String CODE_REQUIRE_POST_METHOD = "REQUIRE_POST_METHOD";

    /**
     * 	签名错误	参数签名结果不正确	请检查签名参数和方法是否都符合签名算法要求
     */
    public static final String CODE_SIGNERROR = "SIGNERROR";

    /**
     * 	XML格式错误	XML格式错误	请检查XML参数格式是否正确
     */
    public static final String CODE_XML_FORMAT_ERROR = "XML_FORMAT_ERROR";


    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;

    /**
     * 业务结果
     */
    @XStreamAlias("result_code")
    private String resultCode;

    /**
     * 错误码
     */
    @XStreamAlias("err_code")
    private String errCode;

    /**
     * 错误描述
     */
    @XStreamAlias("err_code_des")
    private String errCodeDes;

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 子商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 子商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 微信订单号
     */
    @XStreamAlias("transaction_id")
    private String transactionId;

    /**
     * 商户订单号
     */
    @XStreamAlias("out_trade_no")
    private String outTradeNo;

    /**
     * 订单金额
     */
    @XStreamAlias("total_fee")
    private String totalFee;

    /**
     * 应结订单金额
     */
    @XStreamAlias("settlement_total_fee")
    private String settlementTotalFee;

    /**
     * 货币种类
     */
    @XStreamAlias("fee_type")
    private String feeType;

    /**
     * 现金支付金额
     */
    @XStreamAlias("cash_fee")
    private String cashFee;

    /**
     * 退款笔数
     */
    @XStreamAlias("refund_count")
    private String refundCount;

    /**
     * 商户退款单号
     */
    @XStreamAlias("out_refund_no_$n")
    private String outRefundNo$n;

    /**
     * 微信退款单号
     */
    @XStreamAlias("refund_id_$n")
    private String refundId$n;

    /**
     * 退款渠道
     */
    @XStreamAlias("refund_channel_$n")
    private String refundChannel$n;

    /**
     * 订单总退款次数
     */
    @XStreamAlias("total_refund_count")
    private String totalRefundCount;

    /**
     * 申请退款金额
     */
    @XStreamAlias("refund_fee_$n")
    private String refundFee$n;

    /**
     * 退款金额
     */
    @XStreamAlias("settlement_refund_fee_$n")
    private String settlementRefundFee$n;

    /**
     * 代金券类型
     */
    @XStreamAlias("coupon_type$n_$m")
    private String couponType$n$m;

    /**
     * 总代金券退款金额
     */
    @XStreamAlias("coupon_refund_fee_$n")
    private String couponRefundFee$n;

    /**
     * 退款代金券使用数量
     */
    @XStreamAlias("coupon_refund_count_$n")
    private String couponRefundCount$n;

    /**
     * 退款代金券ID
     */
    @XStreamAlias("coupon_refund_id_$n_$m")
    private String couponRefundId$n$m;

    /**
     * 单个代金券退款金额
     */
    @XStreamAlias("coupon_refund_fee_$n_$m")
    private String couponRefundFee$n$m;

    /**
     * 退款状态
     */
    @XStreamAlias("refund_status_$n")
    private String refundStatus$n;

    /**
     * 退款资金来源
     */
    @XStreamAlias("refund_account_$n")
    private String refundAccount$n;

    /**
     * 退款入账账户
     */
    @XStreamAlias("refund_recv_account_$n")
    private String refundRecvAccount$n;

    /**
     * 退款成功时间
     */
    @XStreamAlias("refund_success_time_$n")
    private String refundSuccessTime$n;


}
