package cn.topcode.unicorn.wxsdk.wxpay.dto.refundquery;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 查询退款
 */
@XStreamAlias("xml")
public class RefundQuery {

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 子商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 子商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 微信订单号
     */
    @XStreamAlias("transaction_id")
    private String transactionId;

    /**
     * 商户订单号
     */
    @XStreamAlias("out_trade_no")
    private String outTradeNo;

    /**
     * 商户退款单号
     */
    @XStreamAlias("out_refund_no")
    private String outRefundNo;

    /**
     * 微信退款单号
     */
    @XStreamAlias("refund_id")
    private String refundId;

    /**
     * 偏移量
     */
    @XStreamAlias("offset")
    private String offset;
}
