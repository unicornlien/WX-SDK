package cn.topcode.unicorn.wxsdk.wxpay.dto.paytouser;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 支付结果通知
 */
@Data
@XStreamAlias("xml")
public class GetTransferInfoResult {

    public static final String CA_ERROR = "CA_ERROR";           //	请求未携带证书，或请求携带的证书出错	到商户平台下载证书，请求带上证书后重试。
    public static final String SIGN_ERROR = "SIGN_ERROR";       //		商户签名错误	按文档要求重新生成签名后再重试。
    public static final String NO_AUTH = "NO_AUTH";             //		没有权限	请联系微信支付开通api权限
    public static final String FREQ_LIMIT = "FREQ_LIMIT";       //		受频率限制	请对请求做频率控制
    public static final String XML_ERROR = "XML_ERROR";         //		请求的xml格式错误，或者post的数据为空	检查请求串，确认无误后重试
    public static final String PARAM_ERROR = "PARAM_ERROR";     //		参数错误	请查看err_code_des，修改设置错误的参数
    public static final String SYSTEMERROR = "SYSTEMERROR";     //		系统繁忙，请再试。	系统繁忙。
    public static final String NOT_FOUND = "NOT_FOUND";         //	指定单号数据不存在	查询单号对应的数据不存在，请使用正确的商户订单号查询

    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;

    /**
     * 业务结果
     * SUCCESS/FAIL
     */
    @XStreamAlias("result_code")
    private String resultCode;

    /**
     * 错误代码
     */
    @XStreamAlias("err_code")
    private String errCode;

    /**
     * 错误代码描述
     */
    @XStreamAlias("err_code_des")
    private String errCodeDes;

    /**
     * 商户订单号 长度32
     * 商户订单号需要保持唯一性，只能是字母或数字、不能包含有符号
     */
    @XStreamAlias("partner_trade_no")
    private String partnerTradeNo;

    /**
     * 商户号 长度32
     */
    @XStreamAlias("mchid")
    private String mchId;

    /**
     * 付款单号，长度32
     */
    @XStreamAlias("detail_id")
    private String detailId;


    /**
     * 转账状态，长度16
     * SUCCESS/FAILED/PROCESSING
     */
    @XStreamAlias("status")
    private String status;

    /**
     * 失败原因
     */
    @XStreamAlias("reason")
    private String reason;

    /**
     * 收款用户openid
     */
    @XStreamAlias("openid")
    private String openid;

    /**
     * 收款用户姓名
     */
    @XStreamAlias("transfer_name")
    private String transferName;

    /**
     * 付款金额，单位分
     */
    @XStreamAlias("payment_amount")
    private int paymentAmount;

    /**
     * 转账时间 yyyy-MM-dd HH:mm:ss
     */
    @XStreamAlias("transfer_time")
    private String transferTime;

    /**
     * 付款描述
     */
    @XStreamAlias("desc")
    private String desc;

}




















