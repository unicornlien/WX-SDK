package cn.topcode.unicorn.wxsdk.wxpay.dto.refundnotify;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Unicorn on 2017/12/28.
 */
@XStreamAlias("xml")
public class RespRefundNotify {

    @XStreamAlias("return_code")
    private String returnCode;

    @XStreamAlias("return_msg")
    private String returnMsg;
    
}
