package cn.topcode.unicorn.wxsdk.wxpay;

import cn.topcode.unicorn.utils.DigestUtil;
import cn.topcode.unicorn.utils.SignUtil;
import cn.topcode.unicorn.utils.UuidUtil;
import cn.topcode.unicorn.utils.XmlUtil;
import cn.topcode.unicorn.utils.http.HttpResp;
import cn.topcode.unicorn.utils.http.HttpUtil;
import cn.topcode.unicorn.utils.http.HttpsUtil;
import cn.topcode.unicorn.wxsdk.wxpay.dto.paytouser.GetTransferInfo;
import cn.topcode.unicorn.wxsdk.wxpay.dto.paytouser.GetTransferInfoResult;
import cn.topcode.unicorn.wxsdk.wxpay.dto.paytouser.PayToUser;
import cn.topcode.unicorn.wxsdk.wxpay.dto.paytouser.PayToUserResult;

/**
 * Created by Unicorn on 2018/2/25.
 */
public class WxPayToUserInvoker {

    /**
     * 向用户付款接口地址
     */
    public static final String TRANSFERS = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";

    /**
     * 查询企业付款信息
     */
    public static final String GET_TRANSFER_INFO = "https://api.mch.weixin.qq.com/mmpaymkttransfers/gettransferinfo";

    public static void init(String p12Filepath, String password) {
        HttpsUtil.init(p12Filepath, password);
    }

    /**
     * 企业向用户付款
     * @param payToUser
     * @return
     */
    public static PayToUserResult payToUser(PayToUser payToUser, String key) {
        payToUser.setNonceStr(UuidUtil.getUUID().substring(0,32));
        String str = SignUtil.sortField(payToUser, "sign");
        str += "&key=" + key;
        String sign = DigestUtil.md5(str).toUpperCase();
        payToUser.setSign(sign);

        String xml = XmlUtil.toXml(payToUser);
        HttpResp httpResp = HttpsUtil.postText(TRANSFERS, xml);
        if(httpResp.getStatusCode() != 200) {
            return null;
        }
        return XmlUtil.parse(httpResp.getBody(), PayToUserResult.class);
    }

    /**
     * 查询企业付款信息
     * @param getTransferInfo
     * @return
     */
    public static GetTransferInfoResult getTransferInfo(GetTransferInfo getTransferInfo) {
        String xml = XmlUtil.toXml(getTransferInfo);
        HttpResp httpResp = HttpUtil.postText(GET_TRANSFER_INFO, xml);
        if(httpResp.getStatusCode() != 200) {
            return null;
        }
        return XmlUtil.parse(httpResp.getBody(), GetTransferInfoResult.class);
    }
}
