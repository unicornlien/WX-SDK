package cn.topcode.unicorn.wxsdk.wxpay.dto.refund;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 申请退款结果
 */
@XStreamAlias("xml")
public class RefundResult {

    /**
     * 	接口返回错误	系统超时等	请不要更换商户退款单号，请使用相同参数再次调用API。
     */
    public static final String CODE_SYSTEMERROR = "SYSTEMERROR";

    /**
     * 	退款业务流程错误，需要商户触发重试来解决	并发情况下，业务被拒绝，商户重试即可解决	请不要更换商户退款单号，请使用相同参数再次调用API。
     */
    public static final String CODE_BIZERR_NEED_RETRY = "BIZERR_NEED_RETRY";

    /**
     * 	订单已经超过退款期限	订单已经超过可退款的最大期限(支付后一年内可退款)	请选择其他方式自行退款
     */
    public static final String CODE_TRADE_OVERDUE = "TRADE_OVERDUE";

    /**
     * 	业务错误	申请退款业务发生错误	该错误都会返回具体的错误原因，请根据实际返回做相应处理。
     */
    public static final String CODE_ERROR = "ERROR";

    /**
     * 	退款请求失败	用户帐号注销	此状态代表退款申请失败，商户可自行处理退款。
     */
    public static final String CODE_USER_ACCOUNT_ABNORMAL = "USER_ACCOUNT_ABNORMAL";

    /**
     * 	无效请求过多	连续错误请求数过多被系统短暂屏蔽	请检查业务是否正常，确认业务正常后请在1分钟后再来重试
     */
    public static final String CODE_INVALID_REQ_TOO_MUCH = "INVALID_REQ_TOO_MUCH";

    /**
     * 	余额不足	商户可用退款余额不足	此状态代表退款申请失败，商户可根据具体的错误提示做相应的处理。
     */
    public static final String CODE_NOTENOUGH = "NOTENOUGH";

    /**
     * 	无效transaction_id	请求参数未按指引进行填写	请求参数错误，检查原交易号是否存在或发起支付交易接口返回失败
     */
    public static final String CODE_INVALID_TRANSACTIONID = "INVALID_TRANSACTIONID";

    /**
     * 	参数错误	请求参数未按指引进行填写	请求参数错误，请重新检查再调用退款申请
     */
    public static final String CODE_PARAM_ERROR = "PARAM_ERROR";

    /**
     * 	APPID不存在	参数中缺少APPID	请检查APPID是否正确
     */
    public static final String CODE_APPID_NOT_EXIST = "APPID_NOT_EXIST";

    /**
     * 	MCHID不存在	参数中缺少MCHID	请检查MCHID是否正确
     */
    public static final String CODE_MCHID_NOT_EXIST = "MCHID_NOT_EXIST";

    /**
     * 	请使用post方法	未使用post传递参数 	请检查请求参数是否通过post方法提交
     */
    public static final String CODE_REQUIRE_POST_METHOD = "REQUIRE_POST_METHOD";

    /**
     * 	签名错误	参数签名结果不正确	请检查签名参数和方法是否都符合签名算法要求
     */
    public static final String CODE_SIGNERROR = "SIGNERROR";

    /**
     * 	XML格式错误	XML格式错误	请检查XML参数格式是否正确
     */
    public static final String CODE_XML_FORMAT_ERROR = "XML_FORMAT_ERROR";

    /**
     * 	频率限制	2个月之前的订单申请退款有频率限制	该笔退款未受理，请降低频率后重试
     */
    public static final String CODE_FREQUENCY_LIMITED = "FREQUENCY_LIMITED";


    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;

    /**
     * 业务结果
     */
    @XStreamAlias("result_code")
    private String resultCode;

    /**
     * 错误代码
     */
    @XStreamAlias("err_code")
    private String errCode;

    /**
     * 错误代码描述
     */
    @XStreamAlias("err_code_des")
    private String errCodeDes;

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 子商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 子商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 微信订单号
     */
    @XStreamAlias("transaction_id")
    private String transactionId;

    /**
     * 商户订单号
     */
    @XStreamAlias("out_trade_no")
    private String outTradeNo;

    /**
     * 商户退款单号
     */
    @XStreamAlias("out_refund_no")
    private String outRefundNo;

    /**
     * 微信退款单号
     */
    @XStreamAlias("refund_id")
    private String refundId;

    /**
     * 申请退款金额
     */
    @XStreamAlias("refund_fee")
    private String refundFee;

    /**
     * 退款金额
     */
    @XStreamAlias("settlement_refund_fee")
    private String settlementRefundFee;

    /**
     * 订单金额
     */
    @XStreamAlias("total_fee")
    private String totalFee;

    /**
     * 应结订单金额
     */
    @XStreamAlias("settlement_total_fee")
    private String settlementTotalFee;

    /**
     * 货币种类
     */
    @XStreamAlias("fee_type")
    private String feeType;

    /**
     * 现金支付金额
     */
    @XStreamAlias("cash_fee")
    private String cashFee;

    /**
     * 现金退款金额
     */
    @XStreamAlias("cash_refund_fee")
    private String cashRefundFee;

    /**
     * 代金券退款总金额
     */
    @XStreamAlias("coupon_refund_fee")
    private String couponRefundFee;

    /**
     * 退款代金券使用数量
     */
    @XStreamAlias("coupon_refund_count")
    private String couponRefundCount;

    /**
     * 代金券类型
     */
    @XStreamAlias("coupon_type_$n")
    private String couponType$n;

    /**
     * 退款代金券ID
     */
    @XStreamAlias("coupon_refund_id_$n")
    private String couponRefundId$n;

    /**
     * 单个代金券退款金额
     */
    @XStreamAlias("coupon_refund_fee_$n")
    private String couponRefundFee$n;
}
