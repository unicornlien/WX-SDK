package cn.topcode.unicorn.wxsdk.wxpay.dto.paytouser;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 支付结果通知
 */
@Data
@XStreamAlias("xml")
public class PayToUserResult {

    public static final String NO_AUTH = "NO_AUTH";             //	没有该接口权限	没有授权请求此api	请关注是否满足接口调用条件
    //  public static final String AMOUNT_LIMIT = "AMOUNT_LIMIT";   //	付款金额不能小于最低限额	付款金额不能小于最低限额	每次付款金额必须大于1元
    public static final String PARAM_ERROR = "PARAM_ERROR";     //	参数错误	参数缺失，或参数格式出错，参数不合法等	请查看err_code_des，修改设置错误的参数
    public static final String OPENID_ERROR = "OPENID_ERROR";   //	Openid错误	Openid格式错误或者不属于商家公众账号	请核对商户自身公众号appid和用户在此公众号下的openid。
    public static final String SEND_FAILED = "SEND_FAILED";	    //  付款错误	付款失败，请换单号重试	付款失败，请换单号重试
    public static final String NOTENOUGH = "NOTENOUGH";	        //  余额不足	帐号余额不足	请用户充值或更换支付卡后再支付
    public static final String SYSTEMERROR = "SYSTEMERROR";	    //  系统繁忙，请稍后再试。	系统错误，请重试	请使用原单号以及原请求参数重试，否则可能造成重复支付等资金风险
    public static final String NAME_MISMATCH = "NAME_MISMATCH";	//  姓名校验出错	请求参数里填写了需要检验姓名，但是输入了错误的姓名	填写正确的用户姓名
    public static final String SIGN_ERROR = "SIGN_ERROR";	    //  签名错误	没有按照文档要求进行签名 签名前没有按照要求进行排序。 没有使用商户平台设置的密钥进行签名  参数有空格或者进行了encode后进行签名。
    public static final String XML_ERROR ="XML_ERROR";          //	Post内容出错	Post请求数据不是合法的xml格式内容	修改post的内容
    public static final String FATAL_ERROR = "FATAL_ERROR";     //	两次请求参数不一致	两次请求商户单号一样，但是参数不一致	如果想重试前一次的请求，请用原参数重试，如果重新发送，请更换单号。
    public static final String FREQ_LIMIT = "FREQ_LIMIT";       //	超过频率限制，请稍后再试。	接口请求频率超时接口限制	请关注接口的使用条件
    public static final String MONEY_LIMIT = "MONEY_LIMIT";     //	已经达到今日付款总额上限/已达到付款给此用户额度上限	接口对商户号的每日付款总额，以及付款给同一个用户的总额有限制	请关注接口的付款限额条件
    public static final String CA_ERROR = "CA_ERROR";           //	证书出错	请求没带证书或者带上了错误的证书 到商户平台下载证书 请求的时候带上该证书
    public static final String V2_ACCOUNT_SIMPLE_BAN = "V2_ACCOUNT_SIMPLE_BAN";         //	无法给非实名用户付款	用户微信支付账户未知名，无法付款	引导用户在微信支付内进行绑卡实名
    public static final String PARAM_IS_NOT_UTF8 = "PARAM_IS_NOT_UTF8";                 //	请求参数中包含非utf8编码字符	接口规范要求所有请求参数都必须为utf8编码	请关注接口使用规范
    public static final String AMOUNT_LIMIT = "AMOUNT_LIMIT";                           //	付款失败，因你已违反《微信支付商户平台使用协议》，单笔单次付款下限已被调整为5元	商户号存在违反协议内容行为，单次付款下限提高	请遵守《微信支付商户平台使用协议》

    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;

    /**
     * 商户账号appid
     */
    @XStreamAlias("mch_appid")
    private String mchAppId;

    /**
     * 商户号 长度32
     */
    @XStreamAlias("mchid")
    private String mchId;

    /**
     * 设备号 长度32
     */
    @XStreamAlias("device_info")
    private String deviceInfo;

    /**
     * 随机字符串 长度32
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 业务结果
     * SUCCESS/FAIL
     */
    @XStreamAlias("result_code")
    private String resultCode;

    /**
     * 错误代码
     */
    @XStreamAlias("err_code")
    private String errCode;

    /**
     * 错误代码描述
     */
    @XStreamAlias("err_code_des")
    private String errCodeDes;

    /**
     * 商户订单号 长度32
     * 商户订单号需要保持唯一性，只能是字母或数字、不能包含有符号
     */
    @XStreamAlias("partner_trade_no")
    private String partnerTradeNo;

    /**
     * 微信订单号
     * 企业付款成功，返回的微信订单号
     */
    @XStreamAlias("payment_no")
    private String paymentNo;

    /**
     * 企业付款成功时间 yyyy-MM-dd HH:mm:ss
     */
    @XStreamAlias("payment_time")
    private String paymentTime;

}




















