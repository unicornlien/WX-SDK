package cn.topcode.unicorn.wxsdk.wxpay.dto.report;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Unicorn on 2017/12/28.
 */
@XStreamAlias("xml")
public class ReportResult {

    @XStreamAlias("return_code")
    private String returnCode;

    @XStreamAlias("return_msg")
    private String returnMsg;

    @XStreamAlias("result_code")
    private String resultCode;
}
