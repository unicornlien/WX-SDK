package cn.topcode.unicorn.wxsdk.wxpay.dto.unifiedorder;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 统一下单
 */
@Data
@XStreamAlias("xml")
public class UnifiedOrder {

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 子商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 子商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 设备号
     */
    @XStreamAlias("device_info")
    private String deviceInfo;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 签名类型
     */
    @XStreamAlias("sign_type")
    private String signType;

    /**
     * 商品描述
     */
    @XStreamAlias("body")
    private String body;

    /**
     * 商品详情
     */
    @XStreamAlias("detail")
    private String detail;

    /**
     * 附加数据
     */
    @XStreamAlias("attach")
    private String attach;

    /**
     * 商户订单号
     */
    @XStreamAlias("out_trade_no")
    private String outTradeNo;

    /**
     * 货币类型
     */
    @XStreamAlias("fee_type")
    private String feeType;

    /**
     * 总金额
     */
    @XStreamAlias("total_fee")
    private Integer totalFee;

    /**
     * 终端ip
     */
    @XStreamAlias("spbill_create_ip")
    private String spbillCreateIp;

    /**
     * 交易起始时间
     */
    @XStreamAlias("time_start")
    private String startTime;

    /**
     * 交易结束时间
     */
    @XStreamAlias("time_expire")
    private String expiredTime;

    /**
     * 订单优惠标记
     */
    @XStreamAlias("goods_tag")
    private String goodsTag;

    /**
     * 通知地址
     */
    @XStreamAlias("notify_url")
    private String notifyUrl;

    /**
     * 交易类型
     */
    @XStreamAlias("trade_type")
    private String tradeType;

    /**
     * 商品ID
     */
    @XStreamAlias("product_id")
    private String productId;

    /**
     * 指定支付方式
     */
    @XStreamAlias("limit_pay")
    private String limitPay;

    /**
     * 用户标识
     */
    @XStreamAlias("openid")
    private String openId;

    /**
     * 用户子标识
     */
    @XStreamAlias("sub_openid")
    private String subOpenId;

    /**
     * 场景信息
     */
    @XStreamAlias("scene_info")
    private String sceneInfo;
}
















