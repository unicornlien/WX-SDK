package cn.topcode.unicorn.wxsdk.wxpay.dto.refundnotify;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 退款结果通知
 */
@XStreamAlias("xml")
public class RefundNotify {

    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 退款商户好
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 特约商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 特约商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 加密信息
     */
    @XStreamAlias("req_info")
    private String reqInfo;

    /**
     * 微信订单号
     */
    @XStreamAlias("transcation_id")
    private String transactionId;

    /**
     * 商户订单号
     */
    @XStreamAlias("out_trade_no")
    private String outTradeNo;

    /**
     * 微信退款单号
     */
    @XStreamAlias("refund_id")
    private String refundId;

    /**
     * 商户退款单号
     */
    @XStreamAlias("out_refund_no")
    private String outRefundNo;

    /**
     * 订单金额
     */
    @XStreamAlias("total_fee")
    private String totalFee;

    /**
     * 应结订单金额
     */
    @XStreamAlias("settlement_total_fee")
    private String settlementTotalFee;

    /**
     * 申请退款金额
     */
    @XStreamAlias("refund_fee")
    private String refundFee;

    /**
     * 退款金额
     */
    @XStreamAlias("settlement_refund_fee")
    private String settlementRefundFee;

    /**
     * 退款状态
     */
    @XStreamAlias("refund_status")
    private String refundStatus;

    /**
     * 退款成功时间
     */
    @XStreamAlias("success_time")
    private String successTime;

    /**
     * 退款入账账户
     */
    @XStreamAlias("refund_recv_account")
    private String refundRecvAccount;


    /**
     * 退款资金来源
     */
    @XStreamAlias("refund_account")
    private String refundAccount;

    /**
     * 退款发起来源
     */
    @XStreamAlias("refund_request_source")
    private String refundRequestSource;

}
