package cn.topcode.unicorn.wxsdk.wxpay.dto.downloadbill;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 下载对账单结果
 */
@XStreamAlias("xml")
public class DownloadBillResult {

    /**
     * 	下载失败	系统超时	请尝试再次查询。
     */
    public static final String CODE_SYSTEMERROR = "SYSTEMERROR";

    /**
     * 	参数错误	请求参数未按指引进行填写	参数错误，请重新检查
     */
    public static final String CODE_INVAID_BILL_TYPE = "invalid bill_type";
    public static final String CODE_DATA_FORMAT_ERROR = "data format error";
    public static final String CODE_MISSING_PARAMETER = "missing parameter";
    public static final String CODE_SIGN_ERROR = "SIGN ERROR";

    /**
     * 账单不存在	当前商户号没有已成交的订单，不生成对账单	请检查当前商户号在指定日期内是否有成功的交易。
     */
    public static final String CODE_NO_BILL_EXIST = "NO Bill Exist";

    /**
     * 账单未生成	当前商户号没有已成交的订单或对账单尚未生成	请先检查当前商户号在指定日期内是否有成功的交易，如指定日期有交易则表示账单正在生成中，请在上午10点以后再下载。
     */
    public static final String CODE_BILL_CREATING = "Bill Creating";

    /**
     * 账单压缩失败	账单压缩失败，请稍后重试	账单压缩失败，请稍后重试
     */
    public static final String CODE_COMPRESS_GZIP_ERROR = "CompressGZip Error";

    /**
     * 账单解压失败	账单解压失败，请稍后重试	账单解压失败，请稍后重试
     */
    public static final String CODE_UNCOMPRESS_GZIP_ERROR = "UnCompressGZip Error";

    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String code;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String message;

}
