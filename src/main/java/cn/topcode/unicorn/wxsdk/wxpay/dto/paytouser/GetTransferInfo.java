package cn.topcode.unicorn.wxsdk.wxpay.dto.paytouser;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * Created by Unicorn on 2018/2/25.
 */
@Data
@XStreamAlias("xml")
public class GetTransferInfo {

    /**
     * 随机字符串，不超过32位
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名，长度32
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 商户订单号，长度28
     * 商户调用企业付款api时使用的商户订单号
     */
    @XStreamAlias("partner_trade_no")
    private String partnerTradeNo;

    /**
     * 商户号 长度32
     * 微信支付分配的商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 商户号的appid，长度32
     */
    @XStreamAlias("appid")
    private String appId;
}
