package cn.topcode.unicorn.wxsdk.wxpay.dto.closeorder;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 关闭订单结果
 */
@XStreamAlias("xml")
public class CloseOrderResult {


    /**
     * 	订单已支付	订单已支付，不能发起关单	订单已支付，不能发起关单，请当作已支付的正常交易
     */
    public static final String CODE_ORDERPAID = "ORDERPAID";


    /**
     * 系统错误	系统错误	系统异常，请重新调用该API
     */
    public static final String CODE_SYSTEMERROR = "SYSTEMERROR";


    /**
     * 订单已关闭	订单已关闭，无法重复关闭	订单已关闭，无需继续调用
     */
    public static final String CODE_ORDERCLOSED = "ORDERCLOSED";


    /**
     * 签名错误	参数签名结果不正确	请检查签名参数和方法是否都符合签名算法要求
     */
    public static final String CODE_SIGNERROR = "SIGNERROR";


    /**
     * 请使用post方法	未使用post传递参数 	请检查请求参数是否通过post方法提交
     */
    public static final String CODE_REQUIRE_POST_METHOD = "REQUIRE_POST_METHOD";


    /**
     * 	XML格式错误	XML格式错误	请检查XML参数格式是否正确
     */
    public static final String CODE_XML_FORMAT_ERROR = "XML_FORMAT_ERROR";


    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String code;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String message;

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 子商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 子商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 错误代码
     */
    @XStreamAlias("err_code")
    private String errCode;

    /**
     * 错误代码描述
     */
    @XStreamAlias("err_code_des")
    private String errCodeDes;
}
