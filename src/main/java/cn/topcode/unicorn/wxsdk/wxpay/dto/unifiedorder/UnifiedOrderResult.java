package cn.topcode.unicorn.wxsdk.wxpay.dto.unifiedorder;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 统一下单结果
 */
@Data
@XStreamAlias("xml")
public class UnifiedOrderResult {

    /**
     * 成功
     */
    public static final String CODE_SUCCESS = "SUCCESS";

    /**
     * 商户无此接口权限	商户未开通此接口权限	请商户前往申请此接口权限
     */
    public static final String CODE_NOAUTH = "NOAUTH";

    /**
     * 余额不足	用户帐号余额不足	用户帐号余额不足，请用户充值或更换支付卡后再支付
     */
    public static final String CODE_NOTENOUGH = "NOTENOUGH";

    /**
     * 商户订单已支付	商户订单已支付，无需重复操作	商户订单已支付，无需更多操作
     */
    public static final String CODE_ORDERPAID = "ORDERPAID";

    /**
     * 	订单已关闭	当前订单已关闭，无法支付	当前订单已关闭，请重新下单
     */
    public static final String CODE_ORDERCLOSED = "ORDERCLOSED";

    /**
     * 	系统错误	系统超时	系统异常，请用相同参数重新调用
     */
    public static final String CODE_SYSTEMERROR = "SYSTEMERROR";

    /**
     * 	APPID不存在	参数中缺少APPID	请检查APPID是否正确
     */
    public static final String CODE_APPID_NOT_EXIST = "APPID_NOT_EXIST";


    /**
     * MCHID不存在	参数中缺少MCHID	请检查MCHID是否正确
     */
    public static final String CODE_MCHID_NOT_EXIST = "MCHID_NOT_EXIST";

    /**
     * appid和mch_id不匹配	appid和mch_id不匹配	请确认appid和mch_id是否匹配
     */
    public static final String CODE_APPID_MCHID_NOT_MATCH = "APPID_MCHID_NOT_MATCH";

    /**
     * 缺少参数	缺少必要的请求参数	请检查参数是否齐全
     */
    public static final String CODE_LACK_PARAMS = "LACK_PARAMS";

    /**
     * 商户订单号重复	同一笔交易不能多次提交	请核实商户订单号是否重复提交
     */
    public static final String CODE_OUT_TRADE_NO_USED = "OUT_TRADE_NO_USED";

    /**
     * 签名错误	参数签名结果不正确	请检查签名参数和方法是否都符合签名算法要求
     */
    public static final String CODE_SIGNERROR = "SIGNERROR";

    /**
     * XML格式错误	XML格式错误	请检查XML参数格式是否正确
     */
    public static final String CODE_XML_FORMAT_ERROR = "XML_FORMAT_ERROR";

    /**
     * 请使用post方法	未使用post传递参数 	请检查请求参数是否通过post方法提交
     */
    public static final String CODE_REQUIRE_POST_METHOD = "REQUIRE_POST_METHOD";

    /**
     * post数据为空	post数据不能为空	请检查post数据是否为空
     */
    public static final String CODE_POST_DATA_EMPTY = "POST_DATA_EMPTY";

    /**
     * 编码格式错误	未使用指定编码格式	请使用UTF-8编码格式
     */
    public static final String CODE_NOT_UTF8 = "NOT_UTF8";


    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;

    /**
     * 公众账号ID
     */
    @XStreamAlias("appid")
    private String appId;

    /**
     * 商户号
     */
    @XStreamAlias("mch_id")
    private String mchId;

    /**
     * 子商户公众账号ID
     */
    @XStreamAlias("sub_appid")
    private String subAppId;

    /**
     * 子商户号
     */
    @XStreamAlias("sub_mch_id")
    private String subMchId;

    /**
     * 设备号
     */
    @XStreamAlias("device_info")
    private String deviceInfo;

    /**
     * 随机字符串
     */
    @XStreamAlias("nonce_str")
    private String nonceStr;

    /**
     * 签名，无需填写，wxsdk自动生成
     */
    @XStreamAlias("sign")
    private String sign;

    /**
     * 业务结果
     */
    @XStreamAlias("result_code")
    private String resultCode;

    /**
     * 错误代码
     */
    @XStreamAlias("err_code")
    private String errCode;

    /**
     * 错误代码描述
     */
    @XStreamAlias("err_code_des")
    private String errCodeDes;

    /**
     * 交易类型
     */
    @XStreamAlias("trade_type")
    private String tradeType;

    /**
     * 预支付交易会话标识
     */
    @XStreamAlias("prepay_id")
    private String prepayId;

    /**
     * 二维码链接
     */
    @XStreamAlias("code_url")
    private String codeUrl;
}
