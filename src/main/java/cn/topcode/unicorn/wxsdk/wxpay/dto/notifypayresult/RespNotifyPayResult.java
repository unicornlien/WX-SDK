package cn.topcode.unicorn.wxsdk.wxpay.dto.notifypayresult;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * Created by Unicorn on 2017/12/28.
 */
@Data
@XStreamAlias("xml")
public class RespNotifyPayResult {

    /**
     * 返回状态码
     */
    @XStreamAlias("return_code")
    private String returnCode;

    /**
     * 返回信息
     */
    @XStreamAlias("return_msg")
    private String returnMsg;
}
