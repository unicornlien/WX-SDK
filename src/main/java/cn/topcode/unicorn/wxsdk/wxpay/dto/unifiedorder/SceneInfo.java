package cn.topcode.unicorn.wxsdk.wxpay.dto.unifiedorder;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 场景信息
 */
@Data
public class SceneInfo {

    /**
     * 门店ID
     */
    private String id;

    /**
     * 门店名称
     */
    private String name;

    /**
     * 门店行政区划码
     */
    @JSONField(name = "area_code")
    private String areaCode;

    /**
     * 门店详细地址
     */
    private String address;
}
