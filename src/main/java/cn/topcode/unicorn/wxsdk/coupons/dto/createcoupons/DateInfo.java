package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class DateInfo {

    /**
     * 固定时间范围
     */
    public static final String DATE_TYPE_FIX_TIME_RANGE = "DATE_TYPE_FIX_TIME_RANGE";

    /**
     * 固定时长，自领取后按天算
     */
    public static final String DATE_TYPE_FIX_TERM = "DATE_TYPE_FIX_TERM";

    /**
     * 使用时间的类型
     */
    private String type;

    /**
     * type是自领取后按天算，表示自领取后多少天内有效，不支持填0
     */
    private Integer fixedTerm;

    /**
     * 表示自领取后多少天内有效，单位为天，填0为当天生效
     */
    private Integer fixedBeginTerm;

    /**
     * type为固定时间区间，表示起用时间。单位是秒
     */
    @JSONField(name = "begin_timestamp")
    private Long beginTimestamp;

    /**
     * 表示结束时间，建议设置为截止时间的23:59:59过期，单位为秒
     */
    @JSONField(name = "end_timestamp")
    private Long endTimestamp;
}
