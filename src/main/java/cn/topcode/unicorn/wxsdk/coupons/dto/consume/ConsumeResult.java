package cn.topcode.unicorn.wxsdk.coupons.dto.consume;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class ConsumeResult extends Result {

    private Card card;

    private String openId;
}
