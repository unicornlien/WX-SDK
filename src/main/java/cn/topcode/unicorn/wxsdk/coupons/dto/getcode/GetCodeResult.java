package cn.topcode.unicorn.wxsdk.coupons.dto.getcode;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class GetCodeResult extends Result {

    private Card card;

    @JSONField(name = "openid")
    private String openId;

    @JSONField(name = "can_consume")
    private Boolean canConsume;

    @JSONField(name = "user_card_status")
    private String userCardStatus;
}
