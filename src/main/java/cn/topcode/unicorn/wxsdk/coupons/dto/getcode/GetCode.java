package cn.topcode.unicorn.wxsdk.coupons.dto.getcode;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class GetCode {

    @JSONField(name = "card_id")
    private String cardId;

    private String code;

    @JSONField(name = "check_consume")
    private Boolean checkConsume;
}
