package cn.topcode.unicorn.wxsdk.coupons.dto.getcardlist;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class GetCardListResult extends Result {

    @JSONField(name = "card_list")
    private List<Card> cardList;

    @JSONField(name = "has_share_card")
    private Boolean hasShareCard;

}
