package cn.topcode.unicorn.wxsdk.coupons;

import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.coupons.dto.*;
import cn.topcode.unicorn.wxsdk.coupons.dto.consume.ConsumeResult;
import cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons.CreateCoupns;
import cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons.CreateCouponsResult;
import cn.topcode.unicorn.wxsdk.coupons.dto.createqrcode.CreateCouponQrcode;
import cn.topcode.unicorn.wxsdk.coupons.dto.createqrcode.CreateShift;
import cn.topcode.unicorn.wxsdk.coupons.dto.createqrcode.CreateShiftResult;
import cn.topcode.unicorn.wxsdk.coupons.dto.getcardlist.GetCardListResult;
import cn.topcode.unicorn.wxsdk.coupons.dto.getcode.GetCode;
import cn.topcode.unicorn.wxsdk.coupons.dto.getcode.GetCodeResult;
import cn.topcode.unicorn.wxsdk.coupons.dto.updatecoupon.UpdateCoupon;
import cn.topcode.unicorn.wxsdk.coupons.dto.updatecoupon.UpdateCouponResult;

import java.io.File;
import java.util.Date;
import java.util.List;

public interface CouponsInvoker {

    /**
     * 系统繁忙，此时请开发者稍后再试
     */
    public static final int ERR_CODE_BUSY = -1;

    /**
     * 请求成功
     */
    public static final int ERR_CODE_SUCCESS = 0;

    /**
     * 图片文件超长
     */
    public static final int ERR_CODE_IMAGE_SIZE_TO_LONG = 40009;

    /**
     * 不合法的Appid，请开发者检查AppID的正确性，避免异常字符，注意大小写
     */
    public static final int ERR_CODE_ILLEGAL_APPID = 40013;

    /**
     * 不合法的actioninfo，请开发者确认参数正确
     */
    public static final int ERR_CODE_ILLEGAL_ACTIONINFO = 40053;

    /**
     * 不合法的卡券类型
     */
    public static final int ERR_CODE_ILLEGAL_COUPONS_TYPE = 40071;

    /**
     * 不合法的编码方式
     */
    public static final int ERR_CODE_ILLEGAL_ENCODE = 40072;

    /**
     * 不合法的卡券状态
     */
    public static final int ERR_CODE_ILLEGAL_COUPONS_STATUS = 40078;

    /**
     * 不合法的时间
     */
    public static final int ERR_CODE_ILLEGAL_TIME = 40079;

    /**
     * 不合法的CardExt
     */
    public static final int ERR_CODE_ILLEGAL_CARD_EXT = 40080;

    /**
     * 卡券已被核销
     */
    public static final int ERR_CODE_COUPONS_CONSUMED = 40099;

    /**
     * 不合法的时间区间
     */
    public static final int ERR_CODE_ILLEGAL_TIME_SEG = 40100;

    /**
     * 不合法的Code码
     */
    public static final int ERR_CODE_ILLEGAL_CODE = 40116;

    /**
     * 不合法的库存数量
     */
    public static final int ERR_CODE_ILLEGAL_AMOUNT = 40122;

    /**
     * 会员卡设置查过限制的 custom_field字段
     */
    public static final int ERR_CODE_CUSTOM_FIELD = 40124;

    /**
     * 卡券被用户删除或转赠中
     */
    public static final int ERR_CODE_COUPONS_DELETED = 40127;

    /**
     * 缺少cardid参数
     */
    public static final int ERR_CODE_REQUIRE_CARDID = 41012;

    /**
     * 该cardid无接口权限
     */
    public static final int ERR_CODE_CARDID_NO_PERM = 45030;

    /**
     * 库存为0
     */
    public static final int ERR_CODE_AMOUNT_0 = 45031;

    /**
     * 用户领取次数超过限制get_limit
     */
    public static final int ERR_CODE_USER_OVER_GET_LIMIT = 45033;

    /**
     * 缺少必填字段
     */
    public static final int ERR_CODE_REQUIRE_FIELD = 41011;

    /**
     * 字段超过长度限制，请参考相应接口的字段说明
     */
    public static final int ERR_CODE_FIELD_TO_LONG = 45021;

    /**
     * 不合法的Code码
     */
    public static final int ERR_CODE_ILLEGAL_CODE2 = 40056;

    /**
     * 自定义SN权限，请前往公众平台申请
     */
    public static final int ERR_CODE_DIY_SN = 43009;

    /**
     * 无储值权限，请前往公众平台申请
     */
    public static final int ERR_CODE_STORAGE_NO_PERM = 43010;

    /**
     * 上传卡券logo
     * @author Leo Lien
     * 2016年12月5日 下午10:30:00 创建
     */
    void uploadCouponLogo(String mpId, File file);

    /**
     * 上传卡券Logo
     * @param file
     */
    void uploadCouponLogo(File file);
    
    /**
     * 创建卡券
     * @author Leo Lien
     * 2016年12月5日 下午10:31:02 创建
     */
    CreateCouponsResult createCoupon(String mpId, CreateCoupns createCoupns);

    /**
     * 创建卡券
     * @param createCoupns
     * @return
     */
    CreateCouponsResult createCoupon(CreateCoupns createCoupns);

    /**
     * 设置买单接口
     * @param mpId      公众号ID
     * @param cardId    卡券ID
     * @param isOpen    是否开启买单功能
     * @return
     */
    Result setPayCell(String mpId, String cardId, boolean isOpen);

    /**
     * 设置买单接口
     * @param cardId    卡券ID
     * @param isOpen    是否开启买单功能
     * @return
     */
    Result setPayCell(String cardId, boolean isOpen);

    /**
     * 设置自助核销接口
     * @param mpId              公众号ID
     * @param cardId            卡券ID
     * @param isOpen            是否开启自助核销功能，默认false
     * @param needVerifyCod     用户核销时是否需要输入验证码，默认为false
     * @param needRemarkAmount  用户核销时是否需要备注核销金额，默认为false
     * @return
     */
    Result selfConsumeCell(String mpId, String cardId, boolean isOpen, boolean needVerifyCod, boolean needRemarkAmount);

    /**
     *
     * @param cardId            卡券ID
     * @param isOpen            是否开启自助核销功能，默认false
     * @param needVerifyCod     用户核销时是否需要输入验证码，默认为false
     * @param needRemarkAmount  用户核销时是否需要备注核销金额，默认为false
     * @return
     */
    Result selfConsumeCell(String cardId, boolean isOpen, boolean needVerifyCod, boolean needRemarkAmount);
    
    /**
     * 创建二维码投放
     * @author Leo Lien
     * 2016年12月5日 下午10:32:15 创建
     */
    CreateCouponsResult createCouponQrcode(String mpId, CreateCouponQrcode createCouponQrcode);

    CreateCouponsResult createCouponQrcode(CreateCouponQrcode createCouponQrcode);

    /**
     * 创建货架
     * @param mpId
     * @param createShift
     * @return
     */
    CreateShiftResult createShift(String mpId, CreateShift createShift);

    CreateShiftResult createShift(CreateShift createShift);

    /**
     * 导入code
     * @param mpId
     * @param depositCode
     * @return
     */
    DepositCodeResult depositCode(String mpId, DepositCode depositCode);

    DepositCodeResult depositCode(DepositCode depositCode);

    /**
     * 查询导入code数目接口
     * @param mpId
     * @param cardId
     * @return
     */
    GetDepositCodeResult getDepositCount(String mpId, String cardId);

    GetDepositCodeResult getDepositCount(String cardId);

    /**
     * 核查code
     * @param mpId
     * @param depositCode
     */
    CheckCodeResult checkCode(String mpId, DepositCode depositCode);

    CheckCodeResult checkCode(DepositCode depositCode);

    /**
     * 图文消息群发卡券
     * @param mpId
     * @param cardId
     * @return
     */
    NewsCouponResult newsCoupon(String mpId, String cardId);

    NewsCouponResult newsCoupon(String cardId);
    
    /**
     * 设置测试白名单
     * @author Leo Lien
     * 2016年12月5日 下午10:32:45 创建
     */
    Result setTestWhiteList(String mpId, SetTestWhiteList setTestWhiteList);

    Result setTestWhiteList(SetTestWhiteList setTestWhiteList);

    /**
     * 查询code
     * @param mpId
     * @param getCode
     * @return
     */
    GetCodeResult getCode(String mpId, GetCode getCode);

    GetCodeResult getCode(GetCode getCode);

    /**
     * 核销卡券
     * @author Leo Lien
     * 2016年12月5日 下午10:33:27 创建
     */
    ConsumeResult consume(String mpId, String cardId, String code);

    ConsumeResult consume(String cardId, String code);

    /**
     * code解码
     * @param mpId
     * @param encryptCode
     * @return
     */
    DecryptCodeResult decryptCode(String mpId, String encryptCode);

    DecryptCodeResult decryptCode(String encryptCode);

    /**
     * 获取用户已领取卡券接口
     * @param mpId
     * @param openId
     * @param cardId
     * @return
     */
    GetCardListResult getCardList(String mpId, String openId, String cardId);

    GetCardListResult getCardList(String openId, String cardId);

    /**
     * 卡券详情
     * @param mpId
     * @param cardId
     * @return
     */
    CardDetailResult cardDetail(String mpId, String cardId);

    CardDetailResult cardDetail(String cardId);

    /**
     * 批量查询卡券列表
     * @param mpId
     * @param offset
     * @param count
     * @param statusList
     * @return
     */
    BatchCardDetailResult batchCardDetail(String mpId, Integer offset, Integer count, List<String> statusList);

    BatchCardDetailResult batchCardDetail(Integer offset, Integer count, List<String> statusList);

    /**
     * 更改卡券信息接口
     * @param mpId
     * @param updateCoupon
     * @return
     */
    UpdateCouponResult updateCoupon(String mpId, UpdateCoupon updateCoupon);

    UpdateCouponResult updateCoupon(UpdateCoupon updateCoupon);

    /**
     * 修改库存接口
     * @param mpId
     * @param cardId
     * @param increaseStockValue
     * @param reduceStockValue
     * @return
     */
    Result modifyStock(String mpId, String cardId, Integer increaseStockValue, Integer reduceStockValue);

    Result modifyStock(String cardId, Integer increaseStockValue, Integer reduceStockValue);

    /**
     * 修改code
     * @param mpId
     * @param code
     * @param cardId
     * @param newCode
     * @return
     */
    Result updateCode(String mpId, String code, String cardId, String newCode);

    Result updateCode(String code, String cardId, String newCode);

    /**
     * 删除卡券接口
     * @param mpId
     * @param cardId
     * @return
     */
    Result deleteCard(String mpId, String cardId);

    Result deleteCard(String cardId);

    /**
     * 卡券失效接口
     * @param mpId
     * @param code
     * @param cardId
     * @param reason
     * @return
     */
    Result unavailableCard(String mpId, String code, String cardId, String reason);

    Result unavailableCard(String code, String cardId, String reason);

    /**
     * 获取卡券概况数据接口
     * @param mpId
     * @param beginDate
     * @param endDate
     * @param condSource
     * @return
     */
    GetCardBizUinInfoResult getCardBizUinInfo(String mpId, Date beginDate, Date endDate, Integer condSource);

    GetCardBizUinInfoResult getCardBizUinInfo(Date beginDate, Date endDate, Integer condSource);

    /**
     * 获取免费券数据接口
     * @param mpId
     * @param beginDate
     * @param endDate
     * @param condSource
     * @param cardId
     * @return
     */
    GetCardBizUinInfoResult getFreeCardInfo(String mpId, Date beginDate, Date endDate, Integer condSource, String cardId);

    GetCardBizUinInfoResult getFreeCardInfo(Date beginDate, Date endDate, Integer condSource, String cardId);

    /**
     * 拉取会员卡概况数据接口
     * @return
     */
    GetCardBizUinInfoResult getMemberCardInfo(String mpId, Date beginDate, Date endDate, Integer condSource);

    GetCardBizUinInfoResult getMemberCardInfo(Date beginDate, Date endDate, Integer condSource);

    /**
     * 拉取单张会员卡数据接口
     * @param beginDate
     * @param endDate
     * @param cardId
     * @return
     */
    GetCardBizUinInfoResult getMemberCardDetail(String mpId, Date beginDate, Date endDate, String cardId);

    GetCardBizUinInfoResult getMemberCardDetail(Date beginDate, Date endDate, String cardId);
}
