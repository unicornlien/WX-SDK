package cn.topcode.unicorn.wxsdk.coupons.event;

import cn.topcode.unicorn.wxsdk.message.receive.event.Event;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 转赠卡券事件
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class GivenCouponEvent extends Event {

    /**
     * 用户转赠卡券
     */
    public static final String EVENT_USER_GIFTING_CARD = "user_gifting_card";

    /**
     * 卡券ID
     */
    @XStreamAlias("CardId")
    private String cardId;

    /**
     * code序列号
     */
    @XStreamAlias("UserCardCode")
    private String userCardCode;

    /**
     * 当IsGiveByFriend为1时必填，表示发起转赠用户的openId
     */
    @XStreamAlias("FriendUserName")
    private String friendUserName;

    /**
     * 是否转赠退回，0不是，1是
     */
    @XStreamAlias("IsReturnBack")
    private String isReturnBack;

    /**
     * 是否群转赠
     */
    @XStreamAlias("IsChatRoom")
    private String isChatRoom;
}
