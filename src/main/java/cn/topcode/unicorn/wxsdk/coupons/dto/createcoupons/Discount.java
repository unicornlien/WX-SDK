package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 折扣券
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class Discount {

    @JSONField(name = "base_info")
    private BaseInfo baseInfo;

    /**
     * 折扣券专用，表示打折额度（百分比），填30就是七折
     */
    private Integer discount;
}
