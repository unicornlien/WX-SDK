package cn.topcode.unicorn.wxsdk.coupons.dto.createqrcode;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class Card {

    /**
     * 卡券ID
     */
    @JSONField(name = "card_id")
    private String cardId;

    /**
     * 卡券code码，use_custom_code字段为true的卡券必须填写，非自定义code和导入code模式的卡券不必填写
     */
    private String code;

    /**
     * 指定领取人的openid，只有该用户可以领取。bind_openid字段为true的卡券必须填写，非指定openid不必填写
     */
    @JSONField(name = "open_id")
    private String openId;

    /**
     * 指定下发二维码，生成的二维码随机分配一个code，领取后不可再次扫描。默认为false，
     * 注意填写该字段时，卡券须通过审核且库存不为0
     */
    @JSONField(name = "is_unique_code")
    private Boolean isUniqueCode;

    /**
     * 领取场景值，用于领取渠道的数据统计，用户领取卡券后触发的事件推送中会带上此自定义场景值
     * 用户首次领卡时，会通过领取事件推送给商户；对于会员卡的二维码，用户每次扫码打开会员卡后点击任何url，
     * 会将该值拼入url中，方便开发者定位扫码来源
     */
    @JSONField(name = "outer_str")
    private String outerStr;
}
