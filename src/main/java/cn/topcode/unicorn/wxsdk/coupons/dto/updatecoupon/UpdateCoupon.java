package cn.topcode.unicorn.wxsdk.coupons.dto.updatecoupon;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class UpdateCoupon {

    @JSONField(name = "card_id")
    private String cardId;

    @JSONField(name = "member_card")
    private MemberCard memberCard;

    @JSONField(name = "bonus_cleared")
    private String bonusCleared;

    @JSONField(name = "bonus_rules")
    private String bonusRules;

    private String prerogative;
}
