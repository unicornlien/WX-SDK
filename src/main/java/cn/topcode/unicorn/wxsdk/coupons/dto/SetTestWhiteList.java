package cn.topcode.unicorn.wxsdk.coupons.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class SetTestWhiteList {

    @JSONField(name = "openid")
    private List<String> openId;

    @JSONField(name = "username")
    private List<String> username;
}
