package cn.topcode.unicorn.wxsdk.coupons.dto.createqrcode;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class CreateCouponQrcodeResult extends Result {

    private String ticket;

    private String url;

    @JSONField(name = "show_qrcode_url")
    private String showQrcodeUrl;
}
