package cn.topcode.unicorn.wxsdk.coupons.event;

import cn.topcode.unicorn.wxsdk.message.receive.event.Event;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 领取卡券事件
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class ReceiveCouponEvent extends Event {

    /**
     * 用户领取卡券
     */
    public static final String EVENT_USER_GET_CARD = "user_get_card";

    /**
     * 卡券ID
     */
    @XStreamAlias("CardId")
    private String cardId;

    /**
     * 是否转赠领取，1是，0否
     */
    @XStreamAlias("IsGiveByFriend")
    private String isGiveByFriend;

    /**
     * code序列号
     */
    @XStreamAlias("UserCardCode")
    private String userCardCode;

    /**
     * 当IsGiveByFriend为1时必填，表示发起转赠用户的openId
     */
    @XStreamAlias("FriendUserName")
    private String friendUserName;

    @XStreamAlias("OuterId")
    private String outerId;

    /**
     * 为保证安全，微信会在转赠发送后变更该卡券的code号，该字段表示转赠前的code
     */
    @XStreamAlias("OldUserCardCode")
    private String oldUserCardCode;

    /**
     * 领取场景值，用于领取渠道数据统计，可在生成二维码接口及添加Addcard接口中自定义该字段的字符串值
     */
    @XStreamAlias("OuterStr")
    private String outerStr;

    /**
     * 用户删除会员卡后可重新找回，当用户本次操作为找回时，该值为1，否则为0
     */
    @XStreamAlias("IsRestoreMemberCard")
    private String isRestoreMemberCard;

    @XStreamAlias("IsRecommendByFriend")
    private String isRecommendByFriend;
}
