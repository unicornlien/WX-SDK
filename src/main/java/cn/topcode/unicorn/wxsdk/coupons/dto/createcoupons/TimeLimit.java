package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class TimeLimit {

    public static final String TYPE_MONDAY = "MONDAY";

    public static final String TYPE_TUESDAY = "TUESDAY";

    public static final String TYPE_WEDNESDAY = "WEDNESDAY";

    public static final String TYPE_THURSDAY = "THURSDAY";

    public static final String TYPE_FRIDAY = "FRIDAY";

    public static final String TYPE_SATURDAY = "SATURDAY";

    public static final String TYPE_SUNDAY = "SUNDAY";

    /**
     * 限制类型枚举,此处只控制显示，不控制实际使用逻辑，不填默认不显示
     */
    private String type;

    /**
     * 起始时间（小时）
     */
    @JSONField(name = "begin_hour")
    private Integer beginHour;

    /**
     * 结束时间（小时）
     */
    @JSONField(name = "end_hour")
    private Integer endHour;

    /**
     * 开始时间（分钟）
     */
    @JSONField(name = "begin_minute")
    private Integer beginMinute;

    /**
     * 结束时间（分钟）
     */
    @JSONField(name = "end_minute")
    private Integer endMinute;
}
