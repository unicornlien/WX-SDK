package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import lombok.Data;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class Sku {

    /**
     * 卡券库存数量，上限为100000000
     */
    private Long quantitiy;
}
