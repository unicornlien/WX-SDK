package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 代金券
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class Cash {

    /**
     * 基本卡券数据，所有卡券通用
     */
    @JSONField(name = "base_info")
    private BaseInfo baseInfo;

    /**
     * 代金券专用，表示起用金额（单位为分），无起用门槛填0
     */
    @JSONField(name = "least_cost")
    private Integer leastCost;

    /**
     * 代金券专用，表示减免金额（单位为分）
     */
    @JSONField(name = "reduce_cost")
    private Integer reduceCost;
}
