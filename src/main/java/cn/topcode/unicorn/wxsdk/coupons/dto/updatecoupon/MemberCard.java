package cn.topcode.unicorn.wxsdk.coupons.dto.updatecoupon;

import cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons.BaseInfo;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class MemberCard {

    @JSONField(name = "base_info")
    private BaseInfo baseInfo;
}
