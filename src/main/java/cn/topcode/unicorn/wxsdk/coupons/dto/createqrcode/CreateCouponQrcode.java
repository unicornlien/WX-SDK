package cn.topcode.unicorn.wxsdk.coupons.dto.createqrcode;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class CreateCouponQrcode {

    @JSONField(name = "action_name")
    private String actionName;

    /**
     * 二维码有效时间，范围是60~1800秒，不填默认365天有效
     */
    @JSONField(name = "expire_seconds")
    private Integer expireSeconds;

    @JSONField(name = "action_info")
    private ActionInfo actionInfo;
}
