package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 卡券高级信息
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class AdvancedInfo {

    public static final String BIZ_SERVICE_DELIVER = "BIZ_SERVICE_DELIVER";

    public static final String BIZ_SERVICE_FREE_PARK = "BIZ_SERVICE_FREE_PARK";

    public static final String BIZ_SERVICE_WITH_PET = "BIZ_SERVICE_WITH_PET";

    public static final String BIZ_SERVICE_FREE_WIFI = "BIZ_SERVICE_FREE_WIFI";

    /**
     * 使用条件，若不填写使用条件则在券面填写：无最低消费限制，全场通用，不限品类；
     * 并在使用说明显示：可与其他优惠共享
     */
    @JSONField(name = "use_condition")
    private UseCondition useCondition;

    /**
     * 封面摘要结构体名称
     */
    @JSONField(name = "abstract")
    private AbstractInfo abstractInfo;

    /**
     * 图文列表，显示在详情内页，优惠券开发者至少传入一组图文列表
     */
    @JSONField(name = "text_image_list")
    private List<TextImage> textImageList;

    /**
     * 使用时段限制
     */
    @JSONField(name = "time_limit")
    private List<TimeLimit> timeLimits;

    /**
     * 商家服务类型，可多选
     */
    @JSONField(name = "business_service")
    private List<String> businessService;
}










