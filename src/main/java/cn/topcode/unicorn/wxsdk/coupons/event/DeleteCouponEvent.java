package cn.topcode.unicorn.wxsdk.coupons.event;

import cn.topcode.unicorn.wxsdk.message.receive.event.Event;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 转赠卡券事件
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class DeleteCouponEvent extends Event {

    /**
     * 用户删除卡券
     */
    public static final String EVENT_USER_GET_CARD = "user_del_card";

    /**
     * 卡券ID
     */
    @XStreamAlias("CardId")
    private String cardId;

    /**
     * code序列号
     */
    @XStreamAlias("UserCardCode")
    private String userCardCode;
}
