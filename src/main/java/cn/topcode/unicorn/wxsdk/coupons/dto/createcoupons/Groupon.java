package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class Groupon {

    /**
     * 基本卡券数据，所有卡券类型通用
     */
    @JSONField(name = "base_info")
    private BaseInfo baseInfo;

    @JSONField(name = "advanced_info")
    private AdvancedInfo advancedInfo;

    /**
     * 团购券专用，团购详情
     */
    @JSONField(name = "deal_detail")
    private String dealDetail;
}
