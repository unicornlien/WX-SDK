package cn.topcode.unicorn.wxsdk.coupons.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class BatchCardDetailResult extends Result {

    @JSONField(name = "card_id_list")
    private List<String> cardIdList;

    @JSONField(name = "total_num")
    private Integer totalNum;
}
