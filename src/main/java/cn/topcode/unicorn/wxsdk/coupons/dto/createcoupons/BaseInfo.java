package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class BaseInfo {

    public static final String Color010	= "#63b359";

    public static final String Color020	= "#2c9f67";

    public static final String Color030	= "#509fc9";

    public static final String Color040	= "#5885cf";

    public static final String Color050	= "#9062c0";

    public static final String Color060	= "#d09a45";

    public static final String Color070	= "#e4b138";

    public static final String Color080	= "#ee903c";

    public static final String Color081	= "#f08500";

    public static final String Color082	= "#a9d92d";

    public static final String Color090	= "#dd6549";

    public static final String Color100	= "#cc463d";

    public static final String Color101	= "#cf3e36";

    public static final String Color102	= "#5E6671";

    /**
     * 文本
     */
    public static final String CODE_TYPE_TEXT = "CODE_TYPE_TEXT";

    /**
     * 一维码
     */
    public static final String CODE_TYPE_BARCODE = "CODE_TYPE_BARCODE";

    /**
     * 二维码
     */
    public static final String CODE_TYPE_QRCODE = "CODE_TYPE_QRCODE";

    /**
     * 一维码无code显示
     */
    public static final String CODE_TYPE_ONLY_BARCODE = "CODE_TYPE_ONLY_BARCODE";

    /**
     * 二维码无code显示
     */
    public static final String CODE_TYPE_ONLY_QRCODE = "CODE_TYPE_ONLY_QRCODE";

    /**
     * 不显示code和条形码类型
     */
    public static final String CODE_TYPE_NONE = "CODE_TYPE_NONE";

    /*必填*/

    /**
     * 卡券商户logo，建议像素300*300
     */
    @JSONField(name = "logo_url")
    private String logoUrl;

    /**
     * 商户名字，字数上限12个汉字
     */
    @JSONField(name = "brand_name")
    private String brandName;

    /**
     * 码型
     */
    @JSONField(name = "code_type")
    private String codeType;

    /**
     * 卡券名，字数上限9个汉字，建议涵盖卡券属性、服务及金额，例如双人套餐100元兑换券
     */
    private String title;

    /**
     * 券颜色，按色彩规范填写Color010-Color100
     */
    private String color;

    /**
     * 卡券使用提醒，字数上限16个汉字
     */
    private String notice;

    /**
     * 卡券使用说明，字数上限1024字
     */
    private String description;

    /**
     *  使用日期，有效期信息
     */
    @JSONField(name = "date_info")
    private DateInfo dateInfo;

    /**
     * 商品信息
     */
    private Sku sku;


    /*非必填*/

    /**
     * 是否自定义code码，默认为false
     */
    @JSONField(name = "use_custom_code")
    private Boolean useCustomCode;

    /**
     * 填入GET_CUSTOM_COD E_MODE_DEPOSIT表示该卡券为预存code模式卡券，
     * 需导入超过库存数目的自定义code方可投放，填入该字段后quantity字段必须为0，需导入code后再增加库存
     */
    @JSONField(name = "get_custom_code_mode")
    private String getCustomCodeMode;

    /**
     * 是否指定用户领取，默认为false。
     * 通常指定特殊用户群体 投放卡券或防止刷券时选择指定用户领取
     */
    @JSONField(name = "bind_openid")
    private Boolean bindOpenId;

    /**
     *  客服电话
     */
    @JSONField(name = "service_phone")
    private String servicePhone;

    /**
     * 门店位置poiid。调用POI门店管理接口获取门店位置poiid，
     * 局别线下门店的商户为必填
     */
    @JSONField(name = "location_id_list")
    private List<Long> locationIdList;

    /**
     * 设置本卡券支持全部门店，与location_id_list互斥
     */
    @JSONField(name = "use_all_locations")
    private Boolean useAllLocations;

    /**
     * 卡券顶部居中的按钮，仅在卡券状态正常（可以核销）时显示
     */
    @JSONField(name = "center_title")
    private String centerTitle;

    /**
     * 显示在入口下方的提示语，仅在卡券状态正常（可以核销）时显示
     */
    @JSONField(name = "center_sub_title")
    private String centerSubTitle;

    /**
     * 顶部居中的url，仅在卡券状态正常（可以核销）时显示
     */
    @JSONField(name = "center_url")
    private String centerUrl;

    /**
     * 卡券跳转的小程序的user_name，仅可跳转该公众号绑定的小程序
     */
    @JSONField(name = "center_app_brand_user_name")
    private String centerAppBrandUserName;

    /**
     * 卡券跳转的小程序的path
     */
    @JSONField(name = "center_app_brand_pass")
    private String centerAppBrandPass;

    /**
     * 自定义跳转外链的入口名字
     */
    @JSONField(name = "custom_url_name")
    private String customUrlName;

    /**
     * 自定义跳转的url
     */
    @JSONField(name = "custom_url")
    private String customUrl;

    /**
     * 显示在入口右侧的提示语
     */
    @JSONField(name = "custom_url_sub_title")
    private String customUrlSubTitle;

    /**
     * 卡券跳转的小程序的user_name，仅可跳转该公众号绑定的小程序
     */
    @JSONField(name = "custom_app_brand_user_name")
    private String customAppBrandUserName;

    /**
     * 卡券跳转的小程序的path
     */
    @JSONField(name = "custom_app_brand_pass")
    private String customAppBrandPass;

    /**
     * 营销场景的自定义入口名称
     */
    @JSONField(name = "promotion_url_name")
    private String promotionUrlName;

    /**
     * 入口跳转外链的地址链接
     */
    @JSONField(name = "promotion_url_name")
    private String promotionUrl;

    /**
     * 显示在营销入口右侧的提示语
     */
    @JSONField(name = "promotion_url_name")
    private String promotionUrlSubTitle;

    /**
     * 卡券跳转的小程序的user_name，仅可跳转该公众号绑定的小程序
     */
    @JSONField(name = "promotion_app_brand_user_name")
    private String promotionAppBrandUserName;

    /**
     * 卡券跳转的小程序的path
     */
    @JSONField(name = "promotion_app_brand_pass")
    private String promotionAppBrandPass;

    /**
     * 每人可核销的数量限制，默认为50
     */
    @JSONField(name = "use_limit")
    private Integer useLimit;

    /**
     * 每人可领券的数量限制，默认为50
     */
    @JSONField(name = "get_limit")
    private Integer getLimit;

    /**
     * 卡券领取页面是否可分享
     */
    @JSONField(name = "can_share")
    private Boolean canShare;

    /**
     * 卡券是否可转赠
     */
    @JSONField(name = "can_give_friend")
    private Boolean canGiveFriend;







    @JSONField(name = "source")
    private String source;
}
