package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class Card {

    /**
     * 团购券类型
     */
    public static final String CARD_TYPE_GROUPON = "GROUPON";

    /**
     * 代金券
     */
    public static final String CARD_TYPE_CASH = "CASH";

    /**
     * 折扣券
     */
    public static final String CARD_TYPE_DISCOUNT = "DISCOUNT";

    /**
     * 兑换券
     */
    public static final String CARD_TYPE_GIFT = "GIFT";

    /**
     * 优惠券
     */
    public static final String CARD_TYPE_GENERAL_COUPON = "GENERAL_COUPON";


    /**
     * 卡券类型
     */
    @JSONField(name = "card_type")
    private String cardType;

    /**
     * 团购券信息
     */
    private Groupon groupon;

    /**
     * 代金券
     */
    private Cash cash;

    /**
     * 折扣券
     */
    private Discount discount;

    /**
     * 兑换券
     */
    private Gift gift;

    /**
     * 优惠券
     */
    @JSONField(name = "general_coupon")
    private GeneralCoupon generalCoupon;
}

























