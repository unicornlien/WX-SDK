package cn.topcode.unicorn.wxsdk.coupons.dto.getcode;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class Card {

    @JSONField(name = "card_id")
    private String cardId;

    @JSONField(name = "begin_time")
    private Long beginTime;

    @JSONField(name = "end_time")
    private Long endTime;
}
