package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class CreateCouponsResult extends Result {

    @JSONField(name = "card_id")
    private String cardId;
}
