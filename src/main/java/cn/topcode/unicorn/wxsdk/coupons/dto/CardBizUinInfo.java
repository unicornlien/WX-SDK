package cn.topcode.unicorn.wxsdk.coupons.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class CardBizUinInfo {

    @JSONField(name = "ref_date", format = "yyyy-MM-dd")
    private Date refDate;

    private Integer merchanttype;

    @JSONField(name = "cardid")
    private String cardId;

    private Integer submerchantid;

    @JSONField(name = "view_cnt")
    private Integer viewCnt;

    @JSONField(name = "view_user")
    private Integer viewUser;

    @JSONField(name = "receive_cnt")
    private Integer receiveCnt;

    @JSONField(name = "receive_user")
    private Integer receiveUser;

    @JSONField(name = "verify_cnt")
    private Integer verifyCnt;

    @JSONField(name = "verify_user")
    private Integer verifyUser;

    @JSONField(name = "active_cnt")
    private Integer activeCnt;

    @JSONField(name = "active_user")
    private Integer activeUser;

    @JSONField(name = "given_cnt")
    private Integer givenCnt;

    @JSONField(name = "given_user")
    private Integer givenUser;

    @JSONField(name = "expire_cnt")
    private Integer expireCnt;

    @JSONField(name = "expire_user")
    private Integer expireUser;

    @JSONField(name = "total_user")
    private Integer totalUser;

    @JSONField(name = "total_receive_user")
    private Integer totalReceiveUser;

    private Integer payOriginalFee;

    private Integer fee;
}
