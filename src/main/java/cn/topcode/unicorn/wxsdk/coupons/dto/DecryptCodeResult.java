package cn.topcode.unicorn.wxsdk.coupons.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class DecryptCodeResult extends Result {

    private String code;
}
