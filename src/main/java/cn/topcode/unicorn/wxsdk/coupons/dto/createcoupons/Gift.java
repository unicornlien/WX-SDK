package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 兑换券
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class Gift {

    @JSONField(name = "base_info")
    private BaseInfo baseInfo;

    /**
     * 兑换券专用，填写兑换内容的名称
     */
    private String gift;
}
