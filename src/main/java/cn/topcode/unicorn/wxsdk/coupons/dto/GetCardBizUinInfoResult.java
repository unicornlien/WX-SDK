package cn.topcode.unicorn.wxsdk.coupons.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class GetCardBizUinInfoResult extends Result {

    private List<CardBizUinInfo> list;
}
