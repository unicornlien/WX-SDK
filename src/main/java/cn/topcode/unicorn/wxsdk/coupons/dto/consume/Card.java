package cn.topcode.unicorn.wxsdk.coupons.dto.consume;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class Card {

    @JSONField(name = "card_id")
    private String cardId;
}
