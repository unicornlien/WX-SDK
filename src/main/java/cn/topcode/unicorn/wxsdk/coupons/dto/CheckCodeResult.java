package cn.topcode.unicorn.wxsdk.coupons.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class CheckCodeResult extends Result {

    @JSONField(name = "exist_code")
    private List<String> existCode;

    @JSONField(name = "not_exist_code")
    private List<String> notExistCode;
}
