package cn.topcode.unicorn.wxsdk.coupons.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class GetDepositCodeResult extends Result {

    private Integer count;
}
