package cn.topcode.unicorn.wxsdk.coupons.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class DepositCodeResult extends Result {

    @JSONField(name = "succ_code")
    private Integer succCode;

    @JSONField(name = "duplicate_code")
    private Integer duplicateCode;

    @JSONField(name = "fail_code")
    private Integer failCode;
}
