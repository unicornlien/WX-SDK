package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import lombok.Data;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class CreateCoupns {

    private Card card;
}
