package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 使用条件
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class UseCondition {

    /**
     * 指定可用商品类目，仅用于代金券类型，填入后将在券面拼写适用于xxx
     */
    @JSONField(name = "accept_category")
    private String acceptCategory;

    /**
     * 指定不可用商品类目，仅用于代金券类型，填入后将在券面拼写不适用于xxx
     */
    @JSONField(name = "reject_category")
    private String rejectCategory;

    /**
     * 满减门槛字段，可用于兑换券和代金券，填入后将在券面拼写消费满xx元可用
     */
    @JSONField(name = "least_cost")
    private Integer leastCost;

    /**
     * 购买xx可用类型门槛，仅用于兑换，填入后自动拼写购买xx可用
     */
    @JSONField(name = "object_use_for")
    private String objectUseFor;

    /**
     * 不可以与其他类型共享门槛，填写false系统将在使用须知里拼写"不可与其他优惠共享"，
     * 填写true时系统将在适用须知里拼写"可与其他优惠共享"，默认true
     */
    @JSONField(name = "can_use_with_other_discount")
    private Boolean canUseWithOtherDiscount;
}


















