package cn.topcode.unicorn.wxsdk.coupons.dto.createqrcode;

import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class CreateShift {

    /**
     * 附近
     */
    public static final String SCENE_NEAR_BY = "SCENE_NEAR_BY";

    /**
     * 自定义菜单
     */
    public static final String SCENE_MENU = "SCENE_MENU";

    /**
     * 二维码
     */
    public static final String SCENE_QRCODE = "SCENE_QRCODE";

    /**
     * 公众号文章
     */
    public static final String SCENE_ARTICLE = "SCENE_ARTICLE";

    /**
     * H5页面
     */
    public static final String SCENE_H5 = "SCENE_H5";

    /**
     * 自动回复
     */
    public static final String SCENE_IVR = "SCENE_IVR";

    /**
     * 卡券自定义cell
     */
    public static final String SCENE_CARD_CUSTOM_CELL = "SCENE_CARD_CUSTOM_CELL";

    /**
     * 页面的banner图片链接，需调用，建议尺寸640*300
     */
    private String banner;

    /**
     * 页面title
     */
    private String title;

    /**
     * 页面是否可以分享，填入true/false
     */
    private Boolean canShare;

    /**
     * 投放页面场景值
     */
    private String scene;

    /**
     * 卡券列表，每个item两个字段
     */
    private Long cardList;

    /**
     * 所要在页面投放的card_id
     */
    private Long cardId;

    /**
     * 缩略图url
     */
    private Long thumbUrl;
}
