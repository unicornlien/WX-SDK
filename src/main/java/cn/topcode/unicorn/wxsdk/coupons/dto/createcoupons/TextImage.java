package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class TextImage {

    /**
     * 图片链接，必须调用 上传图片接口 上传图片获得链接，并在此填入，否则报错
     */
    @JSONField(name = "image_url")
    private String imageUrl;

    /**
     * 图文描述
     */
    @JSONField(name = "text")
    private String text;
}
