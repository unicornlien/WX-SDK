package cn.topcode.unicorn.wxsdk.coupons.dto.updatecoupon;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class UpdateCouponResult extends Result {

    @JSONField(name = "send_check")
    private Boolean sendCheck;
}
