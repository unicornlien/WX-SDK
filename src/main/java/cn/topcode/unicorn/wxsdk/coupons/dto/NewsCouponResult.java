package cn.topcode.unicorn.wxsdk.coupons.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class NewsCouponResult extends Result {

    private String content;
}
