package cn.topcode.unicorn.wxsdk.coupons.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons.Card;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class CardDetailResult extends Result {

    private Card card;
}
