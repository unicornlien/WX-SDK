package cn.topcode.unicorn.wxsdk.coupons.event;

import cn.topcode.unicorn.wxsdk.message.receive.event.Event;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 卡券审核事件
 * Created by Unicorn on 2018/4/12.
 */
@Data
public class AuditCouponEvent extends Event {

    /**
     * 卡券通过审核
     */
    public static final String EVENT_CARD_PASS_CHECK = "card_pass_check";

    /**
     * 卡券未通过审核
     */
    public static final String EVENT_CARD_NOT_PASS_CHECK = "card_not_pass_check";

    /**
     * 卡券ID
     */
    @XStreamAlias("CardId")
    private String cardId;

    /**
     * 审核不通过原因
     */
    @XStreamAlias("RefuseReason")
    private String refuseReason;
}
