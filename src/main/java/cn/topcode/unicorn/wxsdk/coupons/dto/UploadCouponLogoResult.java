package cn.topcode.unicorn.wxsdk.coupons.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import lombok.Data;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class UploadCouponLogoResult extends Result {

    /**
     * 商户图片URL，用于创建卡券接口中填入
     */
    private String url;
}
