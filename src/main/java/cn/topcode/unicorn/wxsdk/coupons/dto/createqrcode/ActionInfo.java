package cn.topcode.unicorn.wxsdk.coupons.dto.createqrcode;

import lombok.Data;

/**
 * Created by Unicorn on 2018/4/11.
 */
@Data
public class ActionInfo {

    private Card card;
}
