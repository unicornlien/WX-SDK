package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class AbstractInfo {

    /**
     * 封面摘要简介
     */
    @JSONField(name = "abstract")
    private String abstractInfo;

    /**
     * 封面图片列表，仅支持填入一个封面图片链接，上传图片接口 上传获取图片获得地址
     * 填写非cdn链接或报错，并在此填入，建议图片尺寸像素850*350
     */
    @JSONField(name = "icon_url_list")
    private List<String> iconUrlList;
}
