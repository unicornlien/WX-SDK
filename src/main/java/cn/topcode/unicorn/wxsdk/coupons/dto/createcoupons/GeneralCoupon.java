package cn.topcode.unicorn.wxsdk.coupons.dto.createcoupons;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 优惠券
 * Created by Unicorn on 2018/4/10.
 */
@Data
public class GeneralCoupon {

    @JSONField(name = "base_info")
    private BaseInfo baseInfo;

    /**
     * 优惠券专用，填写优惠详情
     */
    @JSONField(name = "default_detail")
    private String defaultDetail;
}
