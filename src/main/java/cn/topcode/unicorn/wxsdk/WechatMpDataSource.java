package cn.topcode.unicorn.wxsdk;

import cn.topcode.unicorn.wxsdk.base.UserInfo;
import cn.topcode.unicorn.wxsdk.base.WechatMpInfo;

/**
 * Created by Unicorn on 2018/3/20.
 */
public interface WechatMpDataSource {

    /**
     * 通过公众号ID查找公众号信息
     * @param mpId
     * @return
     */
    WechatMpInfo findByMpId(String mpId);

    /**
     * 保存公众号信息
     * @param wechatMpInfo
     */
    void saveWechatMpInfo(WechatMpInfo wechatMpInfo);

    /**
     * 获取默认公众号ID
     * @return
     */
    String getDefaultMpId();

    /**
     * 是否存在微信用户信息
     * @param mpId  公众号ID
     * @param openId    用户openId
     * @return  是否存在
     */
    boolean existWechatUser(String mpId, String openId);

    /**
     * 保存微信用户信息
     * @param mpId  公众号ID
     * @param userInfo  微信用户信息
     */
    void saveWechatUser(String mpId, UserInfo userInfo);
}
