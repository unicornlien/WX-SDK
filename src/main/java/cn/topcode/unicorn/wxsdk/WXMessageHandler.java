package cn.topcode.unicorn.wxsdk;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import cn.topcode.unicorn.wxsdk.message.receive.msg.Message;
import cn.topcode.unicorn.wxsdk.message.reply.ReplyMessage;

/**
 * 微信消息处理器接口
 * @author Unicorn Lien
 * 2017年3月28日 下午10:55:15 创建
 * @param <T>   消息
 */
public abstract class WXMessageHandler<T extends Message> {
    
    @SuppressWarnings("unchecked")
    public WXMessageHandler() {
        Class<T> entityClass = null;
        Type t = getClass().getGenericSuperclass();
        if(t instanceof ParameterizedType){
            Type[] p = ((ParameterizedType)t).getActualTypeArguments();
            entityClass = (Class<T>)p[0];
        }
        WXContext.registHandler(entityClass, this);
    }

    public abstract ReplyMessage handle(T message);
}
