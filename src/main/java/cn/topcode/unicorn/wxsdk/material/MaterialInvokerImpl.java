package cn.topcode.unicorn.wxsdk.material;

import cn.topcode.unicorn.utils.JsonUtil;
import cn.topcode.unicorn.utils.http.HttpResp;
import cn.topcode.unicorn.utils.http.HttpUtil;
import cn.topcode.unicorn.wxsdk.WXContext;
import cn.topcode.unicorn.wxsdk.base.ApiUrl;
import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.material.dto.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Unicorn on 2017/12/25.
 */
public class MaterialInvokerImpl implements MaterialInvoker {

    /**
     * 上传临时图片素材
     *
     * @param image
     */
    @Override
    public UploadTempMaterialResult uploadTempImageMaterial(String mpId, File image) {
        return uploadTempMaterial(mpId,"image", image);
    }

    @Override
    public UploadTempMaterialResult uploadTempImageMaterial(File image) {
        return uploadTempImageMaterial(WXContext.getDefaultMpId(), image);
    }

    /**
     * 上传临时语音素材
     *
     * @param voice
     * @return
     */
    @Override
    public UploadTempMaterialResult uploadTempVoiceMaterial(String mpId, File voice) {
        return uploadTempMaterial(mpId, "voice", voice);
    }

    @Override
    public UploadTempMaterialResult uploadTempVoiceMaterial(File voice) {
        return uploadTempVoiceMaterial(WXContext.getDefaultMpId(), voice);
    }

    /**
     * 上传临时视频素材
     *
     * @param video
     * @return
     */
    @Override
    public UploadTempMaterialResult uploadTempVideoMaterial(String mpId, File video) {
        return uploadTempMaterial(mpId, "video", video);
    }

    @Override
    public UploadTempMaterialResult uploadTempVideoMaterial(File video) {
        return uploadTempVideoMaterial(WXContext.getDefaultMpId(), video);
    }

    /**
     * 上传临时缩略图素材
     *
     * @param thumb
     * @return
     */
    @Override
    public UploadTempMaterialResult uploadTempThumbMaterial(String mpId, File thumb) {
        return uploadTempMaterial(mpId, "thumb", thumb);
    }

    @Override
    public UploadTempMaterialResult uploadTempThumbMaterial(File thumb) {
        return uploadTempThumbMaterial(WXContext.getDefaultMpId(), thumb);
    }

    private UploadTempMaterialResult uploadTempMaterial(String mpId, String type, File file) {
        if(!file.exists()) {
            throw new IllegalArgumentException("文件不存在：" + file.getPath());
        }
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Material.CREATE_TEMP_MATERIAL, token, type);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("media", file);
        HttpResp r = HttpUtil.uploadFile(url, params);
        return JsonUtil.parse(r.getBody(), UploadTempMaterialResult.class);
    }

    /**
     * 下载临时素材到指定位置
     *
     * @param mediaId
     * @param path
     */
    @Override
    public Result downloadTempMaterial(String mpId, String mediaId, String path) {
        return null;
    }

    @Override
    public Result downloadTempMaterial(String mediaId, String path) {
        return downloadTempMaterial(WXContext.getDefaultMpId(), mediaId, path);
    }

    /**
     * 上传永久图文素材
     *
     * @param material
     * @return
     */
    @Override
    public AddForeverMaterialResult uploadForeverArticleMaterial(String mpId, AddForeverMaterial material) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Material.CREATE_FOREVER_ARTICLE_MATERIAL, token);
        return HttpUtil.post(material, AddForeverMaterialResult.class, url);
    }

    @Override
    public AddForeverMaterialResult uploadForeverArticleMaterial(AddForeverMaterial material) {
        return uploadForeverArticleMaterial(WXContext.getDefaultMpId(), material);
    }

    /**
     * 上传图文素材的图片
     * @param image 图片文件
     * @return  url地址
     */
    @Override
    public String uploadArticleImage(String mpId, File image) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Material.UPLOAD_ARTICLE_IMG, token);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("media", image);
        HttpResp resp = HttpUtil.uploadFile(url, params);
        return resp.getBody();
    }

    @Override
    public String uploadArticleImage(File image) {
        return uploadArticleImage(WXContext.getDefaultMpId(), image);
    }

    /**
     * 上传永久素材-视频
     * @param video
     * @return
     */
    @Override
    public UploadForeverMaterialResult uploadForeverVideoMaterial(String mpId, String title, String introduction, File video) {
        if(!video.exists()) {
            throw new IllegalArgumentException("文件不存在：" + video.getPath());
        }
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Material.CREATE_FOREVER_MATERIAL, token, "video");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("media", video);
        VideoDescription description = new VideoDescription();
        description.setTitle(title);
        description.setIntroduction(introduction);
        params.put("description", JsonUtil.toJson(description));
        HttpResp r = HttpUtil.uploadFile(url, params);
        return JsonUtil.parse(r.getBody(), UploadForeverMaterialResult.class);
    }

    @Override
    public UploadForeverMaterialResult uploadForeverVideoMaterial(String title, String introduction, File video) {
        return uploadForeverVideoMaterial(WXContext.getDefaultMpId(), title, introduction, video);
    }

    /**
     * 上传永久素材-图片
     * @param image
     * @return
     */
    @Override
    public UploadForeverMaterialResult uploadForeverImageMaterial(String mpId, File image) {
        return uploadForeverMaterial(mpId, "image", image);
    }

    @Override
    public UploadForeverMaterialResult uploadForeverImageMaterial(File image) {
        return uploadForeverImageMaterial(WXContext.getDefaultMpId(), image);
    }

    /**
     * 上传永久素材-语音
     * @param voice
     * @return
     */
    @Override
    public UploadForeverMaterialResult uploadForeverVoiceMaterial(String mpId, File voice) {
        return uploadForeverMaterial(mpId, "voice", voice);
    }

    @Override
    public UploadForeverMaterialResult uploadForeverVoiceMaterial(File voice) {
        return uploadForeverVoiceMaterial(WXContext.getDefaultMpId(), voice);
    }

    /**
     * 上传永久素材-缩略图
     * @param thumb
     * @return
     */
    @Override
    public UploadForeverMaterialResult uploadForeverThumbMaterial(String mpId, File thumb) {
        return uploadForeverMaterial(mpId, "thumb", thumb);
    }

    @Override
    public UploadForeverMaterialResult uploadForeverThumbMaterial(File thumb) {
        return uploadForeverThumbMaterial(WXContext.getDefaultMpId(), thumb);
    }

    /**
     * 上传永久素材
     * @param type
     * @param file
     * @return
     */
    private UploadForeverMaterialResult uploadForeverMaterial(String mpId, String type, File file) {
        if(!file.exists()) {
            throw new IllegalArgumentException("文件不存在：" + file.getPath());
        }
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Material.CREATE_FOREVER_MATERIAL, token, type);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("media", file);
        HttpResp r = HttpUtil.uploadFile(url, params);
        return JsonUtil.parse(r.getBody(), UploadForeverMaterialResult.class);
    }

    @Override
    public void downloadForeverMaterial(String mpId) {

    }

    @Override
    public void downloadForeverMaterial() {
        downloadForeverMaterial(WXContext.getDefaultMpId());
    }

    /**
     * 删除永久素材
     *
     * @param mediaId
     * @return
     */
    @Override
    public Result deleteForeverMaterial(String mpId, String mediaId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Material.DELETE_FOREVER_MATERIAL, token);
        DeleteForeverMaterial deleteForeverMaterial = new DeleteForeverMaterial();
        deleteForeverMaterial.setMediaId(mediaId);
        return HttpUtil.post(deleteForeverMaterial, Result.class, url);
    }

    @Override
    public Result deleteForeverMaterial(String mediaId) {
        return deleteForeverMaterial(WXContext.getDefaultMpId(), mediaId);
    }

    @Override
    public MaterialCount countMaterial(String mpId) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Material.GET_MATERIAL_COUNT, token);
        return HttpUtil.getJson(url, MaterialCount.class);
    }

    @Override
    public MaterialCount countMaterial() {
        return countMaterial(WXContext.getDefaultMpId());
    }

    @Override
    public MaterialQueryResult queryMaterialList(String mpId, MaterialQuery materialQuery) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Material.GET_MATERIAL_LIST, token);
        return HttpUtil.post(materialQuery, MaterialQueryResult.class, url);
    }

    @Override
    public MaterialQueryResult queryMaterialList(MaterialQuery materialQuery) {
        return queryMaterialList(WXContext.getDefaultMpId(), materialQuery);
    }
}
