package cn.topcode.unicorn.wxsdk.material.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2017/8/18.
 */
@Data
public class UploadTempMaterialResult extends Result {

    private String type;

    @JSONField(name = "media_id")
    private String mediaId;

    @JSONField(name = "created_at")
    private Long createdAt;
}
