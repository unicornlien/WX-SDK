package cn.topcode.unicorn.wxsdk.material.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2017/8/19.
 */
@Data
public class AddForeverMaterial {

    private List<Article> articles;

}
