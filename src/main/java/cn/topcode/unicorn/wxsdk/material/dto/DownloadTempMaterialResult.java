package cn.topcode.unicorn.wxsdk.material.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2017/8/18.
 */
@Data
public class DownloadTempMaterialResult extends Result {

    @JSONField(name = "video_url")
    private String videoUrl;
}
