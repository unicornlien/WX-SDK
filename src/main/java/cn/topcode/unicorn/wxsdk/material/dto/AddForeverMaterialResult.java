package cn.topcode.unicorn.wxsdk.material.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2017/8/19.
 */
@Data
public class AddForeverMaterialResult extends Result {

    @JSONField(name = "media_id")
    private String mediaId;
}
