package cn.topcode.unicorn.wxsdk.material.dto;

import lombok.Data;

/**
 * Created by Unicorn on 2018/2/11.
 */
@Data
public class MaterialQuery {

    public static final String TYPE_IMAGE = "image";

    public static final String TYPE_VIDEO = "video";

    public static final String TYPE_VOICE = "voice";

    public static final String TYPE_NEWS = "news";

    private String type;

    private Integer offset;

    private Integer count;
}
