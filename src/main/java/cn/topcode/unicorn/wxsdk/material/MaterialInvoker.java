package cn.topcode.unicorn.wxsdk.material;

import cn.topcode.unicorn.wxsdk.base.Result;
import cn.topcode.unicorn.wxsdk.material.dto.*;

import java.io.File;

public interface MaterialInvoker {


    /**
     * 上传临时图片素材
     */
    UploadTempMaterialResult uploadTempImageMaterial(String mpId, File image);

    UploadTempMaterialResult uploadTempImageMaterial(File image);

    /**
     * 上传临时语音素材
     * @param voice
     * @return
     */
    UploadTempMaterialResult uploadTempVoiceMaterial(String mpId, File voice);

    UploadTempMaterialResult uploadTempVoiceMaterial(File voice);

    /**
     * 上传临时视频素材
     * @param video
     * @return
     */
    UploadTempMaterialResult uploadTempVideoMaterial(String mpId, File video);

    UploadTempMaterialResult uploadTempVideoMaterial(File video);

    /**
     * 上传临时缩略图素材
     * @param thumb
     * @return
     */
    UploadTempMaterialResult uploadTempThumbMaterial(String mpId, File thumb);

    UploadTempMaterialResult uploadTempThumbMaterial(File thumb);

    /**
     * 下载临时素材到指定位置 未实现
     * @param mediaId
     * @param path
     */
    Result downloadTempMaterial(String mpId, String mediaId, String path);

    Result downloadTempMaterial(String mediaId, String path);

    /**
     * 上传永久图文素材
     * @param material
     * @return
     */
    AddForeverMaterialResult uploadForeverArticleMaterial(String mpId, AddForeverMaterial material);

    AddForeverMaterialResult uploadForeverArticleMaterial(AddForeverMaterial material);

    /**
     * 上传图文素材的图片
     * @param image 图片文件
     * @return  url地址
     */
    String uploadArticleImage(String mpId, File image);

    String uploadArticleImage(File image);

    /**
     * 上传永久素材-视频
     * @param video
     * @return
     */
    UploadForeverMaterialResult uploadForeverVideoMaterial(String mpId, String title, String introduction, File video);

    UploadForeverMaterialResult uploadForeverVideoMaterial(String title, String introduction, File video);

    /**
     * 上传永久素材-图片
     * @param image
     * @return
     */
    UploadForeverMaterialResult uploadForeverImageMaterial(String mpId, File image);

    UploadForeverMaterialResult uploadForeverImageMaterial(File image);

    /**
     * 上传永久素材-语音
     * @param voice
     * @return
     */
    UploadForeverMaterialResult uploadForeverVoiceMaterial(String mpId, File voice);

    UploadForeverMaterialResult uploadForeverVoiceMaterial(File voice);

    /**
     * 上传永久素材-缩略图
     * @param thumb
     * @return
     */
    UploadForeverMaterialResult uploadForeverThumbMaterial(String mpId, File thumb);

    UploadForeverMaterialResult uploadForeverThumbMaterial(File thumb);

    /**
     * 获取永久素材 未实现
     */
    void downloadForeverMaterial(String mpId);

    void downloadForeverMaterial();

    /**
     * 删除永久素材
     * @param mediaId
     * @return
     */
    Result deleteForeverMaterial(String mpId, String mediaId);

    Result deleteForeverMaterial(String mediaId);

    /**
     * 获取素材数量
     * @return
     */
    MaterialCount countMaterial(String mpId);

    MaterialCount countMaterial();

    /**
     * 获取素材列表 未实现
     */
    MaterialQueryResult queryMaterialList(String mpId, MaterialQuery materialQuery);

    MaterialQueryResult queryMaterialList(MaterialQuery materialQuery);
}
