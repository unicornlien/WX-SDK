package cn.topcode.unicorn.wxsdk.material.dto;

import cn.topcode.unicorn.wxsdk.base.Result;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/2/11.
 */
@Data
public class MaterialQueryResult extends Result {

    @JSONField(name = "total_count")
    private Integer totalCount;

    @JSONField(name = "item_count")
    private Integer itemCount;

    private List<MaterialItem> item;
}
