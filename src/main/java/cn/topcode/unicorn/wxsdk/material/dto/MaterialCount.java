package cn.topcode.unicorn.wxsdk.material.dto;

import lombok.Data;

import com.alibaba.fastjson.annotation.JSONField;

@Data
public class MaterialCount {

    @JSONField(name="voice_count")
    private Integer voiceCount;
    
    @JSONField(name="video_count")
    private Integer videoCount;
    
    @JSONField(name="image_count")
    private Integer imageCount;
    
    @JSONField(name="news_count")
    private Integer newsCount;

}
