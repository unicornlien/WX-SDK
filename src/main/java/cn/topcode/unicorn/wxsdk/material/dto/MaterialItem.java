package cn.topcode.unicorn.wxsdk.material.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;

/**
 * Created by Unicorn on 2018/2/11.
 */
@Data
public class MaterialItem {

    @JSONField(name = "media_id")
    private String mediaId;

    private String name;

    private String url;

    private MaterialItemContent content;

    @JSONField(name = "update_time")
    private String updateTime;
}
