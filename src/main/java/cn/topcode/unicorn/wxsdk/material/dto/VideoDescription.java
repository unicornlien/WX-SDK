package cn.topcode.unicorn.wxsdk.material.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Unicorn on 2017/12/25.
 */
@Data
public class VideoDescription implements Serializable {

    private String title;

    private String introduction;
}
