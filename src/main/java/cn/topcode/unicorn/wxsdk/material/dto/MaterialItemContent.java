package cn.topcode.unicorn.wxsdk.material.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Created by Unicorn on 2018/2/11.
 */
@Data
public class MaterialItemContent {

    @JSONField(name = "news_item")
    private List<News> newsItem;
}
