package cn.topcode.unicorn.wxsdk.material.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2017/8/19.
 */
@Data
public class DeleteForeverMaterial {

    @JSONField(name = "media_id")
    private String mediaId;
}
