package cn.topcode.unicorn.wxsdk.material.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created by Unicorn on 2017/8/19.
 */
@Data
public class Article {

    private String title;

    @JSONField(name = "thumb_media_id")
    private String thumbMediaId;

    private String author;

    private String digest;

    @JSONField(name = "show_cover_pic")
    private String showCoverPic;

    private String content;

    @JSONField(name = "content_source_url")
    private String contentSourceUrl;
}
