package cn.topcode.unicorn.wxsdk.account.dto;

import lombok.Data;
import com.alibaba.fastjson.annotation.JSONField;

@Data
public class ReqCreateTicket {

    /**
     * 临时二维码（整型场景值）
     */
    public static final String ACTION_QR_SCENE = "QR_SCENE";

    /**
     * 临时二维码（字符串场景值）
     */
    public static final String ACTION_QR_STR_SCENE = "QR_STR_SCENE";

    /**
     * 永久二维码（整型场景值）
     */
    public static final String ACTION_QR_LIMIT_SCENE = "QR_LIMIT_SCENE";

    /**
     * 永久二维码（字符串场景值）
     */
    public static final String ACTION_QR_LIMIT_STR_SCENE = "QR_LIMIT_STR_SCENE";


    @JSONField(name="expire_seconds")
    private long expireSeconds;
    
    @JSONField(name="action_name")
    private String actionName;
    
    @JSONField(name="action_info")
    private ActionInfo actionInfo;

}
