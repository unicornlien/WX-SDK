package cn.topcode.unicorn.wxsdk.account.dto;

import lombok.Data;
import com.alibaba.fastjson.annotation.JSONField;

@Data
public class ReqShortUrl {

    private String action;
    
    @JSONField(name="long_url")
    private String longUrl;

}
