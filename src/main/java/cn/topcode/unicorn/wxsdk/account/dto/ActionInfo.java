package cn.topcode.unicorn.wxsdk.account.dto;

import lombok.Data;

@Data
public class ActionInfo {

    private Scene scene;

}
