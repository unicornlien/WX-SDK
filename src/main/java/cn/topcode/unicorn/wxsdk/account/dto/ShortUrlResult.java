package cn.topcode.unicorn.wxsdk.account.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class ShortUrlResult extends Result {

    @JSONField(name="short_url")
    private String shortUrl;

}
