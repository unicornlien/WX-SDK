package cn.topcode.unicorn.wxsdk.account.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.topcode.unicorn.wxsdk.base.Result;

import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class CreateTicketResult extends Result {

    private String ticket;
    
    @JSONField(name="expire_seconds")
    private long expireSeconds;
    
    private String url;

}
