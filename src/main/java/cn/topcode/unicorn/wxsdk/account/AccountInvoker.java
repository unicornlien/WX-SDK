package cn.topcode.unicorn.wxsdk.account;

import java.io.InputStream;
import cn.topcode.unicorn.wxsdk.account.dto.CreateTicketResult;
import cn.topcode.unicorn.wxsdk.account.dto.ReqCreateTicket;
import cn.topcode.unicorn.wxsdk.account.dto.ReqShortUrl;
import cn.topcode.unicorn.wxsdk.account.dto.ShortUrlResult;

public interface AccountInvoker {

    CreateTicketResult createTicket(String mpId, ReqCreateTicket req);

    CreateTicketResult createTicket(ReqCreateTicket req);
    
    InputStream getQrcodeByTicket(String mpId, String ticket);

    InputStream getQrcodeByTicket(String ticket);
    
    ShortUrlResult shorturl(String mpId, ReqShortUrl req);

    ShortUrlResult shorturl(ReqShortUrl req);
}
