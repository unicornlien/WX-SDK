package cn.topcode.unicorn.wxsdk.account.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.alibaba.fastjson.annotation.JSONField;

@Data
@EqualsAndHashCode(callSuper=false)
public class Scene {

    @JSONField(name="scene_id")
    private long sceneId;
    
    @JSONField(name="scene_str")
    private String sceneStr;
    
}
