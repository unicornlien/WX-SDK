package cn.topcode.unicorn.wxsdk.account;

import cn.topcode.unicorn.utils.http.HttpUtil;
import cn.topcode.unicorn.wxsdk.WXContext;
import cn.topcode.unicorn.wxsdk.account.dto.CreateTicketResult;
import cn.topcode.unicorn.wxsdk.account.dto.ReqCreateTicket;
import cn.topcode.unicorn.wxsdk.account.dto.ReqShortUrl;
import cn.topcode.unicorn.wxsdk.account.dto.ShortUrlResult;
import cn.topcode.unicorn.wxsdk.base.ApiUrl;
import java.io.InputStream;

/**
 * Created by Unicorn on 2017/12/3.
 */
public class AccountInvokerImpl implements AccountInvoker {

    @Override
    public CreateTicketResult createTicket(String mpId, ReqCreateTicket req) {
        String token = WXContext.getToken(mpId);
        String url = String.format(ApiUrl.Account.CREATE_TICKET, token);
        return HttpUtil.post(req, CreateTicketResult.class, url);
    }

    @Override
    public CreateTicketResult createTicket(ReqCreateTicket req) {
        return createTicket(WXContext.getDefaultMpId(), req);
    }

    @Override
    public InputStream getQrcodeByTicket(String mpId, String ticket) {
        return null;
    }

    @Override
    public InputStream getQrcodeByTicket(String ticket) {
        return getQrcodeByTicket(WXContext.getDefaultMpId(), ticket);
    }

    @Override
    public ShortUrlResult shorturl(String mpId, ReqShortUrl req) {
        return null;
    }

    @Override
    public ShortUrlResult shorturl(ReqShortUrl req) {
        return shorturl(WXContext.getDefaultMpId(), req);
    }
}
